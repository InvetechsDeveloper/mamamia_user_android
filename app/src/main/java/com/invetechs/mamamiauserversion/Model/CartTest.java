package com.invetechs.mamamiauserversion.Model;

import java.util.List;

public class CartTest
{

    /**
     * data : {"meals":[{"meal_id":"62","amount":"30"},{"meal_id":"70","amount":"34"}],"distancetouser":"3.4","user_id":"131","kitchen_id":"140","lat":"30.077707","lng":"31.336341","userplace":"hgfugfujgf","deliverytotalprice":"15","totalprice":"45","BillTotal":"30"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * meals : [{"meal_id":"62","amount":"30"},{"meal_id":"70","amount":"34"}]
         * distancetouser : 3.4
         * user_id : 131
         * kitchen_id : 140
         * lat : 30.077707
         * lng : 31.336341
         * userplace : hgfugfujgf
         * deliverytotalprice : 15
         * totalprice : 45
         * BillTotal : 30
         */

        private String distancetouser;
        private String user_id;
        private String kitchen_id;
        private String lat;
        private String lng;
        private String userplace;
        private String deliverytotalprice;
        private String totalprice;
        private String BillTotal;
        private List<MealsBean> meals;

        public String getDistancetouser() {
            return distancetouser;
        }

        public void setDistancetouser(String distancetouser) {
            this.distancetouser = distancetouser;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(String kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public String getDeliverytotalprice() {
            return deliverytotalprice;
        }

        public void setDeliverytotalprice(String deliverytotalprice) {
            this.deliverytotalprice = deliverytotalprice;
        }

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getBillTotal() {
            return BillTotal;
        }

        public void setBillTotal(String BillTotal) {
            this.BillTotal = BillTotal;
        }

        public List<MealsBean> getMeals() {
            return meals;
        }

        public void setMeals(List<MealsBean> meals) {
            this.meals = meals;
        }

        public static class MealsBean {
            /**
             * meal_id : 62
             * amount : 30
             */

            private int meal_id;
            private int amount;
            private  String note ;

            public int getMeal_id() {
                return meal_id;
            }

            public void setMeal_id(int meal_id) {
                this.meal_id = meal_id;
            }

            public int getAmount() {
                return amount;
            }

            public void setAmount(int  amount) {
                this.amount = amount;
            }

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }
        }
    }
}
