package com.invetechs.mamamiauserversion.Model;

public class PointsModel {

    private String restaurant;
    private String points;
    private String money;

    public PointsModel() {
    }

    public PointsModel(String restaurant, String points, String money) {
        this.restaurant = restaurant;
        this.points = points;
        this.money = money;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
