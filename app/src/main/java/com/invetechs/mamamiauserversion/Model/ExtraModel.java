package com.invetechs.mamamiauserversion.Model;

import java.util.Observable;

public class ExtraModel extends Observable {

    private int id;
    private String price;
    int sum = -1;
    private int itemCounts = 1;
    private static ExtraModel extraModelObserver;

    public static ExtraModel getInstance() {
        if (extraModelObserver == null)
            extraModelObserver = new ExtraModel();
        return extraModelObserver;
    }

    public int getItemCounts() {
        setChanged();
        return itemCounts;
    }

    public void setItemCounts(int itemCounts) {
        setChanged();
        this.itemCounts = itemCounts;
    }

    public int getSum() {
        setChanged();
        return sum;
    }

    public void setSum(int sum) {
        setChanged();
        this.sum = sum;
    }

    public int getId() {
        setChanged();
        return id;
    }

    public void setId(int id) {
        setChanged();
        this.id = id;
    }

    public String getPrice() {
        setChanged();
        return price;
    }

    public String setPrice(String price) {
        setChanged();
        this.price = price;
        return price;
    }
}
