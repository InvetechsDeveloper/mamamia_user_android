package com.invetechs.mamamiauserversion.Model;

public class OrderDetailsModel {

    // attributes
    private String order_num;
    private String order_name;

    public OrderDetailsModel() {
    } // empty constructor

    public OrderDetailsModel(String order_num, String order_name) {
        this.order_num = order_num;
        this.order_name = order_name;
    } // constructor with parameters


    // getter

    public String getOrder_num() {
        return order_num;
    }

    public String getOrder_name() {
        return order_name;
    }
}
