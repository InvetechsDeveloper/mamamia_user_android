package com.invetechs.mamamiauserversion.Model;

public class RulesModel {

    private String points;
    private String money;
    private String discount;

    public RulesModel() {
    }

    public RulesModel(String points, String money, String discount) {
        this.points = points;
        this.money = money;
        this.discount = discount;
    }

    public String getDiscount() {
        return discount;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
