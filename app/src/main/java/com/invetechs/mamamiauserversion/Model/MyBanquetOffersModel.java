package com.invetechs.mamamiauserversion.Model;

public class MyBanquetOffersModel {

    // attributes
    private String offer_name;
    private String offer_price;

    public MyBanquetOffersModel() {
    } // empty constructor


    public MyBanquetOffersModel(String offer_name, String offer_price) {
        this.offer_name = offer_name;
        this.offer_price = offer_price;
    } // constructor with parameteres


    // getter


    public String getOffer_name() {
        return offer_name;
    }

    public String getOffer_price() {
        return offer_price;
    }
}
