package com.invetechs.mamamiauserversion.Model;

public class InvoiceModel {

    // attributes
    private String order_num;
    private String date;
    private String totaly_payment;

    public InvoiceModel() {
    } // empty constructor

    public InvoiceModel(String order_num, String date, String totaly_payment) {
        this.order_num = order_num;
        this.date = date;
        this.totaly_payment = totaly_payment;
    } // constructor with parameteres


    // getter
    public String getOrder_num() {
        return order_num;
    }

    public String getDate() {
        return date;
    }

    public String getTotaly_payment() {
        return totaly_payment;
    }
}
