package com.invetechs.mamamiauserversion.Model;

public class OrdersModel {

    // attributes
    private String order_name;
    private String order_num;
    private String location;
    private String price;

    public OrdersModel() {
    } // empty constructor

    public OrdersModel(String order_name, String order_num, String location, String price) {
        this.order_name = order_name;
        this.order_num = order_num;
        this.location = location;
        this.price = price;
    } // constructor with parameteres


    // getter

    public String getOrder_name() {
        return order_name;
    }

    public String getOrder_num() {
        return order_num;
    }

    public String getLocation() {
        return location;
    }

    public String getPrice() {
        return price;
    }
}
