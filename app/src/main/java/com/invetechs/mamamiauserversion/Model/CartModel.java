package com.invetechs.mamamiauserversion.Model;

import java.util.ArrayList;

public class CartModel {

    private String enTitle , arTitle , notes;
    private  int amount , id , kitchenId;
    private  double price , total ;
    ArrayList<Integer> additions;

    public  CartModel()
    {} // default Constructor


    public CartModel(int id ,String enTitle, String arTitle, String notes, int amount , double price) {
        this.enTitle = enTitle;
        this.arTitle = arTitle;
        this.notes = notes;
        this.amount = amount;
        this.id = id;
        this.price = price;
        this.total = total;
    }

    public CartModel(int id, String enTitle, String arTitle, String notes, int amount, double price, int kitchenId) {
        this.enTitle = enTitle;
        this.arTitle = arTitle;
        this.notes = notes;
        this.amount = amount;
        this.price = price;
        this.id = id;
        this.kitchenId = kitchenId;
        this.additions = additions;
    } // Parma Constructor


    public CartModel(int id, String enTitle, String arTitle, String notes, int amount, double price, int kitchenId , ArrayList<Integer> additions) {
        this.enTitle = enTitle;
        this.arTitle = arTitle;
        this.notes = notes;
        this.amount = amount;
        this.price = price;
        this.id = id;
        this.kitchenId = kitchenId;
        this.additions = additions;
    } // Parma Constructor


    public ArrayList<Integer> getAdditions() {
        return additions;
    }

    public void setAdditions(ArrayList<Integer> additions) {
        this.additions = additions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnTitle() {
        return enTitle;
    }

    public void setEnTitle(String enTitle) {
        this.enTitle = enTitle;
    }

    public String getArTitle() {
        return arTitle;
    }

    public void setArTitle(String arTitle) {
        this.arTitle = arTitle;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getKitchenId() {
        return kitchenId;
    }

    public void setKitchenId(int kitchenId) {
        this.kitchenId = kitchenId;
    }

    public double getTotal() {
        return price*amount;
    }

    public void setTotal(double total) {
        this.total = total;
    }

} // class of CartModel
