package com.invetechs.mamamiauserversion.Model;

import java.util.ArrayList;

public class CartApiModel
{

    ArrayList<Meals> meals ;

    int user_id ;
    int kitchen_id ;
    double lat ;
    double lng ;
    String userplace ;
    float distancetouser ;
    float deliverytotalprice ;
    double totalprice ;
    double BillTotal ;
    double discountpoint;
    int checkuserpoint;
    int point ;
    double priceVat ;
    String code ;

    public  CartApiModel(){} // default constructor

    public CartApiModel(ArrayList<Meals> meals, int user_id, int kitchen_id, double lat, double lng, String userplace, float distancetouser, float deliverytotalprice, double totalprice, double billTotal,int point,double priceVat)
    {
        this.meals = meals;
        this.user_id = user_id;
        this.kitchen_id = kitchen_id;
        this.lat = lat;
        this.lng = lng;
        this.userplace = userplace;
        this.distancetouser = distancetouser;
        this.deliverytotalprice = deliverytotalprice;
        this.totalprice = totalprice;
        this.point = point;
        BillTotal = billTotal;
        this.priceVat = priceVat ;
    } // param


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getDiscountpoint() {
        return discountpoint;
    }

    public void setDiscountpoint(double discountpoint) {
        this.discountpoint = discountpoint;
    }

    public int getCheckuserpoint() {
        return checkuserpoint;
    }

    public void setCheckuserpoint(int checkuserpoint) {
        this.checkuserpoint = checkuserpoint;
    }

    public ArrayList<Meals> getMeals() {
        return meals;
    }

    public void setMeals(ArrayList<Meals> meals) {
        this.meals = meals;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getKitchen_id() {
        return kitchen_id;
    }

    public void setKitchen_id(int kitchen_id) {
        this.kitchen_id = kitchen_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getUserplace() {
        return userplace;
    }

    public void setUserplace(String userplace) {
        this.userplace = userplace;
    }

    public float getDistancetouser() {
        return distancetouser;
    }

    public void setDistancetouser(float distancetouser) {
        this.distancetouser = distancetouser;
    }

    public float getDeliverytotalprice() {
        return deliverytotalprice;
    }

    public void setDeliverytotalprice(float deliverytotalprice) {
        this.deliverytotalprice = deliverytotalprice;
    }

    public double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(double totalprice) {
        this.totalprice = totalprice;
    }

    public double getBillTotal() {
        return BillTotal;
    }

    public void setBillTotal(double billTotal) {
        BillTotal = billTotal;
    }


    public static class Meals {
        int meal_id, amount ;
        String note;
        ArrayList<Integer> additions ;

        public Meals(int meal_id, int amount, String note, ArrayList<Integer> additions) {
            this.meal_id = meal_id;
            this.amount = amount;
            this.note = note;
            this.additions = additions;
        }

        public Meals() {
        } // default constructor

//
//        public ArrayList<String> getAddiotions() {
//            return addiotions;
//        }
//
//        public void setAddiotions(ArrayList<String> addiotions) {
//            this.addiotions = addiotions;
//        }

        public int getMeal_id() {
            return meal_id;
        }

        public void setMeal_id(int meal_id) {
            this.meal_id = meal_id;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    } // class of meals




    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public double getPriceVat() {
        return priceVat;
    }

    public void setPriceVat(double priceVat) {
        this.priceVat = priceVat;
    }
} // class of CartApiModel
