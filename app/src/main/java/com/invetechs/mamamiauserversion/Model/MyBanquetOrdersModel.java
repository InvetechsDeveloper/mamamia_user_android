package com.invetechs.mamamiauserversion.Model;

public class MyBanquetOrdersModel {

    // attributes
    private String banquet_name;
    private String order_number;
    private String restaurant_name;
    private String price;

    public MyBanquetOrdersModel() {
    } // empty constructor

    public MyBanquetOrdersModel(String banquet_name, String order_number, String restaurant_name, String price) {
        this.banquet_name = banquet_name;
        this.order_number = order_number;
        this.restaurant_name = restaurant_name;
        this.price = price;
    } // constructor with parameteres

    // getter

    public String getBanquet_name() {
        return banquet_name;
    }

    public String getOrder_number() {
        return order_number;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public String getPrice() {
        return price;
    }
}
