package com.invetechs.mamamiauserversion.Model;

public class ReviewModel {

    // attributes
    private String name;
    private String date;
    private String description;

    public ReviewModel() {
    } // empty constructor

    public ReviewModel(String name, String date, String description) {
        this.name = name;
        this.date = date;
        this.description = description;
    } // constructor with parameteres

    // getter
    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }
}
