package com.invetechs.mamamiauserversion.Config;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.invetechs.mamamiauserversion.View.Activity.LoginActivity;

public class CheckUserLogin
{
    private SharedPreferences pref ;
    private  String isLogin="";
    private  int userId;
    private static final String MY_PREFS_NAME = "MyPrefsFile";

    public   boolean check(Context context)
    {
        pref = context.getSharedPreferences(MY_PREFS_NAME,Context.MODE_PRIVATE);
        if(pref != null) {
            isLogin = pref.getString("isLogin", "false");
            userId = pref.getInt("id", 0);

            if (userId == 0 || isLogin.equals("false"))
                return false;
        }

        return  true;
    } // function of check

    public void openLoginActivity(Context context)
    {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);

    } // function of openLoginActivity

    public int getUserId(Context context)
    {
        pref = context.getSharedPreferences(MY_PREFS_NAME,Context.MODE_PRIVATE);
        if(pref != null)
            userId = pref.getInt("id", 0);
        return userId;
    } // function of getUserId
} // class of CheckUserLogin


//AIzaSyCT2FcysFxfEQcbxQ0MW2241Tma0Rgbp5s
