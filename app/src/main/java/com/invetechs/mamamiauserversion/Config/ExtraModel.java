package com.invetechs.mamamiauserversion.Config;

public class ExtraModel {

    private String extra;
    private String price;

    public ExtraModel() {
    }

    public ExtraModel(String extra, String price) {
        this.extra = extra;
        this.price = price;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
