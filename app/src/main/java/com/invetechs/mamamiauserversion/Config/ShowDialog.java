package com.invetechs.mamamiauserversion.Config;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.R;

import static android.content.Context.MODE_PRIVATE;

public class ShowDialog {

    Dialog acceptAndRejectDialog;
    TextView messageShow;
    LinearLayout layout;

    private static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void initDialog(String message , Context context)
    {
        acceptAndRejectDialog = new Dialog(context);
        acceptAndRejectDialog.setContentView(R.layout.progress_message);
        layout = acceptAndRejectDialog.findViewById(R.id.layoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        acceptAndRejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        acceptAndRejectDialog.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
        acceptAndRejectDialog.show();
        messageShow = acceptAndRejectDialog.findViewById(R.id.tv_message);

     SharedPreferences sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            messageShow.setTypeface(font);
            messageShow.setText(message);

        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            messageShow.setTypeface(font);
            messageShow.setText(message);

        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                acceptAndRejectDialog.dismiss();
                // Toast.makeText(getContext(), getString(R.string.enterVaildDate), Toast.LENGTH_SHORT).show();
            }
        }, 2000);
    }

    public void dismissDialog()
    {
        acceptAndRejectDialog.dismiss();
    }

}
