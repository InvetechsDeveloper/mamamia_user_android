package com.invetechs.mamamiauserversion.Config;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invetechs.mamamiauserversion.Control.NotificationstAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Notifications.NotificationApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.NotificationResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.NotificationsActivity;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;
import com.nex3z.notificationbadge.NotificationBadge;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.anwarshahriar.calligrapher.Calligrapher;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


public class ToolBarConfig extends Language {

    // vars

    static int count_number = 0;
    static String c = "0";
    static int user_id = 0;
    int userId = 0;
    JSONObject jsonObject;
    public TextView toolbar_title;
    public ImageView toolbar_cart;
    ImageView img_notifications;
    int user_login = 0;
    String token = "";
    Activity context;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    NotificationBadge cart_badge;
    NotificationBadge badge_notification;
    Toolbar tool_bar;
    private static int cartCounter = 0;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    ArrayList<CartModel> cartlist = new ArrayList<>();
    CustomDialogProgress progress;
    Handler handler;
    String flag = "";
    public Context cthis;
    Animation shake;
    public ImageView mamamia_logo;
    DrawerLayout drawer_layout;
    Pusher pusher;
    Channel channel;
    List<NotificationResponse.DataBean> arrayList = new ArrayList<>();


    public ToolBarConfig() {
    } // default constructor

    public ToolBarConfig(Activity context) {
        this.context = context;
    } // param constructor

    public void inVisibleBackButton() {
        //toolbar_back.setVisibility(View.INVISIBLE);
    } // function of inVisibleBackButton

    public ToolBarConfig(final Activity context, String flag) {
        this.context = context;
        shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        toolbar_title = context.findViewById(R.id.tv_title);
        //toolbar_back = context.findViewById(R.id.img_back);
        tool_bar = context.findViewById(R.id.tool_bar);
        toolbar_cart = context.findViewById(R.id.img_cart);
        cart_badge = context.findViewById(R.id.badge_cart);
        badge_notification = context.findViewById(R.id.badge_notification);
        img_notifications = context.findViewById(R.id.img_notifications);
        mamamia_logo = context.findViewById(R.id.mamamia_logo);
        this.flag = flag;

        drawer_layout = context.findViewById(R.id.drawer_layout);

        ((AppCompatActivity) (context)).setSupportActionBar(tool_bar);
        ActionBar actionBar = ((AppCompatActivity) (context)).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

//        if (sharedPreferences.getString("language", "ar").equals("ar")) {
//
//            ((AppCompatActivity) (context)).getSupportActionBar().setHomeAsUpIndicator(R.drawable.arabic_back);
//
//
//        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
//            ((AppCompatActivity) (context)).getSupportActionBar().setHomeAsUpIndicator(R.drawable.english_back);
//
//        }

        if (((AppCompatActivity) (context)).getSupportActionBar() != null) {
            ((AppCompatActivity) (context)).getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (drawer_layout != null) {
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(context, drawer_layout, tool_bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer_layout.addDrawerListener(toggle);
                toggle.syncState();
            } else {
                tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.onBackPressed();
                    }
                });
            }
        }


        getUserID();
        notification();
        //getUserID();
    } // param constructor

    public ToolBarConfig(final Activity context, String flag, Context cthis) {
        this.context = context;
        shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        toolbar_title = context.findViewById(R.id.tv_title);
        //toolbar_back = context.findViewById(R.id.img_back);
        tool_bar = context.findViewById(R.id.tool_bar);
        toolbar_cart = context.findViewById(R.id.img_cart);
        cart_badge = context.findViewById(R.id.badge_cart);
        badge_notification = context.findViewById(R.id.badge_notification);
        img_notifications = context.findViewById(R.id.img_notifications);
        mamamia_logo = context.findViewById(R.id.mamamia_logo);
        this.flag = flag;
        this.cthis = cthis;
        getUserID();
        notification();

        ((AppCompatActivity) (context)).setSupportActionBar(tool_bar);
        ActionBar actionBar = ((AppCompatActivity) (context)).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

//        if (sharedPreferences.getString("language", "ar").equals("ar")) {
//
//            ((AppCompatActivity) (context)).getSupportActionBar().setHomeAsUpIndicator(R.drawable.arabic_back);
//
//        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
//            ((AppCompatActivity) (context)).getSupportActionBar().setHomeAsUpIndicator(R.drawable.english_back);
//
//        }

        if (((AppCompatActivity) (context)).getSupportActionBar() != null) {
            ((AppCompatActivity) (context)).getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (drawer_layout != null) {
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(context, drawer_layout, tool_bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer_layout.addDrawerListener(toggle);
                toggle.syncState();
            } else {
                tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.onBackPressed();
                    }
                });
            }
        }

        getUserID();
        notification();

    } // param constructor

    public void setTitle(String title) {
        toolbar_title.setText(title);
    } // function of setTitle to toolbar

    public void setBack(final Activity context) {
      /*  toolbar_back.setVisibility(View.VISIBLE);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onBackPressed();
            }
        });*/
    } // function handl backButton on toolbar

    public void initCartBadge() {

        pref = context.getSharedPreferences("cart", Context.MODE_PRIVATE);
        int badge = pref.getInt("badge", 0);
        cartCounter = badge;
        cart_badge.setNumber(cartCounter);

        toolbar_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("QP", "cart click");
                Intent intent = new Intent(context, MenuActivity.class);
                intent.putExtra("fragmentFlag", "cart");
                context.startActivity(intent);
            }
        });

    } // init cart

    public void destroy() {
        if (context != null) {
            context = null;
        }
        Thread.interrupted();
        pusher.disconnect();
        pusher.unsubscribe("my-channel");

        Log.i("QQ", "Destroy12");
    }

    public void addCart(CartModel cartModel) {

        int repeatItem = 0;
        Log.i("QIP", "addCart Id : " + cartModel.getId());
        getCartData();

        if (cartlist.size() > 0)
            for (int i = 0; i < cartlist.size(); i++) {
                if (cartModel.getId() == cartlist.get(i).getId()) {
                    Log.i("QIP", "addCart if : amount //" + cartlist.get(i).getAmount() + " : " + cartModel.getAmount());
                    cartModel.setAmount(cartlist.get(i).getAmount() + cartModel.getAmount());
                    cartlist.remove(i);
                    repeatItem = 1;
                } // item added before

            } // for loop

        cartlist.add(cartModel);
        if (repeatItem == 0) {
            cart_badge.setNumber(++cartCounter);
            editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
            editor.putInt("badge", cartCounter);
            editor.apply();
        }

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cartlist);
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();

        Log.i("QP", "add json : " + json);

    } // function increase cart

    public void removeCart() {
        if (cartCounter != 0) {
            cart_badge.setNumber(--cartCounter);
            editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
            editor.putInt("badge", cartCounter);
            editor.apply();
        } else {
            cart_badge.setNumber(0);
            editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
            editor.putInt("badge", 0);
            editor.apply();
        }
    } // function decrease cart

    public void clearCart() {
        cartCounter = 0;
        cart_badge.setNumber(cartCounter);
        editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
        editor.putInt("badge", 0);
        editor.apply();
        Log.i("QP", "clear");
        editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
        editor.putInt("kitchenId", 0);
        editor.putInt("maxbill", 0);
        editor.putFloat("deliveryPerKilo", 0f);
        editor.putFloat("distanceToUser", 0f);
        editor.apply();
        getCartData();
        cartlist.clear();

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cartlist);
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();
    } // function clear cart

    private void getCartData() {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("MyObject", "");
        if (json != null) {
            Type type = new TypeToken<List<CartModel>>() {
            }.getType();
            cartlist = gson.fromJson(json, type);
            if (cartlist == null) cartlist = new ArrayList<>();

        }

    } // function of getCartData

    public void removeItemWithId(int id) {
        cart_badge.setNumber(--cartCounter);
        editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
        editor.putInt("badge", cartCounter);
        editor.apply();
        getCartData();
        for (int i = 0; i < cartlist.size(); i++) {
            if (id == cartlist.get(i).getId()) {
                cartlist.remove(i);
            }
        }
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cartlist);
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();
    } // function of removeItemWithId

    public int getUserID() {

        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if (prefs != null) {
            user_login = prefs.getInt("id", 0);

        }
        Log.i("QP", "profile id : " + user_login);
        return user_login;
    } // function get the id of user to send it to api

    public String getUserToken() {

        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if (prefs != null) {
            token = prefs.getString("token", "");

        }
        Log.e("QS", "toolbar : token" + token);
        return token;
    } // function get the token of user to send it to api

    public void notification() {

        Log.i("QP", "notification FUNCTION");
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        pusher = new Pusher("2f846111e977afd34a83", options);
        channel = pusher.subscribe("my-channel");

        channel.bind("my-event", new SubscriptionEventListener() {

            @Override
            public void onEvent(String channelName, String eventName, final String data) {

                try {

                    jsonObject = new JSONObject(data);
                    user_id = jsonObject.getInt("user_id");

                    Log.i("QP", "notification count   : " + count_number);

                    try {

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.i("EER", user_id + " : " + user_login);
                                if (user_id == user_login) {

                                    try {

                                        count_number = Integer.parseInt(jsonObject.getString("count"));
                                        badge_notification.setNumber(count_number);
                                        ShortcutBadger.applyCount(context, count_number); //for 1.1.4+
                                        img_notifications.setAnimation(shake);

                                        if (flag.equals("notification")) {
                                            if (!(context).isFinishing()) {
                                                getNotifications();
                                            }

                                        }
                                        Log.i("QP", "notification count   : " + count_number);
                                        Log.i("EER", "count : " + count_number);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // Stuff that updates the UI
                            }
                        });
                    } catch (Exception e) {
                        Log.i("QP", "notification exception   : " + e.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });

        pusher.connect();

        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent notification = new Intent(context, NotificationsActivity.class);
                context.startActivity(notification);
                //context.finish();
            }
        }); // intent to notification activity

    } // function handle notification icon on toolbar

    public void addNotification() {
        shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        Log.i("QP", "add counter : " + c);
        badge_notification.setNumber(count_number);
        ShortcutBadger.applyCount(context, count_number); //for 1.1.4+

    }

    public void getNotifications() {

        if (arrayList.size() > 0) arrayList.clear();

        progress = new CustomDialogProgress();
        progress.init(cthis);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final NotificationApi notificationApi = retrofit.create(NotificationApi.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.showNotifications(getUserID());

                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            response.body();

                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                arrayList = response.body().getData();
                                initRecyclerView();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // show notifications

    private void initRecyclerView() {

        LinearLayoutManager layoutManager;
        NotificationstAdapter adapter;
        RecyclerView notification_recycler_view = context.findViewById(R.id.notification_recycler_view);
        layoutManager = new LinearLayoutManager(context);
        adapter = new NotificationstAdapter(context, arrayList);
        notification_recycler_view.setHasFixedSize(true);
        notification_recycler_view.setLayoutManager(layoutManager);
        notification_recycler_view.setAdapter(adapter);

    } // initialize recycler view

    public int getPrefUserId() {
        SharedPreferences preferences = context.getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        } catch (Exception e) {
            Log.i("QP", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId


} // class of tool_bar
