package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.SearchMenuResponse;
import com.invetechs.mamamiauserversion.View.Activity.LoginActivity;
import com.invetechs.mamamiauserversion.View.Activity.MenuItemDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class OfferAndSearchMenuAdapter extends RecyclerView.Adapter<OfferAndSearchMenuAdapter.MyViewHolder>
{

    Context context;
    List<SearchMenuResponse.DataBean> menuList ;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    double totalDelivery ;
    String name="";
    Activity activity;
    SharedPreferences prefs ;
    int cartKitchenId =0;

    public OfferAndSearchMenuAdapter(Context context,  List<SearchMenuResponse.DataBean> menuList,Activity activity)
    {
        this.context = context;
        this.menuList = menuList;
        this.activity = activity;
        toolBarConfig = new ToolBarConfig(activity,"none");
    }
    ToolBarConfig toolBarConfig ;
    CartModel cartModel;
    ShowDialog toastDialog = new ShowDialog();

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.i_name)
        TextView itemName ;
        @BindView(R.id.i_description)
        TextView itemDescription ;
        @BindView(R.id.i_price)
        TextView itemPrice ;
        @BindView(R.id.i_branch)
        TextView itemBranch ;
        @BindView(R.id.i_image)
        ImageView itemImage ;

        @BindView(R.id.cart_menu_item)
        ImageButton cart_menu_item;

        Dialog cartDialog ;
        Button cancelCardtDialog , addCart  ;
        TextView   textTitle , textPrice ;
        ImageView  imageItem ;
        EditText ed_notes;
        ElegantNumberButton buttonNumber;

        Dialog dialog;
        Button yes , no ;
        LinearLayout layout , layout2;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            cartDialog = new Dialog(context);
            cartDialog.setContentView(R.layout.dialog_card);

            cancelCardtDialog = cartDialog.findViewById(R.id.cancelCartDialog);
            addCart = cartDialog.findViewById(R.id.addCartDialog);
            buttonNumber = cartDialog.findViewById(R.id.number_button);

            ed_notes = cartDialog.findViewById(R.id.ed_notes);
            textTitle = cartDialog.findViewById(R.id.nameMenu_dialog);
            textPrice = cartDialog.findViewById(R.id.priceMenu_dialog);
            imageItem = cartDialog.findViewById(R.id.imageMenu_dialog);

            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.confirm_remove_cart);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_menu_meal,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position)
    {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.itemName.setTypeface(font);
            holder.itemBranch.setTypeface(font);
            holder.itemPrice.setTypeface(font);
            holder.itemDescription.setTypeface(font);
            holder.itemName.setText(menuList.get(position).getAr_title_meal());
            holder.itemBranch.setText(menuList.get(position).getAr_title_kitchen());
            holder.textTitle.setTypeface(font);
            holder.textPrice.setTypeface(font);

            String arDes = "";
            if(menuList.get(position).getAr_description().length() > 15)
            {
                for(int i=0;i<15;i++)
                {
                    if(i == 15)
                        arDes = arDes + "...";
                    else
                    arDes = arDes + menuList.get(position).getAr_description().charAt(i);
                }
            } // if description exceed 15 chars
            else
            {
                arDes = menuList.get(position).getAr_description();
            }
            holder.itemDescription.setText("("+arDes+")");

            String order_name = menuList.get(position).getAr_title_meal();
            name = "";
            for (int i = 0; i < 20; i++) {
                if (order_name != null)
                    if (order_name.length() > 20) {
                        if (i == 19) {
                            name = name + "...";
                        } else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 20) {
                        name = order_name;
                    }
            }

        } // if language arabic
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.itemName.setTypeface(font);
            holder.itemBranch.setTypeface(font);
            holder.itemPrice.setTypeface(font);
            holder.itemDescription.setTypeface(font);
            holder.itemName.setText(menuList.get(position).getEn_title_meal());
            holder.itemBranch.setText(menuList.get(position).getEn_title_kitchen());
            holder.textTitle.setTypeface(font);
            holder.textPrice.setTypeface(font);

            String arDes = "";
            if(menuList.get(position).getAr_description().length() > 15)
            {
                for(int i=0;i<15;i++)
                {
                    if(i == 15)
                        arDes = arDes + "...";
                    else
                        arDes = arDes + menuList.get(position).getEn_description().charAt(i);
                }
            } // if description exceed 15 chars
            else
            {
                arDes = menuList.get(position).getAr_description();
            }
            holder.itemDescription.setText("("+arDes+")");

            String order_name = menuList.get(position).getEn_title_meal();
            name = "";
            for (int i = 0; i < 20; i++) {
                if (order_name != null)
                    if (order_name.length() > 20) {
                        if (i == 19) {
                            name = name + "...";
                        } else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 20) {
                        name = order_name;
                    }
            }
        } // if language english



        holder.itemPrice.setText(menuList.get(position).getPrice()+" "+context.getResources().getString(R.string.sr));

        final int itemId = menuList.get(position).getMeal_id();

        String imageUrl = "https://mammamiaa.com/cpanel/upload/meal/" + menuList.get(position).getImage();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.pizza_back);
        requestOptions.error(R.drawable.pizza_back);


        if (!imageUrl.equals(null))
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(imageUrl)
                .into(holder.itemImage);

        //dialog

        holder.textTitle.setText(name);
        holder.textPrice.setText(menuList.get(position).getPrice() + " " + context.getResources().getString(R.string.sr));

        RequestOptions requestOptionss = new RequestOptions();
        requestOptions.placeholder(R.drawable.pizza_back);
        requestOptions.error(R.drawable.pizza_back);

        if (!imageUrl.equals(null))
        Glide.with(context)
                .setDefaultRequestOptions(requestOptionss)
                .load(imageUrl)
                .into(holder.imageItem);

        holder.buttonNumber.setRange(1,99);

        totalDelivery = Math.round(menuList.get(position).getDistance()) * menuList.get(position).getDeliverypriceperkilo();
        menuList.get(position).setTotalDelivery(totalDelivery);

        holder.buttonNumber.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                holder.textPrice.setText(""+Integer.parseInt(menuList.get(position).getPrice())  * newValue+" "+context.getString(R.string.sr));
            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MenuItemDetails.class);
                Log.i("QP","id : adapter"+menuList.get(position).getMeal_id());
                intent.putExtra("id", menuList.get(position).getMeal_id());
                intent.putExtra("kitchenId", menuList.get(position).getKitchen_id());
                intent.putExtra("maxbill", menuList.get(position).getDelivermaxbill());
                intent.putExtra("deliveryPerKilo", menuList.get(position).getTotalDelivery());
                intent.putExtra("distanceToUser", menuList.get(position).getDistance());
                view.getContext().startActivity(intent);

            }
        });

        holder.cart_menu_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(toolBarConfig.getPrefUserId() != 0) {
                    prefs = context.getSharedPreferences("cart", MODE_PRIVATE);
                    if (prefs != null)
                        cartKitchenId = prefs.getInt("kitchenId", 0);

                    if (cartKitchenId == menuList.get(position).getKitchen_id() || cartKitchenId == 0) {
                        holder.cartDialog.show();
                    } // user not have on cart product from another kitchen
                    else {
                        holder.layout.setBackgroundResource(R.color.white);
                        holder.layout2.setBackgroundResource(R.color.white);
                        holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        holder.dialog.getWindow()
                                .setLayout((int) (getScreenWidth(activity) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                        holder.dialog.show();
                        Log.i("QP", "clearCart : ");
                    }
                } // user login
                else
                {
                    Intent intent = new Intent(context,LoginActivity.class);
                    context.startActivity(intent);
                } // user not login

            }
        });

        holder.cancelCardtDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.cartDialog.dismiss();

            }
        }); // cancel buttoon



        holder.addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                saveCartLocation();


                       editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
                       editor.putInt("kitchenId", menuList.get(position).getKitchen_id());
                       editor.putInt("maxbill", menuList.get(position).getDelivermaxbill());
                       editor.putFloat("deliveryPerKilo", (float) menuList.get(position).getTotalDelivery());
                       editor.putFloat("distanceToUser", (float) menuList.get(position).getDistance());
                       editor.apply();


                       //cart dialog
                       String enTitle = menuList.get(position).getEn_title_meal();
                       String arTitle = menuList.get(position).getAr_title_meal();
                       double price = Double.parseDouble(menuList.get(position).getPrice());
                       String notes = holder.ed_notes.getText().toString();
                       int amount = Integer.parseInt(holder.buttonNumber.getNumber());
                       int itemID = menuList.get(position).getMeal_id();

                       if (notes.equals(null) || notes.isEmpty()) notes = "none";

                       Log.i("QP", "offer cart : \n"
                               + "itmeId : " + itemID + "\n"
                               + "itmeId : " + itemID + "\n"
                               + "enTitle : " + enTitle + "\n"
                               + "arTitle : " + arTitle + "\n"
                               + "notes : " + notes + "\n"
                               + "amount : " + amount + "\n"
                               + "maxbill : " + menuList.get(position).getDelivermaxbill() + "\n"
                               + "kitchenId : " + menuList.get(position).getKitchen_id() + "\n");


                       cartModel = new CartModel(itemID, enTitle, arTitle, notes, amount, price, menuList.get(position).getKitchen_id());
                       toolBarConfig.addCart(cartModel);


                       toastDialog.initDialog(context.getString(R.string.itmeAdded), context);
                       holder.cartDialog.dismiss();




            }
        });

        holder.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
                toolBarConfig.clearCart();
            }
        });

        holder.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
            }
        });
    }

    private void saveCartLocation ()
    {
        SharedPreferences prefs = context.getSharedPreferences("AddressPref", MODE_PRIVATE);
        String   address = prefs.getString("address","");
        float  lat = prefs.getFloat("lat",0);
        float  lng = prefs.getFloat("lng",0);

        SharedPreferences.Editor prefEditor =  context.getSharedPreferences("AddressPrefCart", MODE_PRIVATE).edit();
        prefEditor.putString("address",address);
        prefEditor.putFloat("lat",lat);
        prefEditor.putFloat("lng",lng);
        prefEditor.apply();
    } // function of saveCartLocation

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

}
