package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.invetechs.mamamiauserversion.Model.ExtraModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MealDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RestaurantDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class ExtraAdapter extends RecyclerView.Adapter<ExtraAdapter.MyViewHolder> {

    // variables

    Context context;
    private List<RestaurantDetailsResponse.DataBean.MealBean.AdditionsBean> additionsBeans;
    List<RestaurantDetailsResponse.DataBean.MealBean> menuList = new ArrayList<>();

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<ExtraModel> addiationsList = new ArrayList<>();
    ExtraModel extraModelArrayList;
    ArrayList<Integer> listOfId = new ArrayList<>();
    int sum;
    String recieve;
    MenuAdapter.MyViewHolder menuAdapter;
    TextView value;
    private List<MealDetailsResponse.AdditionsBean> additionsBeansmenu = new ArrayList<>();


    public ExtraAdapter(Context context, List<RestaurantDetailsResponse.DataBean.MealBean.AdditionsBean> additionsBeans, String recieve, TextView textPrice) {
        this.context = context;
        this.additionsBeans = additionsBeans;
        this.recieve = recieve;
        value = textPrice;
        sum = 0;
        extraModelArrayList = ExtraModel.getInstance();
        extraModelArrayList.setSum(Integer.parseInt(recieve));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_add_extras, parent, false);
        return new ExtraAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.extra_title_checkBox.setTypeface(font);
            holder.extra_salary.setTypeface(font);
            holder.extra_title_checkBox.setText(additionsBeans.get(position).getAr_title());
            holder.extra_salary.setText(additionsBeans.get(position).getPrice() + " " + context.getString(R.string.sr));
            Log.i("QP", "extra_title : " + additionsBeans.get(position).getAr_title());
            Log.i("QP", "extra_salary : " + additionsBeans.get(position).getPrice());

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.extra_title_checkBox.setTypeface(font);
            holder.extra_salary.setTypeface(font);
            holder.extra_title_checkBox.setText(additionsBeans.get(position).getEn_title());
            holder.extra_salary.setText(additionsBeans.get(position).getPrice() + " " + context.getString(R.string.sr));
            Log.i("QP", "extra_title : " + additionsBeans.get(position).getEn_title());

        }

        // when click check box
        holder.extra_title_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    // pricesArray.add(Integer.parseInt(additionsBeans.get(position).getPrice()));
                    sum += Integer.parseInt(additionsBeans.get(position).getPrice());

                    listOfId.add(additionsBeans.get(position).getId());

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(listOfId);
                    editor.putString("Additions", json);
                    editor.apply();     // This line is IMPORTANT !

                    Log.i("QP", "listOfId =  : " + listOfId.size());

                    additionsBeans.get(position).setChecked(true);

                    Log.i("QP", " sum =  : " + sum);
                    Log.i("QP", "recieve : " + recieve);
                    // recieve = recieve * extraModelArrayList.getItemCounts();
                    extraModelArrayList.setSum(sum
                            + Integer.valueOf(recieve));

                    Log.i("QP", "set sum =  : " + String.valueOf(sum + Integer.valueOf(recieve)));
                    Log.i("QP", "recieve length =  : " + String.valueOf(sum + Integer.valueOf(recieve)));

                    value.setText(String.valueOf(sum * extraModelArrayList.getItemCounts() + Integer.valueOf(recieve) *
                            extraModelArrayList.getItemCounts()) + " " + context.getString(R.string.sr));

                    Log.i("QP", "price addition value =  : " + sum);

//                    extraModelArrayList.setId(additionsBeans.get(position).getId());
//                    extraModelArrayList.setPrice(additionsBeans.get(position).getPrice());

                    Log.i("QP", "check id : " + extraModelArrayList.getId());
                    Log.i("QP", "check price : " + extraModelArrayList.getPrice());

                } // checked

                else {

                    for (int i = 0; i < listOfId.size(); i++) {
                        if (listOfId.get(i).equals(additionsBeans.get(position).equals(true))) {
                            listOfId.remove(i);
                        }
                    }

                    additionsBeans.get(position).setChecked(false);

                    sum = sum - Integer.parseInt(additionsBeans.get(position).getPrice());
                    addiationsList.remove(extraModelArrayList);
                    Log.i("QP", "uncheck extraModelArrayList : " + extraModelArrayList);
                    Log.i("QP", "uncheck addiationsList : " + addiationsList.size());
                    Log.i("QP", "recieve in uncheck : " + recieve);
                    extraModelArrayList.setSum(sum
                            + Integer.valueOf(recieve));
                    value.setText(String.valueOf(sum * extraModelArrayList.getItemCounts() +
                            Integer.valueOf(recieve) * extraModelArrayList.getItemCounts()) + " " + context.getString(R.string.sr));


                } // not checked
            }
        });
    }

    @Override
    public int getItemCount() {
        return additionsBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.extra_title_checkBox)
        CheckBox extra_title_checkBox;

        @BindView(R.id.extra_salary)
        TextView extra_salary;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
