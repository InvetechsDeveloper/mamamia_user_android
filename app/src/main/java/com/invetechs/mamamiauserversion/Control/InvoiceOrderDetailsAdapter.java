package com.invetechs.mamamiauserversion.Control;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Model.OrderDetailsModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrderDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvoiceOrderDetailsAdapter extends RecyclerView.Adapter<InvoiceOrderDetailsAdapter.MyViewHolder> {

    // vars
    Context context;
    private List<OrderDetailsResponse.OneorderBean.MealsBean> orderDetailsArrayList = new ArrayList<>();

    public InvoiceOrderDetailsAdapter(Context context, List<OrderDetailsResponse.OneorderBean.MealsBean> orderDetailsArrayList) {
        this.context = context;
        this.orderDetailsArrayList = orderDetailsArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_details_item,parent,false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


//        holder.tv_order_name.setText(getPosition.getOrder_name());
//        holder.tvOrderNum.setText(getPosition.getOrder_num());

    } // on bind function

    @Override
    public int getItemCount() {
        return orderDetailsArrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_num)
        TextView tvOrderNum;

        @BindView(R.id.tv_order_name)
        TextView tv_order_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    } // view holder class
}
