package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.Model.ExtraModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RestaurantDetailsResponse;
import com.invetechs.mamamiauserversion.View.Activity.LoginActivity;
import com.invetechs.mamamiauserversion.View.Activity.MenuItemDetails;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    //vars
    SharedPreferences sharedPreferences;
    SharedPreferences prefs;

    SharedPreferences.Editor editor;
    Context context;
    ArrayList<String> arrayList;
    String name = "", desc = "";
    ToolBarConfig toolBarConfig;
    CartModel cartModel;
    ShowDialog toastDialog = new ShowDialog();
    Activity activity;
    ExtraAdapter extraAdapter;
    String send = "";
    List<RestaurantDetailsResponse.DataBean.MealBean> menuList = new ArrayList<>();
    private List<RestaurantDetailsResponse.DataBean.MealBean.AdditionsBean> additionsBeans = new ArrayList<>();
    ExtraModel extraModelArrayList;

    public int itemCount = 1;
    int kitchenId, deliveryMaxBill;

    double distanceToUser = 0, totalDelivery;
    int sum;

    public MenuAdapter() {
    }

    public MenuAdapter(Context context, List<RestaurantDetailsResponse.DataBean.MealBean> menuList, Activity activity,
                       int kitchenId, int deliveryMaxBill, double totalDelivery, double distanceToUser) {
        this.context = context;
        this.menuList = menuList;
        this.activity = activity;
        toolBarConfig = new ToolBarConfig(activity, "none");
        this.kitchenId = kitchenId;
        this.deliveryMaxBill = deliveryMaxBill;
        this.totalDelivery = totalDelivery;
        this.distanceToUser = distanceToUser;
        extraModelArrayList = ExtraModel.getInstance();
    } // constructor

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView_menuItem)
        ImageView imageView_menuItem;

        @BindView(R.id.txt_nameMenuItem)
        TextView txt_nameMenuItem;

        @BindView(R.id.txt_desMenuItem)
        TextView txt_desMenuItem;

        @BindView(R.id.txt_costMenuItem)
        TextView txt_costMenuItem;

        @BindView(R.id.cart_menu_item)
        Button cart_menu_item;

        Dialog cartDialog;
        Button cancelCardtDialog, addCart;
        public TextView textTitle, textPrices;
        ImageView imageItem;
        EditText ed_notes;
        ElegantNumberButton buttonNumber;
        RecyclerView extra_recycler_view;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cartDialog = new Dialog(context);
            cartDialog.setContentView(R.layout.dialog_card);
            cartDialog.setCancelable(false);
            cancelCardtDialog = cartDialog.findViewById(R.id.cancelCartDialog);
            addCart = cartDialog.findViewById(R.id.addCartDialog);
            buttonNumber = cartDialog.findViewById(R.id.number_button);

            ed_notes = cartDialog.findViewById(R.id.ed_notes);
            textTitle = cartDialog.findViewById(R.id.nameMenu_dialog);
            textPrices = cartDialog.findViewById(R.id.priceMenu_dialog);
            imageItem = cartDialog.findViewById(R.id.imageMenu_dialog);
            extra_recycler_view = cartDialog.findViewById(R.id.extra_recycler_view);

        }
    } // class MViewHolder

    private void initLanguage() {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    } //initialization language and set font

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.new_item_restaurant_menu, parent, false);
        initLanguage();
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.buttonNumber.setRange(1, 99);
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.txt_nameMenuItem.setTypeface(font);
            holder.txt_desMenuItem.setTypeface(font);
            holder.txt_costMenuItem.setTypeface(font);
            holder.textTitle.setTypeface(font);
            holder.textPrices.setTypeface(font);
            holder.ed_notes.setTypeface(font);
            holder.textTitle.setTypeface(font);
            holder.addCart.setTypeface(font);
            holder.cancelCardtDialog.setTypeface(font);

            String order_name = menuList.get(position).getAr_title();
            String order_desc = menuList.get(position).getAr_description();
            name = "";
            desc = "";

            for (int i = 0; i < 20; i++) {
                if (order_name != null)
                    if (order_name.length() > 20) {
                        if (i == 19) {
                            name = name + "...";
                        } else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 20) {
                        name = order_name;
                    }
            }

            for (int i = 0; i < 20; i++) {
                if (order_desc != null)
                    if (order_desc.length() > 20) {
                        if (i == 19) {
                            desc = desc + "...";
                        } else
                            desc = desc + order_desc.charAt(i);
                    } else if (order_desc.length() <= 20) {
                        desc = order_desc;
                    }
            }

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.txt_nameMenuItem.setTypeface(font);
            holder.txt_desMenuItem.setTypeface(font);
            holder.txt_costMenuItem.setTypeface(font);
            holder.textTitle.setTypeface(font);
            holder.textPrices.setTypeface(font);
            holder.textPrices.setTypeface(font);
            holder.ed_notes.setTypeface(font);
            holder.textTitle.setTypeface(font);
            holder.addCart.setTypeface(font);
            holder.cancelCardtDialog.setTypeface(font);

//            for (array)
//            //if  (array.get(i).equal(additions.getposition.getid == true)
//            {
//                arrayList.remove(i);
//            }
            String order_name = menuList.get(position).getEn_title();
            String order_desc = menuList.get(position).getEn_description();
            name = "";
            desc = "";

            for (int i = 0; i < 20; i++) {
                if (order_name != null)
                    if (order_name.length() > 20) {
                        if (i == 19) {
                            name = name + "...";
                        } else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 20) {
                        name = order_name;
                    }
            }

            for (int i = 0; i < 20; i++) {
                if (order_desc != null)
                    if (order_desc.length() > 20) {
                        if (i == 19) {
                            desc = desc + "...";
                        } else
                            desc = desc + order_desc.charAt(i);
                    } else if (order_desc.length() <= 20) {
                        desc = order_desc;
                    }
            }
        }

        holder.txt_nameMenuItem.setText(name);
        holder.txt_desMenuItem.setText(desc);
        holder.txt_costMenuItem.setText(menuList.get(position).getPrice() + " " + context.getResources().getString(R.string.sr));
        String imageUrl = "https://mammamiaa.com/cpanel/upload/meal/" + menuList.get(position).getImage();

        Log.i("QP", "thing : " + menuList.get(position).getAdditions().size());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.pizza_back);
        requestOptions.error(R.drawable.pizza_back);

        if (!menuList.get(position).getImage().equals(null))
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(imageUrl)
                    .into(holder.imageView_menuItem);

        // dialog ....................................................

        holder.textTitle.setText(name);
        holder.textPrices.setText(menuList.get(position).getPrice() + " " + context.getResources().getString(R.string.sr));

        if (!imageUrl.equals(null))
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(imageUrl)
                    .into(holder.imageItem);

        holder.buttonNumber.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                // code

                extraModelArrayList = ExtraModel.getInstance();
                int a = extraModelArrayList.getId();
                String b = extraModelArrayList.getPrice();
                extraModelArrayList.setItemCounts(newValue);
                itemCount = newValue;

                try {
                    sum = extraModelArrayList.getSum();
                    Log.i("QP", "get sum : " + sum);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "sum exception : " + e.getMessage());
                }

                Log.i("QP", "a : " + a);
                Log.i("QP", "b : " + b);
                //  send = "" + Integer.parseInt(menuList.get(position).getPrice()) + " " + context.getString(R.string.sr);

                Log.i("QP", "send : " + send);
                //holder.textPrice.setText("" + Integer.parseInt(menuList.get(position).getPrice()) * newValue + " " + context.getString(R.string.sr));

                if (sum != -1) {
                    holder.textPrices.setText(sum * newValue + " " + context.getString(R.string.sr));
                } else {
                    holder.textPrices.setText("" + Integer.parseInt(menuList.get(position).getPrice()) * newValue + " " + context.getString(R.string.sr));
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MenuItemDetails.class);
                intent.putExtra("id", menuList.get(position).getId());
                intent.putExtra("kitchenId", kitchenId);
                intent.putExtra("maxbill", deliveryMaxBill);
                intent.putExtra("deliveryPerKilo", totalDelivery);
                intent.putExtra("distanceToUser", distanceToUser);
                view.getContext().startActivity(intent);
                Log.i("QT", "MI delivery : " + totalDelivery + " : distance : " + distanceToUser);

            }
        });

        holder.cart_menu_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  holder.textPrice.setText(menuList.get(position).getPrice() + " " + context.getResources().getString(R.string.sr));
                extraModelArrayList.setItemCounts(1);
                send = "" + Integer.parseInt(menuList.get(position).getPrice());
                holder.buttonNumber.setNumber("1");
                extraModelArrayList.setPrice(send);
                extraModelArrayList.setSum(Integer.parseInt(send));

                Log.i("QP", "send when click : " + send);
                Log.i("QP", "array addition : " + additionsBeans);

                additionsBeans = menuList.get(position).getAdditions();

                Log.i("QP", "integer : " + additionsBeans);

                extraAdapter = new ExtraAdapter(context, additionsBeans, send, holder.textPrices);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                holder.extra_recycler_view.setLayoutManager(layoutManager);
                holder.extra_recycler_view.setHasFixedSize(true);
                holder.extra_recycler_view.setAdapter(extraAdapter);

                if (toolBarConfig.getPrefUserId() != 0) {

                    holder.cartDialog.show();
                    holder.textPrices.setText(menuList.get(position).getPrice());

                } // user login
                else {
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                } // user not login

            }
        });

        holder.cancelCardtDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (editor != null) {
                        editor.remove("Additions");
                        editor.commit();
                        editor.apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "exception : " + e.getMessage());

                }

                extraModelArrayList.setItemCounts(1);
                holder.cartDialog.cancel();

            }
        }); // cancel buttoon

        Log.i("QW", "menu dapter : " + totalDelivery);

        holder.addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get shared preference ids
                prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                Gson gson = new Gson();
                String json = prefs.getString("Additions", null);
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();

                Log.i("QP", " additions arrray list in shared preference =  : " + gson.fromJson(json, type));

                extraModelArrayList.setItemCounts(1);

                saveCartLocation();
                editor = context.getSharedPreferences("cart", context.MODE_PRIVATE).edit();
                editor.putInt("kitchenId", kitchenId);
                editor.putInt("maxbill", deliveryMaxBill);
                editor.putFloat("deliveryPerKilo", (float) totalDelivery);
                editor.putFloat("distanceToUser", (float) distanceToUser);
                editor.apply();
                Log.i("QP", "MenuAdapter : " + kitchenId);

                //cart dialog
                String enTitle = menuList.get(position).getEn_title();
                String arTitle = menuList.get(position).getAr_title();
                double price;
                sum = extraModelArrayList.getSum();

                if (sum != -1) {
                    price = sum;
                    Log.i("QP", "if != -1 price sending to cart : " + price);

                } else {
                    price = Integer.parseInt(menuList.get(position).getPrice());
                    Log.i("QP", "else price sending to cart : " + price);
                }

                Log.i("QP", "cartModel price sending to cart : " + price);

                String notes = holder.ed_notes.getText().toString();
                int amount = itemCount;
                int itemID = menuList.get(position).getId();

                if (notes.equals(null) || notes.isEmpty()) notes = "none";

                cartModel = new CartModel(itemID, enTitle, arTitle, notes, amount, price, kitchenId, (ArrayList<Integer>) gson.fromJson(json, type));

                Log.i("QP", "cartModel : " + cartModel);

                toolBarConfig.addCart(cartModel);
                toastDialog.initDialog(context.getString(R.string.itmeAdded), context);
                holder.cartDialog.dismiss();

                try {
                    if (editor != null) {
                        editor.remove("Additions");
                        editor.commit();
                        editor.apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "exception : " + e.getMessage());

                }


            }
        });


    }

    private void saveCartLocation() {
        SharedPreferences prefs = context.getSharedPreferences("AddressPref", MODE_PRIVATE);
        String address = prefs.getString("address", "");
        float lat = prefs.getFloat("lat", 0);
        float lng = prefs.getFloat("lng", 0);

        SharedPreferences.Editor prefEditor = context.getSharedPreferences("AddressPrefCart", MODE_PRIVATE).edit();
        prefEditor.putString("address", address);
        prefEditor.putFloat("lat", lat);
        prefEditor.putFloat("lng", lng);
        prefEditor.apply();


        SharedPreferences m = context.getSharedPreferences("AddressPrefCart", MODE_PRIVATE);
        Log.i("QP", "Adapter Address : " + m.getString("address", ""));
    } // function of saveCartLocation

    @Override
    public int getItemCount() {
        return menuList.size();
    }


}

