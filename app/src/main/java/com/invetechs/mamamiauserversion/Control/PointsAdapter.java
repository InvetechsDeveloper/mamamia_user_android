package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Model.PointsModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.UserPointsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.MyViewHolder> {

    //vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    List<UserPointsResponse.DataBean> pointsList;

    public PointsAdapter(Context context,  List<UserPointsResponse.DataBean> pointsList) {
        this.context = context;
        this.pointsList = pointsList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.point_item,parent,false);
        initLanguage();
        return new MyViewHolder(view);

    } //onCreateView Function

    private void initLanguage()
    {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    } //initialization language and set font


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        String restaurantTitle = "";
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_res.setTypeface(font);
            holder.tv_points.setTypeface(font);
            holder.tv_money.setTypeface(font);
            restaurantTitle = pointsList.get(position).getAr_title();
        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_res.setTypeface(font);
            holder.tv_points.setTypeface(font);
            holder.tv_money.setTypeface(font);
            restaurantTitle = pointsList.get(position).getAr_title();

            restaurantTitle = pointsList.get(position).getEn_title();
        }
        holder.tv_res.setText(restaurantTitle);
        holder.tv_points.setText(pointsList.get(position).getPoints()+"");
        holder.tv_money.setText(pointsList.get(position).getDiscount());


    } //onBindView Function

    @Override
    public int getItemCount() {
        return pointsList.size();
    } // getItem count function


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_res)
        TextView tv_res;

        @BindView(R.id.tv_points)
        TextView tv_points;

        @BindView(R.id.tv_money)
        TextView tv_money;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    } //view holder function
}
