package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Model.InvoiceModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.InvoiceResponse;
import com.invetechs.mamamiauserversion.View.Activity.InvoiceDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    // vars
    Context context;
    private List<InvoiceResponse.ArchivesBean> archiveArrayList;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public InvoiceAdapter(Context context, List<InvoiceResponse.ArchivesBean> archivesBeans) {
        this.context = context;
        this.archiveArrayList = archivesBeans;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.invoice_item, parent, false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();


        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_date.setText(archiveArrayList.get(position).getAr_orderDate());
            holder.order_num_title.setTypeface(font);
            holder.date_title.setTypeface(font);
            holder.total_title.setTypeface(font);
            holder.tvOrderNum.setTypeface(font);
            holder.totaly_payment.setTypeface(font);
            holder.tv_date.setTypeface(font);

            holder.order_num_title.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_date.setText(archiveArrayList.get(position).getEn_orderDate());
            holder.order_num_title.setTypeface(font);
            holder.date_title.setTypeface(font);
            holder.total_title.setTypeface(font);
            holder.tvOrderNum.setTypeface(font);
            holder.totaly_payment.setTypeface(font);
            holder.tv_date.setTypeface(font);

        }

        holder.tvOrderNum.setText(String.valueOf(archiveArrayList.get(position).getId()));
        holder.totaly_payment.setText(archiveArrayList.get(position).getTotalprice());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent order_details = new Intent(context, InvoiceDetails.class);
                order_details.putExtra("invoiceId",String.valueOf(archiveArrayList.get(position).getId()));
                context.startActivity(order_details);
            }
        });

    } // on bind function

    @Override
    public int getItemCount() {
        return archiveArrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_num)
        TextView tvOrderNum;

        @BindView(R.id.date_title)
        TextView date_title;

        @BindView(R.id.total_title)
        TextView total_title;

        @BindView(R.id.order_num_title)
        TextView order_num_title;

        @BindView(R.id.tv_date)
        TextView tv_date;

        @BindView(R.id.totaly_payment)
        TextView totaly_payment;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // view holder class
}
