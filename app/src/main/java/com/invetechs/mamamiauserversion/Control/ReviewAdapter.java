package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.ReviewResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    // vars
    Context context;
    List<ReviewResponse.DataBean> reviewList ;
    SharedPreferences sharedPreferences;
    String name ="" , review_name = "";

    public ReviewAdapter(Context context,   List<ReviewResponse.DataBean> reviewList) {
        this.context = context;
        this.reviewList = reviewList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.review_item,parent,false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_name.setTypeface(font);
            holder.tv_date.setTypeface(font);
            holder.tv_description.setTypeface(font);
            holder.tx_rate_num.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_name.setTypeface(font);
            holder.tv_date.setTypeface(font);
            holder.tv_description.setTypeface(font);
            holder.tx_rate_num.setTypeface(font);
        }



         review_name = reviewList.get(position).getName();

        name = "";

        for (int i = 0 ; i < 20 ; i++)
        {
            if (review_name != null)
                if(review_name.length() > 20)
                {
                    if(i == 19)
                        name = name + "..." ;
                    else
                        name = name + review_name.charAt(i);
                }

                else if (review_name.length() <= 20)
                {
                    name = review_name;

                }
        }

        //Log.i("QP","name : "+name+" : "+"\tdate : "+reviewList.get(position).getCreated_at()+"\n");
        holder.tv_name.setText(name);

        holder.tv_date.setText(reviewList.get(position).getCreated_at());
        holder.tv_description.setText(reviewList.get(position).getReview());

        holder.tx_rate_num.setText(reviewList.get(position).getRate()+"");
        holder.rateBar.setRating(reviewList.get(position).getRate());
    } // on bind function

    @Override
    public int getItemCount() {
        return reviewList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tv_name;

        @BindView(R.id.tv_date)
        TextView tv_date;

        @BindView(R.id.tv_description)
        TextView tv_description;

        @BindView(R.id.rateBar)
        AppCompatRatingBar rateBar;

        @BindView(R.id.tx_rate_num)
        TextView tx_rate_num;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    } // view holder class
}
