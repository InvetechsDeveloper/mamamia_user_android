package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.MyBanquetOfferApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.BanquetOfferResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.OrdersAndOffersDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class MyBanquetOffersAdapter extends RecyclerView.Adapter<MyBanquetOffersAdapter.MyViewHolder> {

    // vars
    Context context;
    List<BanquetOfferResponse.DataBean.OffersBean> arrayList;

    //ProgressDialog
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences sharedPreferences;
    ShowDialog toastDialog = new ShowDialog();

    public MyBanquetOffersAdapter(Context context, List<BanquetOfferResponse.DataBean.OffersBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_banquet_offer_item,parent,false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_offer_name.setTypeface(font);
            holder.tv_offer_price.setTypeface(font);
            holder.btn_accept.setTypeface(font);
            holder.btn_refuse.setTypeface(font);
            holder.yes.setTypeface(font);
            holder.no.setTypeface(font);
            holder.tv_show.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_offer_name.setTypeface(font);
            holder.tv_offer_price.setTypeface(font);
            holder.btn_accept.setTypeface(font);
            holder.btn_refuse.setTypeface(font);
            holder.yes.setTypeface(font);
            holder.no.setTypeface(font);
            holder.tv_show.setTypeface(font);
        }


        holder.tv_offer_name.setText(arrayList.get(position).getAr_title());
        holder.tv_offer_price.setText(arrayList.get(position).getTotalprice()+" $ ");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent intent = new Intent(context,OrdersAndOffersDetails.class);
               context.startActivity(intent);
            }
       });

        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptBanquetOfferApi(arrayList.get(position).getCodeorder(),arrayList.get(position).getKitchen_id());
            }
        }); // accept banquet odder

        holder.btn_refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.layout.setBackgroundResource(R.color.white);
                holder.layout2.setBackgroundResource(R.color.white);
                holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                holder.dialog.getWindow()
                        .setLayout((int) (getScreenWidth((Activity) view.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.dialog.show();
            }
        }); // refuse banquet offer

        holder.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
                refuseBanquetOfferApi(arrayList.get(position).getCodeorder(),arrayList.get(position).getKitchen_id(),position);
            }
        });

        holder.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OrdersAndOffersDetails.class);
                intent.putExtra("id",String.valueOf(arrayList.get(position).getId())); // send id to get order details
                Log.i("QP", "id send from adapter : " + arrayList.get(position).getId());
                intent.putExtra("status","offer");
                context.startActivity(intent);
            }
        });

    } // on bind function

    @Override
    public int getItemCount() {
        return arrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_offer_name)
        TextView tv_offer_name;

        @BindView(R.id.tv_show)
        TextView tv_show;

        @BindView(R.id.tv_offer_price)
        TextView tv_offer_price;

        @BindView(R.id.btn_accept)
        Button btn_accept;

        Dialog dialog;
        Button yes , no ;
        @BindView(R.id.btn_refuse)
        Button btn_refuse;

        LinearLayout layout , layout2;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.refuse_banquet_offer_dialog);

            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
        }
    } // view holder class

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void acceptBanquetOfferApi(final String orderCode, final int kitchenId)
    {
        progress = new CustomDialogProgress();
        progress.init(context);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOfferApi banquetOfferApi = retrofit.create(MyBanquetOfferApi.class);

                final Call<BanquetOfferResponse> getInterestConnection = banquetOfferApi.acceptBanquetOffer(orderCode,kitchenId);

                getInterestConnection.enqueue(new Callback<BanquetOfferResponse>() {
                    @Override
                    public void onResponse(Call<BanquetOfferResponse> call, Response<BanquetOfferResponse> response) {
                        try
                        {
                            String code = response.body().getCode();
                            Log.i("QP","code" + code);

                            if (code.equals("200"))
                            {

                                 toastDialog.initDialog(context.getString(R.string.banquetAccepted),context);
                                 arrayList.clear();
                                 notifyDataSetChanged();
                            } //


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BanquetOfferResponse> call, Throwable t) {

                        toastDialog.initDialog(context.getString(R.string.retry),context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function  of acceptBanquetOfferApi

    private void refuseBanquetOfferApi(final String orderCode, final int kitchenId, final int position)
    {
        progress = new CustomDialogProgress();
        progress.init(context);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection((Activity) context);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOfferApi banquetOfferApi = retrofit.create(MyBanquetOfferApi.class);

                final Call<BanquetOfferResponse> getInterestConnection = banquetOfferApi.refuseBanquetOffer(orderCode,kitchenId);

                getInterestConnection.enqueue(new Callback<BanquetOfferResponse>() {
                    @Override
                    public void onResponse(Call<BanquetOfferResponse> call, Response<BanquetOfferResponse> response) {
                        try
                        {
                            String code = response.body().getCode();
                            Log.i("QP","code" + code);

                            if (code.equals("200"))
                            {
                                toastDialog.initDialog(context.getString(R.string.banquetRefused),context);
                                arrayList.remove(position);
                                notifyItemRemoved(position);
                            } //


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BanquetOfferResponse> call, Throwable t) {

                        toastDialog.initDialog(context.getString(R.string.retry),context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function  of refuseBanquetOfferApi
} // class of MyBanquetOffersAdapter
