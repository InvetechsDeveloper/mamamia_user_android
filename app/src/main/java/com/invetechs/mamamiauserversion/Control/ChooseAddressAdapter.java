package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class ChooseAddressAdapter extends RecyclerView.Adapter<ChooseAddressAdapter.MyViewHolder> {

    // vars

    Context context;
    List<AddressResponse.DataBean> addressList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    String flag;
    SharedPreferences.Editor editor;
    Button okChooseAddressButton;
    private int lastSelectedPosition = -1;
    double lat, lng;
    SharedPreferences.Editor editorAddress;
    SharedPreferences.Editor cartAddress;


    public ChooseAddressAdapter(Context context, List<AddressResponse.DataBean> addressList, Button okChooseAddressButton) {

        this.context = context;
        this.addressList = addressList;
        this.okChooseAddressButton = okChooseAddressButton;

    } // constructor of AddressesAdapter

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_title)
        TextView title;

        @BindView(R.id.address_address)
        TextView address;

        @BindView(R.id.rb_chooseAddress)
        RadioButton rb_chooseAddress;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rb_chooseAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    } // class of MyViewHolder

    @NonNull
    @Override
    public ChooseAddressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_chhose_address, parent, false);
        initLanguage();

        return new ChooseAddressAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.title.setTypeface(font);
            holder.address.setTypeface(font);
            holder.rb_chooseAddress.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.title.setTypeface(font);
            holder.address.setTypeface(font);
            holder.rb_chooseAddress.setTypeface(font);
        }


        holder.title.setText(addressList.get(position).getNote());
        holder.address.setText(addressList.get(position).getAddress());


        holder.rb_chooseAddress.setChecked(lastSelectedPosition == position);

        if (holder.rb_chooseAddress.isChecked()) {
            Log.i("QP", "itemChecked : " + addressList.get(position).getId());
            lat = addressList.get(position).getLat();
            lng = addressList.get(position).getLng();
        }
        okChooseAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editorAddress = view.getContext().getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
                editorAddress.putFloat("lat", (float) lat);
                editorAddress.putFloat("lng", (float) lng);
                editorAddress.apply();

                Log.i("QP", "cart address: " + cartAddress);
                Log.i("QP", "lat from adapter choose address: " + lat);
                Log.i("QP", "lng from adapter choose address: " + lng);
                Log.i("QP", "address from adapter choose address: " + addressList.get(position).getAddress());

//                if (flag.equals("cart")) {
//                    Intent intent = new Intent(view.getContext(), MenuActivity.class);
//                    intent.putExtra("fragmentFlag", "cart");
//                    view.getContext().startActivity(intent);
//                    Log.i("QP", "intent with cart flag : " + lat + " : " + lng);
//
//                    cartAddress = view.getContext().getSharedPreferences("EditorCart", MODE_PRIVATE).edit();
//                    cartAddress.putFloat("lat", (float) lat);
//                    cartAddress.putFloat("lng", (float) lng);
//                    cartAddress.putString("address", addressList.get(position).getAddress());
//                    cartAddress.apply();
//
//
//                }

//                else {
                    Log.i("QP", "adapterAddress : " + lat + " : " + lng);
                    Intent intent = new Intent(view.getContext(), MenuActivity.class);
                    intent.putExtra("fragmentFlag", "home");
                    view.getContext().startActivity(intent);
                    Log.i("QP", "intent with out cart flag : " + lat + " : " + lng);

              //  }

            }
        });
    }

    private void initLanguage() {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
        }

    } //initialization language and set font


    @Override
    public int getItemCount() {
        return addressList.size();
    }


} // class of AddressesAdapter




