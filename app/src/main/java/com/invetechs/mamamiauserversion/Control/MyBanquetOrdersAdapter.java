package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.CancelOrder;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MyBanquerOrderResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.OrdersAndOffersDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class MyBanquetOrdersAdapter extends RecyclerView.Adapter<MyBanquetOrdersAdapter.MyViewHolder> {

    // vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    private List<MyBanquerOrderResponse.DataBean> data;
    String code_order = "";
    ProgressDialog progress;
    Handler handler;
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    String order_name , name;

    ShowDialog toastDialog = new ShowDialog();

    public MyBanquetOrdersAdapter(Context context, List<MyBanquerOrderResponse.DataBean> data) {
        this.context = context;
        this.data = data;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_banquet_order_item, parent, false);
        return new MyViewHolder(view);
    } // on create function


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (data.get(position).getStatus().equals("2") || data.get(position).getStatus().equals("3")) {

            code_order = data.get(position).getCodeorder();
            holder.rest_name_title.setText(context.getString(R.string.party_date));
            holder.tv_restaurant_name.setText(data.get(position).getPartydate());
            holder.price_title.setVisibility(View.GONE);
            holder.tv_price.setVisibility(View.GONE);
            holder.tv_order_num.setText(String.valueOf(data.get(position).getCodeorder()));
            holder.btn_order_cancel.setVisibility(View.VISIBLE);
            holder.btn_order_status.setVisibility(View.VISIBLE);
            holder.btn_order_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelOrder(code_order,position);
                }
            });


            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
                holder.rest_name_title.setTypeface(font);
                holder.tv_order_num.setTypeface(font);
                holder.tv_restaurant_name.setTypeface(font);
                holder.price_title.setTypeface(font);
                holder.order_num_title.setTypeface(font);
                holder.tv_price.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_cancel.setTypeface(font);


            } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
                holder.rest_name_title.setTypeface(font);
                holder.tv_restaurant_name.setTypeface(font);
                holder.tv_order_num.setTypeface(font);
                holder.order_num_title.setTypeface(font);
                holder.price_title.setTypeface(font);
                holder.tv_price.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_cancel.setTypeface(font);

            }

        } // fill first item in array list

        else {

            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
                holder.tv_banquet_name.setTypeface(font);
                holder.order_num_title.setTypeface(font);
                holder.tv_order_num.setTypeface(font);
                holder.rest_name_title.setTypeface(font);
                holder.tv_restaurant_name.setTypeface(font);
                holder.price_title.setTypeface(font);
                holder.tv_price.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_cancel.setTypeface(font);

                if (data.get(position) != null)
                    order_name =  data.get(position).getAr_title();
                name = "";

                for (int i = 0 ; i < 18 ; i++)
                {

                    if (order_name != null)
                        if(order_name.length() > 18)
                        {
                            if(i == 17)
                                name = name + "...";
                            else
                                name = name + order_name.charAt(i);
                        }

                        else if (order_name.length() <= 18)
                        {
                            name = order_name;

                        }

                }
                holder.tv_restaurant_name.setText(name);


            }

            else if (sharedPreferences.getString("language", "ar").equals("en")) {
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
                holder.tv_banquet_name.setTypeface(font);
                holder.order_num_title.setTypeface(font);
                holder.tv_order_num.setTypeface(font);
                holder.rest_name_title.setTypeface(font);
                holder.tv_restaurant_name.setTypeface(font);
                holder.price_title.setTypeface(font);
                holder.tv_price.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_status.setTypeface(font);
                holder.btn_order_cancel.setTypeface(font);

                if (data.get(position) != null)

                    order_name =  data.get(position).getEn_title();
                name = "";

                for (int i = 0 ; i < 20 ; i++)
                {

                    if (order_name != null)
                        if(order_name.length() > 20)
                        {
                            if(i == 19)
                                name = name+"...";
                            else
                                name = name + order_name.charAt(i);
                        }

                        else if (order_name.length() <= 20)
                        {
                            name = order_name;

                        }

                }
                holder.tv_restaurant_name.setText(name);

            }


            if (data.get(position).getStatus().equals("1")) {
                holder.tv_order_num.setText(String.valueOf(data.get(position).getId()));
                holder.btn_order_status.setText(R.string.accepted);
                holder.btn_order_cancel.setVisibility(View.GONE);

            }

            else if (data.get(position).getStatus().equals("-4"))
            {
                holder.btn_order_status.setText(R.string.canceled);
                holder.btn_order_cancel.setVisibility(View.GONE);
                holder.price_title.setVisibility(View.GONE);
                holder.tv_price.setVisibility(View.GONE);
                holder.rest_name_title.setText(context.getString(R.string.party_date));
                holder.tv_restaurant_name.setText(data.get(position).getPartydate());
                holder.tv_order_num.setText(String.valueOf(data.get(position).getCodeorder()));

            }
            else if (data.get(position).getStatus().equals("-2")) {
                holder.tv_order_num.setText(String.valueOf(data.get(position).getId()));
                holder.btn_order_status.setText(R.string.refused);
                holder.btn_order_cancel.setVisibility(View.GONE);



            }

        }// else


        if (data.get(position) != null) {

            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
                holder.tv_banquet_name.setText(data.get(position).getPartyname());
                holder.tv_banquet_name.setTypeface(font);
            }
            else if (sharedPreferences.getString("language", "ar").equals("en")) {
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
                holder.tv_banquet_name.setText(data.get(position).getPartyname());
                holder.tv_banquet_name.setTypeface(font);
            }
        }

        holder.tv_price.setText(data.get(position).getTotalprice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("QP", "id send from adapter : " + data.get(position).getId());

                if(data.get(position).getStatus().equals("-4") || data.get(position).getStatus().equals("2") || data.get(position).getStatus().equals("3"))
                {
                    Intent intent = new Intent(context, OrdersAndOffersDetails.class);
                    intent.putExtra("id",data.get(position).getCodeorder()); // send code order to get details
                    Log.i("QP", "code order send from adapter : " + data.get(position).getCodeorder());
                    intent.putExtra("status",data.get(position).getStatus()); // send code order to get details
                    intent.putExtra("flag","order");
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, OrdersAndOffersDetails.class);
                    intent.putExtra("id",String.valueOf(data.get(position).getId())); // send id to get order details
                    Log.i("QP", "id send from adapter : " + data.get(position).getId());
                    intent.putExtra("status",data.get(position).getStatus()); // send code order to get details
                    context.startActivity(intent);

                }

            }
        });

    } // on bind function



    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void cancelOrder(final String code_order, final int position) {
        progress = new ProgressDialog(context);
        progress.show();
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);
        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow().setLayout((int) (getScreenWidth(((Activity) context)) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                RetrofitConnection connection = new RetrofitConnection((Activity) context);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final CancelOrder cancelOrder = retrofit.create(CancelOrder.class);

                final Call<LoginResponse> getInterestConnection = cancelOrder.canceNewlOrder(code_order);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(context.getString(R.string.order_cancelled),context);
                               /* Intent intent = new Intent(context, MenuActivity.class);
                                context.startActivity(intent);
                                ((Activity) context).finish();
*/
                                data.remove(position);
                                notifyItemRemoved(position);

                            } //  success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(context.getString(R.string.retry),context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // cancel order

    @Override
    public int getItemCount() {
        Log.i("QP", "order size : " + data.size());
        return data.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_banquet_name)
        TextView tv_banquet_name;

        @BindView(R.id.price_title)
        TextView price_title;

        @BindView(R.id.rest_name_title)
        TextView rest_name_title;

        @BindView(R.id.order_num_title)
        TextView order_num_title;

        @BindView(R.id.btn_order_status)
        Button btn_order_status;

        @BindView(R.id.btn_order_cancel)
        Button btn_order_cancel;

        @BindView(R.id.tv_order_num)
        TextView tv_order_num;

        @BindView(R.id.tv_restaurant_name)
        TextView tv_restaurant_name;

        @BindView(R.id.tv_price)
        TextView tv_price;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // view holder class
}
