package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Notifications.NotificationApi;
import com.invetechs.mamamiauserversion.Retrofit.Notifications.SendNotification;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.NotificationResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.NotificationsActivity;
import com.invetechs.mamamiauserversion.View.Activity.OrdersAndOffersDetails;
import com.invetechs.mamamiauserversion.View.Activity.OrdersDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class NotificationstAdapter extends RecyclerView.Adapter<NotificationstAdapter.MyViewHolder> {

  // vars
  SharedPreferences sharedPreferences;
  Context context;
  private List<NotificationResponse.DataBean> arrayList;
  String status_value;
  String notification_id = "";


  public NotificationstAdapter(Context context, List<NotificationResponse.DataBean> arrayList) {
    this.context = context;
    this.arrayList = arrayList;
  } // constructor

  @NonNull
  @Override
  public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false);
    return new MyViewHolder(view);
  } // on create function

  @Override
  public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

    sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

    if (sharedPreferences.getString("language", "ar").equals("ar")) {
      ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

      holder.tv_desc.setText(arrayList.get(position).getAr_description());
      Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
      holder.tv_desc.setTypeface(font);
      holder.img_view.setRotation(180);

    }

    else if (sharedPreferences.getString("language", "ar").equals("en")) {
      holder.tv_desc.setText(arrayList.get(position).getEn_description());
      Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
      holder.tv_desc.setTypeface(font);
    }

    status_value = arrayList.get(position).getStatus();

    notification_id = arrayList.get(position).getId();

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Log.i("QP", "notification status : " + status_value);

        if (status_value.equals("1") || status_value.equals("-1")) {
          Intent intent = new Intent(context, OrdersDetails.class);
          intent.putExtra("orderId", String.valueOf(arrayList.get(position).getOrder_id()));
          context.startActivity(intent);
          ((Activity)context).finish();
        }

        else if (status_value.equals("3")) {
          Intent intent = new Intent(context, OrdersAndOffersDetails.class);
          intent.putExtra("id", String.valueOf(arrayList.get(position).getOrder_id()));
          intent.putExtra("status", "offer");
          context.startActivity(intent);
          ((Activity)context).finish();

        }
      }
    });

  } // on bind function



  @Override
  public int getItemCount() {
    return arrayList.size();
  } // get items count

  public class MyViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.tv_desc)
    TextView tv_desc;

    @BindView(R.id.img_view)
    ImageView img_view;

    public MyViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

  } // view holder class
}
