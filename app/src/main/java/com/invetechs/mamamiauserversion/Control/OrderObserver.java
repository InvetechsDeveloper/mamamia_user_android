package com.invetechs.mamamiauserversion.Control;

import java.util.Observable;

public class OrderObserver extends Observable {

    String order_data = "";
    String party_name = "";
    String party_type = "";
    String person_number = "";
    String party_date = "";
    String party_time = "";
    String customer_comments = "";

    private static OrderObserver orderObserver;

    public static OrderObserver getInstance() {
        if (orderObserver == null)
            orderObserver = new OrderObserver();
        return orderObserver;
    }


    public String getParty_name() {
        setChanged();
        return party_name;
    }

    public void setParty_name(String party_name) {
        setChanged();
        this.party_name = party_name;
    }

    public String getParty_type() {
        setChanged();
        return party_type;
    }

    public void setParty_type(String party_type) {
        setChanged();
        this.party_type = party_type;
    }

    public String getPerson_number() {
        setChanged();
        return person_number;
    }

    public void setPerson_number(String person_number) {
        setChanged();
        this.person_number = person_number;
    }

    public String getParty_date() {
        setChanged();
        return party_date;
    }

    public void setParty_date(String party_date) {
        setChanged();
        this.party_date = party_date;
    }

    public String getParty_time() {
        setChanged();
        return party_time;
    }

    public void setParty_time(String party_time) {
        setChanged();
        this.party_time = party_time;
    }

    public String getCustomer_comments() {
        setChanged();
        return customer_comments;
    }

    public void setCustomer_comments(String customer_comments) {
        setChanged();
        this.customer_comments = customer_comments;
    }

    public String getOrder_data() {
        setChanged();
        return order_data;
    }

    public void setOrder_data(String order_data) {
        setChanged();
        this.order_data = order_data;
    }
}
