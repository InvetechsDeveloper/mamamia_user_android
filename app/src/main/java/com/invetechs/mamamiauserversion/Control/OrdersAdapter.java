package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi.RejectOrder;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrdersResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;
import com.invetechs.mamamiauserversion.View.Activity.OrdersDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> {

    // vars
    Context context;
    ArrayList<OrdersResponse.DataBean> ordersArrayList;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progress;
    Handler handler;
    String order_id = "";
    String type = "user";
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    String order_name, name;

    ShowDialog toastDialog = new ShowDialog();


    public OrdersAdapter(Context context, ArrayList<OrdersResponse.DataBean> ordersArrayList) {
        this.context = context;
        this.ordersArrayList = ordersArrayList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_order_name.setTypeface(font);
            holder.tv_order_num.setTypeface(font);
            holder.tv_location.setTypeface(font);
            holder.tv_price.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.price_title.setTypeface(font);

            if (ordersArrayList.get(position).getMeal() != null)

                order_name = ordersArrayList.get(position).getMeal().getAr_title();
            name = "";

            for (int i = 0; i < 18; i++) {

                if (order_name != null)

                    if (order_name.length() > 18) {
                        if (i == 17)
                            name = name + "...";
                        else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 18) {
                        name = order_name;

                    }

            }
            holder.tv_order_name.setText(name);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_order_name.setTypeface(font);
            holder.tv_order_num.setTypeface(font);
            holder.tv_location.setTypeface(font);
            holder.tv_price.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.price_title.setTypeface(font);

            if (ordersArrayList.get(position).getMeal() != null)

                order_name = ordersArrayList.get(position).getMeal().getEn_title();
            name = "";

            for (int i = 0; i < 20; i++) {

                if (order_name != null)
                    if (order_name.length() > 20) {
                        if (i == 19)
                            name = name + "...";
                        else
                            name = name + order_name.charAt(i);
                    } else if (order_name.length() <= 20) {
                        name = order_name;

                    }

            }

            holder.tv_order_name.setText(name);

        }

        order_id = String.valueOf(ordersArrayList.get(position).getId());


        if (ordersArrayList.get(position).getStatus().equals("1")) {

            holder.btn_order_status.setText(R.string.accepted);

        } else if (ordersArrayList.get(position).getStatus().equals("-1")) {
            holder.btn_order_status.setText(R.string.rejected);
        } else if (ordersArrayList.get(position).getStatus().equals("-2")) {
            holder.btn_order_status.setText(R.string.canceled);
        } else if (ordersArrayList.get(position).getStatus().equals("0")) {
            holder.btn_order_status.setText(R.string.cancel);
            holder.btn_order_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rejectOrder(String.valueOf(ordersArrayList.get(position).getId()), type);

                    Log.i("QR", String.valueOf(ordersArrayList.get(position).getId()));
                }
            });
        }

        holder.tv_order_num.setText(order_id);

        holder.tv_location.setText(ordersArrayList.get(position).getUserplace());

        holder.tv_price.setText(ordersArrayList.get(position).getTotalprice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ordersIntent = new Intent(context, OrdersDetails.class);
                ordersIntent.putExtra("orderId", String.valueOf(ordersArrayList.get(position).getId()));
                context.startActivity(ordersIntent);
            }
        });

    } // on bind function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    private void rejectOrder(final String order_id, final String type) {
        progress = new ProgressDialog(context);
        progress.show();
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);
        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow().setLayout((int) (getScreenWidth(((Activity) context)) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                RetrofitConnection connection = new RetrofitConnection((Activity) context);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final RejectOrder orderApi = retrofit.create(RejectOrder.class);

                final Call<LoginResponse> getInterestConnection = orderApi.rejectOrderApi(order_id, type);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(context.getString(R.string.order_cancelled), context);
                                Intent intent = new Intent(context, MenuActivity.class);
                                intent.putExtra("fragmentFlag","home");
                                context.startActivity(intent);
                                ((Activity) context).finish();

                            } //  success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        toastDialog.initDialog(context.getString(R.string.retry), context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
            }

        }.start();
    } // reject order if its status cancel

    @Override
    public int getItemCount() {
        return ordersArrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_name)
        TextView tv_order_name;

        @BindView(R.id.btn_order_status)
        Button btn_order_status;

        @BindView(R.id.order_name_title)
        TextView order_name_title;

        @BindView(R.id.tv_order_num)
        TextView tv_order_num;

        @BindView(R.id.price_title)
        TextView price_title;

        @BindView(R.id.location_title)
        TextView location_title;

        @BindView(R.id.tv_location)
        TextView tv_location;

        @BindView(R.id.tv_price)
        TextView tv_price;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // view holder class
}
