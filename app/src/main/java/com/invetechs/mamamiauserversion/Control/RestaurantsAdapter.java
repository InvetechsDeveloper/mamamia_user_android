package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AllRestaurantResponse;
import com.invetechs.mamamiauserversion.View.Activity.RestaurantDetailsAvtivity;
import com.invetechs.mamamiauserversion.View.Activity.BanquetDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.MyViewHolder>
{

    //vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    Activity activity;

    double latitude , longitude ;

    List<AllRestaurantResponse.DataBean> restaurantList ;
    List<AllRestaurantResponse.DataBean> restaurantListFiltered ;

    String flag ="";
    String type = "";
    double rate=0 ,distanceWithKilo;
    String title ="";
    ShowDialog toastDialog = new ShowDialog();
    SharedPreferences prefs ;
    int cartKitchenId =0;
    ToolBarConfig toolBarConfig;
    LinearLayout layout;
    double totalDelivery;


    public RestaurantsAdapter(Context context,List<AllRestaurantResponse.DataBean> restaurantList,String flag,
                              double latitude,double longitude,Activity activity,String type)
    {
        this.context = context;
        this.restaurantList = restaurantList;
        this.flag = flag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.restaurantListFiltered = restaurantList;
        this.activity =activity;
        this.type = type ;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.restaurant_image)
        AppCompatImageView restaurant_image ;

        @BindView(R.id.restaurnt_name)
        AppCompatTextView restaurnt_name;

        @BindView(R.id.restaurant_rate)
        AppCompatTextView restaurant_rate;

        @BindView(R.id.restaurant_count_people)
        AppCompatTextView restaurant_count_people;

        @BindView(R.id.restaurant_distance)
        AppCompatTextView restaurant_distance;

        @BindView(R.id.restaurant_minCharge)
        AppCompatTextView restaurant_minCharge;

        @BindView(R.id.restaurant_delivery)
        AppCompatTextView restaurant_delivery;

        @BindView(R.id.restaurant_ratingBar)
        AppCompatRatingBar restaurant_ratingBar;

        Dialog dialog;
        Button yes , no ;
        LinearLayout layout , layout2;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.confirm_remove_cart);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.restaurant_item,parent,false);
        initLanguage();
        return new MyViewHolder(view);
    }

    private void initLanguage()
    {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    } //initialization language and set font

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar"))
        {
          holder.restaurnt_name.setText(restaurantListFiltered.get(position).getAr_title());
          title = restaurantListFiltered.get(position).getAr_title();
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.restaurant_rate.setTypeface(font);
            holder.restaurant_count_people.setTypeface(font);
            holder.restaurnt_name.setTypeface(font);
            holder.restaurant_distance.setTypeface(font);
            holder.restaurant_minCharge.setTypeface(font);
            holder.restaurant_delivery.setTypeface(font);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en"))
        {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.restaurant_rate.setTypeface(font);
            holder.restaurant_count_people.setTypeface(font);
            holder.restaurnt_name.setTypeface(font);
            holder.restaurant_distance.setTypeface(font);
            holder.restaurant_minCharge.setTypeface(font);
            holder.restaurant_delivery.setTypeface(font);

            holder.restaurnt_name.setText(restaurantListFiltered.get(position).getEn_title());
            title = restaurantListFiltered.get(position).getEn_title();
        }
        if(restaurantListFiltered.get(position).getCount_rate() > 0) {
            rate = (restaurantListFiltered.get(position).getTotal_rate() / restaurantListFiltered.get(position).getCount_rate());
            int precision1 = 10; //keep 1 digits
            rate = Math.floor(rate * precision1 + .5) / precision1;
        }else rate = 0;

        holder.restaurant_ratingBar.setRating((float) rate);

        holder.restaurant_rate.setText(rate+"");

        holder.restaurant_count_people.setText(" ("+restaurantListFiltered.get(position).getCount_rate()+") ");

        final double distanceWithMeter =distance(restaurantListFiltered.get(position).getLat(),restaurantListFiltered.get(position).getLng(),latitude,longitude);
         distanceWithKilo =  distanceWithMeter / 1000 ;
        int precision = 10; //keep 1 digits
        distanceWithKilo= Math.floor(distanceWithKilo * precision +.5)/precision;

        Log.i("QP" , "distanceWithKilo : " + distanceWithKilo);

        holder.restaurant_distance.setText(distanceWithKilo+" "+context.getResources().getString(R.string.km));


        holder.restaurant_minCharge.setText(context.getResources().getString(R.string.miniCharge)
        +" : "+restaurantListFiltered.get(position).getMinicharge()+" "+context.getResources().getString(R.string.sr));

        final double deliveryPerKilo = Double.parseDouble(restaurantListFiltered.get(position).getDeliverypriceperkilo());
        if(distanceWithKilo < 1) distanceWithKilo = 1 ;


        totalDelivery = Math.round(distanceWithKilo) * deliveryPerKilo;
        restaurantListFiltered.get(position).setTotalDelivery(totalDelivery);

        holder.restaurant_delivery.setText(context.getResources().getString(R.string.delivery)
                +" : "+totalDelivery+" "+context.getResources().getString(R.string.sr));

        String imageUrl = "https://mammamiaa.com/cpanel/upload/kitchen/"+restaurantListFiltered.get(position).getImage();

        RequestOptions requestOptionss = new RequestOptions();
        requestOptionss.placeholder(R.drawable.pizza_back);
        requestOptionss.error(R.drawable.pizza_back);

        if (!imageUrl.equals(null))
        Glide.with(context)
                .setDefaultRequestOptions(requestOptionss)
                .load(imageUrl)
                .into(holder.restaurant_image);

        if(flag.equals("restaurant")) {


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    prefs = context.getSharedPreferences("cart",MODE_PRIVATE);
                    if(prefs != null)
                        cartKitchenId = prefs.getInt("kitchenId",0);

                   Log.i("QR","cartKitchen : restaurant"+cartKitchenId);

                    Log.i("QW","restAdapter : "+restaurantListFiltered.get(position).getTotalDelivery());
                   if(cartKitchenId == restaurantListFiltered.get(position).getId() || cartKitchenId == 0)
                   {
                       Intent intent = new Intent(view.getContext(), RestaurantDetailsAvtivity.class);
                       intent.putExtra("id",restaurantListFiltered.get(position).getId());
                       intent.putExtra("maxbill",restaurantListFiltered.get(position).getDelivermaxbill());
                       intent.putExtra("totalDelivery",restaurantListFiltered.get(position).getTotalDelivery());
                       intent.putExtra("distanceWithKilo",distanceWithKilo);
                       intent.putExtra("lat",latitude);
                       intent.putExtra("lng",longitude);
                       intent.putExtra("type",type);
                       view.getContext().startActivity(intent);
                   }

                   else
                   {

                       toolBarConfig = new ToolBarConfig(activity,"none");
                       holder.layout.setBackgroundResource(R.color.white);
                       holder.layout2.setBackgroundResource(R.color.white);
                       holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                       holder.dialog.getWindow()
                               .setLayout((int) (getScreenWidth((Activity) view.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                       holder.dialog.show();
                       Log.i("QP","clearCart : ");
                   }


                }
            });

            holder.yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.dialog.dismiss();
                    toolBarConfig.clearCart();
                }
            });

            holder.no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.dialog.dismiss();
                }
            });
        } // open restarant details

        else if(flag.equals("banquet")) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    prefs = context.getSharedPreferences("cart",MODE_PRIVATE);
                    if(prefs != null)
                        cartKitchenId = prefs.getInt("kitchenId",0);

                    if(cartKitchenId == restaurantListFiltered.get(position).getId() || cartKitchenId == 0) {
                        Intent intent = new Intent(view.getContext(), BanquetDetailsActivity.class);
                        intent.putExtra("id", restaurantListFiltered.get(position).getId());
                        intent.putExtra("maxbill", restaurantListFiltered.get(position).getDelivermaxbill());
                        intent.putExtra("totalDelivery", totalDelivery);
                        intent.putExtra("distanceWithKilo", distanceWithKilo);
                        intent.putExtra("lat", latitude);
                        intent.putExtra("lng", longitude);
                        view.getContext().startActivity(intent);
                    }
                    else
                    {


                        toolBarConfig = new ToolBarConfig(activity,"none");
                        holder.layout.setBackgroundResource(R.color.white);
                        holder.layout2.setBackgroundResource(R.color.white);
                        holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        holder.dialog.getWindow()
                                .setLayout((int) (getScreenWidth((Activity) view.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                        holder.dialog.show();
                        Log.i("QP","clearCart : ");
                    }
                }
            });

            holder.yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.dialog.dismiss();
                    toolBarConfig.clearCart();
                }
            });

            holder.no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.dialog.dismiss();
                }
            });
        } // open restarant details


    } // function of onBindView holder

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth
    @Override
    public int getItemCount() {
        return restaurantListFiltered.size();
    }

    public float distance (double lat_a, double lng_a, double lat_b, double lng_b )
    {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b-lat_a);
        double lngDiff = Math.toRadians(lng_b-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    } // distance between restaurant and user



    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.i("QP","text : "+charSequence);
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    restaurantListFiltered = restaurantList;
                } else {
                    List<AllRestaurantResponse.DataBean> filteredList = new ArrayList<>();
                    for (AllRestaurantResponse.DataBean row : restaurantList) {

                        if (row.getEn_title().toLowerCase().contains(charString.toLowerCase()) || row.getAr_title().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    restaurantListFiltered = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = restaurantListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                restaurantListFiltered = (ArrayList<AllRestaurantResponse.DataBean>) filterResults.values;

                if(restaurantListFiltered.size() <= 0)
                    toastDialog.initDialog(context.getString(R.string.noResults),context);
                notifyDataSetChanged();
            }
        };
    }
}
