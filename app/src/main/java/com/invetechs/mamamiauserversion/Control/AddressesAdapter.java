package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AdressesApi.AddNewAddressApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.AddAddressActivity;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.MyViewHolder> {

    // vars
    Context context;
    List<AddressResponse.DataBean> addressList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Handler handler;
    Dialog progress;
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    ShowDialog toastDialog = new ShowDialog();

    public AddressesAdapter(Context context, List<AddressResponse.DataBean> addressList) {
        this.context = context;
        this.addressList = addressList;

    } // constructor of AddressesAdapter

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_title)
        TextView title;

        @BindView(R.id.address_address)
        TextView address;

        @BindView(R.id.address_edit)
        ImageView edit;

        @BindView(R.id.address_delete)
        ImageView delete;

        Dialog dialog;
        Button yes, no;
        LinearLayout layout, layout2;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.dialog_confirm_delete_address);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);

        }
    } // class of MyViewHolder

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_address, parent, false);
        initLanguage();
        return new MyViewHolder(view);
    }

    private void initLanguage() {
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    } //initialization language and set font

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.title.setTypeface(font);
            holder.address.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.title.setTypeface(font);
            holder.address.setTypeface(font);

        }

        holder.title.setText(addressList.get(position).getNote());
        holder.address.setText(addressList.get(position).getAddress());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.layout.setBackgroundResource(R.color.white);
                holder.layout2.setBackgroundResource(R.color.white);
                holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                holder.dialog.getWindow()
                        .setLayout((int) (getScreenWidth((Activity) view.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.dialog.show();
            }
        });

        holder.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
                deleteAddress(addressList.get(position).getId(), position);
            }
        });

        holder.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddAddressActivity.class);
                intent.putExtra("id", addressList.get(position).getId());
                intent.putExtra("lat", addressList.get(position).getLat());
                intent.putExtra("lng", addressList.get(position).getLng());
                intent.putExtra("note", addressList.get(position).getNote());
                intent.putExtra("address", addressList.get(position).getAddress());
                intent.putExtra("flag", "update");
                Log.i("QP", "aadapter :  " + addressList.get(position).getLat() + " 6: " + addressList.get(position).getLng());
                view.getContext().startActivity(intent);
            }
        });
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void deleteAddress(final int addressId, final int position) {
        progress = new Dialog(context);
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);
        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AddNewAddressApi addressApi = retrofit.create(AddNewAddressApi.class);

                final Call<AddressResponse> getInterestConnection = addressApi.deleteUserAddress(addressId);

                getInterestConnection.enqueue(new Callback<AddressResponse>() {
                    @Override
                    public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                        try {
                            if (response.body() != null) {
                                String code = response.body().getCode();
                                Log.i("QP", "code" + code);

                                if (code.equals("200")) {

                                    toastDialog.initDialog(context.getString(R.string.addressDeleted), context);
                                    addressList.remove(position);
                                    notifyItemRemoved(position);
                                } //

                            } // if response success
                            else {
                                toastDialog.initDialog(context.getString(R.string.retry), context);
                            } // response faild
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<AddressResponse> call, Throwable t) {

                        toastDialog.initDialog(context.getString(R.string.retry), context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of deleteAddress

    @Override
    public int getItemCount() {
        return addressList.size();
    }


} // class of AddressesAdapter
