package com.invetechs.mamamiauserversion.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.View.Fragment.CartFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    // vars
    Context context;
    List<CartModel> arrayList;
    SharedPreferences sharedPreferences;
    ToolBarConfig toolBarConfig;
    TextView txt_totalBill;
    TextView txt_grandTotal;
    CartModel cartModel;
    CheckBox checkPoint;
    double discountPoint = 0, vat;
    int deliveryMaxBill;
    float totalDelivery;
    TextView txt_totalDelivery, txt_userPoints, txt_vat;
    double totalAfterVat = 0;
    CartFragment cartFragment = new CartFragment();
    double totalBill = 0;
    DecimalFormat df = new DecimalFormat("0.00");

    public CartAdapter(Context context, List<CartModel> arrayList, TextView txt_totalBill, TextView txt_vat, TextView txt_grandTotal
            , CheckBox checkPoint, float totalDelivery, int deliveryMaxBill, TextView txt_totalDelivery
            , TextView txt_userPoints, double vat) {
        this.vat = vat;
        this.context = context;
        this.arrayList = arrayList;
        this.txt_totalBill = txt_totalBill;
        this.txt_grandTotal = txt_grandTotal;
        this.checkPoint = checkPoint;
        this.totalDelivery = totalDelivery;
        this.deliveryMaxBill = deliveryMaxBill;
        this.txt_totalDelivery = txt_totalDelivery;
        this.txt_userPoints = txt_userPoints;
        this.txt_vat = txt_vat;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        totalBill = totalBill + arrayList.get(position).getTotal();

        Log.i("QP", "totalBill : adapter : " + totalBill);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tvEnglishMealName.setTypeface(font);
            holder.tvPrice.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tvEnglishMealName.setTypeface(font);
            holder.tvPrice.setTypeface(font);
        }

        final CartModel getPosition = arrayList.get(position);

        if (sharedPreferences.getString("language", "ar").equals("ar"))
            holder.tvEnglishMealName.setText(getPosition.getArTitle());

        else if (sharedPreferences.getString("language", "ar").equals("en"))
            holder.tvEnglishMealName.setText(getPosition.getEnTitle());

        holder.numberButton.setRange(1, 99);
        int amount = Integer.parseInt(holder.numberButton.getNumber());

        holder.numberButton.setNumber("" + getPosition.getAmount());

        Log.i("cart", "amount from cart adapter : " + amount);

        holder.tvPrice.setText("" + getPosition.getPrice() * getPosition.getAmount() + " " + context.getString(R.string.sr));

        holder.numberButton.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {

                holder.tvPrice.setText("" + getPosition.getPrice() * newValue + " " + context.getString(R.string.sr));

                ///////////////////////////////////////////////////////////////////
                Log.i("QP", "total : " + totalBill + " : totalItem : " +
                        getPosition.getTotal() + " : newTotalItem : " + getPosition.getPrice() * newValue);
                totalBill = (totalBill + ((getPosition.getPrice() * newValue) - getPosition.getTotal()));

                txt_totalBill.setText(df.format(totalBill) + " " + context.getString(R.string.sr));
                totalAfterVat = totalBill + totalBill * vat;

                txt_vat.setText(df.format(totalBill * vat) + " " + context.getString(R.string.sr));

                discountPoint = cartFragment.pointDiscount;
                Log.i("QP", "casesTest : " + "\n" +
                        "total bill : " + totalBill + "\n" +
                        "deliveryMaxBill : " + deliveryMaxBill + "\n" +
                        "check : " + checkPoint.isChecked() + "\n"
                        + "point : " + discountPoint + "\n"
                        + "totalDelivery : " + totalDelivery + "\n");
                if (totalBill >= deliveryMaxBill) {
                    txt_totalDelivery.setText(context.getString(R.string.deliveryFree));
                    if (checkPoint.isChecked() == false) {
                        txt_grandTotal.setText(df.format(totalAfterVat) + " " + context.getString(R.string.sr));
                        discountPoint = 0;
                        cartFragment.pointDiscount = 0;
                    } else if (checkPoint.isChecked() == true) {
                        discountPoint = cartFragment.pointDiscount;
                        if (totalBill > discountPoint)
                            txt_grandTotal.setText(df.format(totalAfterVat - discountPoint) + " " + context.getString(R.string.sr));
                        else
                            txt_grandTotal.setText("0 " + context.getString(R.string.sr));
                    }
                    Log.i("QQ", "inve : " + totalBill + " : " + discountPoint);
                } // delivery for this order free
                else {
                    Log.i("QP", "inve : " + totalBill + " : " + totalDelivery);
                    txt_totalDelivery.setText(df.format(totalDelivery) + " " + context.getString(R.string.sr));
                    if (checkPoint.isChecked() == false) {
                        txt_grandTotal.setText(df.format(totalAfterVat + totalDelivery) + " " + context.getString(R.string.sr));
                        discountPoint = 0;
                    } else if (checkPoint.isChecked() == true) {
                        discountPoint = cartFragment.pointDiscount;
                        if (totalBill + totalDelivery > discountPoint)
                            txt_grandTotal.setText(df.format((totalAfterVat + totalDelivery) - discountPoint) + " " + context.getString(R.string.sr));

                        else
                            txt_grandTotal.setText("0" + " " + context.getString(R.string.sr));
                    }
                    Log.i("QQ", "totaaaaaaaal" + " : " + ((totalAfterVat + totalDelivery + totalBill) - discountPoint));

                    Log.i("QQ", "inve : not f" + totalBill + " : " + discountPoint);
                } // delivery fro this order not free


                toolBarConfig = new ToolBarConfig((Activity) context, "none");
                //cart dialog
                String enTitle = arrayList.get(position).getEnTitle();
                String arTitle = arrayList.get(position).getArTitle();
                double price = Double.parseDouble(String.valueOf(arrayList.get(position).getPrice()));
                String notes = arrayList.get(position).getNotes();
                int amountCart = Integer.parseInt(holder.numberButton.getNumber()) - arrayList.get(position).getAmount();
                int itemID = arrayList.get(position).getId();

                if (notes.equals(null) || notes.isEmpty()) notes = "none";

                cartModel = new CartModel(itemID, enTitle, arTitle, notes, amountCart, price);

                toolBarConfig.addCart(cartModel);

                arrayList.get(position).setAmount(newValue);
            }
        });

        holder.delete_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolBarConfig = new ToolBarConfig((Activity) context, "none");
                holder.layout.setBackgroundResource(R.color.white);
                holder.layout2.setBackgroundResource(R.color.white);
                holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                holder.dialog.getWindow()
                        .setLayout((int) (getScreenWidth((Activity) view.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.dialog.show();

            }
        }); // delete item


        holder.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
                Log.i("QP", "size : " + arrayList.size() + " : index: " + position);

                if (arrayList.size() == 1) {
                    try {
                        txt_totalBill.setText(context.getString(R.string.zero) + " " + context.getString(R.string.sr));
                        txt_vat.setText(context.getString(R.string.zero) + " " + context.getString(R.string.sr));
                        txt_grandTotal.setText(context.getString(R.string.zero) + " " + context.getString(R.string.sr));
                        txt_totalDelivery.setText("");
                        arrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, arrayList.size());
                        notifyDataSetChanged();
                        toolBarConfig.clearCart();

                    } catch (Exception e) {
                        Log.i("QP", "clear : " + e.toString());
                    }

                } else {
                    try {

                        /////////////////////////////////////////////////////////////
                        totalBill = totalBill - (getPosition.getTotal());
                        txt_totalBill.setText(df.format(totalBill) + " " + context.getString(R.string.sr));
                        totalAfterVat = totalBill + totalBill * vat;

                        txt_vat.setText(df.format(totalBill * vat) + " " + context.getString(R.string.sr));

                        Log.i("QP", "casesTest : " + "\n" +
                                "total bill : " + totalBill + "\n" +
                                "deliveryMaxBill : " + deliveryMaxBill + "\n" +
                                "check : " + checkPoint.isChecked() + "\n"
                                + "point : " + discountPoint + "\n"
                                + "totalDelivery : " + totalDelivery + "\n");
                        if (totalBill >= deliveryMaxBill) {
                            txt_totalDelivery.setText(context.getString(R.string.deliveryFree));
                            if (checkPoint.isChecked() == false) {
                                txt_grandTotal.setText(df.format(totalAfterVat) + " " + context.getString(R.string.sr));
                                discountPoint = 0;
                                cartFragment.pointDiscount = 0;
                            } else if (checkPoint.isChecked() == true) {
                                discountPoint = cartFragment.pointDiscount;
                                txt_grandTotal.setText(df.format(totalAfterVat - discountPoint) + " " + context.getString(R.string.sr));
                            }
                        } // delivery for this order free
                        else {
                            txt_totalDelivery.setText(df.format(totalDelivery) + " " + context.getString(R.string.sr));
                            if (checkPoint.isChecked() == false) {
                                txt_grandTotal.setText(df.format(totalAfterVat + totalDelivery) + " " + context.getString(R.string.sr));
                                discountPoint = 0;
                            } else if (checkPoint.isChecked() == true) {
                                discountPoint = cartFragment.pointDiscount;
                                txt_grandTotal.setText(df.format((totalAfterVat + totalDelivery) - discountPoint) + " " + context.getString(R.string.sr));
                            }

                        } // delivery fro this order not free

                        totalBill = 0;

                        arrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, arrayList.size());
                        notifyDataSetChanged();

                        toolBarConfig.removeItemWithId(getPosition.getId());
                    } catch (Exception e) {
                        Log.i("QP", "remove : " + e.toString());
                    }
                }

            }
        });

        holder.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
            }
        });


    } // on bind function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    @Override
    public int getItemCount() {

        return arrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_english_meal_name)
        TextView tvEnglishMealName;

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.delete_item)
        ImageView delete_item;

        Dialog dialog;
        Button yes, no;
        LinearLayout layout, layout2;

        @BindView(R.id.number_button)
        ElegantNumberButton numberButton;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.delete_cart_item);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);
        }

    } // view holder class

}
