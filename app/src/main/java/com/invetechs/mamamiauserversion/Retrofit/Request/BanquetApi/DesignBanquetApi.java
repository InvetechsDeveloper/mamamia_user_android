package com.invetechs.mamamiauserversion.Retrofit.Request.BanquetApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.CheckResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DesignBanquetApi
{
    @FormUrlEncoded
    @POST("Store_banquetUser")
    retrofit2.Call<CheckResponse> designBanquet(
            @Field("user_id") int user_id,
            @Field("lat") double lat,
            @Field("lng") double lng,
            @Field("component") String component,
            @Field("partyname") String partyname,
            @Field("partytype") String partytype,
            @Field("partylocation") String partylocation,
            @Field("noofperson") String noofperson,
            @Field("partytime") String partytime,
            @Field("partydate") String partydate,
            @Field("usernote") String usernote
    );  // insert banquet
} /// Interface of DesignBanquetApi
