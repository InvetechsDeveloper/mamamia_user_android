package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class SearchMenuResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * count : 16
     * data : [{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا هت","en_title_kitchen":"MCdonalds"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا هت","en_title_kitchen":"MCdonalds"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا هت","en_title_kitchen":"MCdonalds"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا كينج","en_title_kitchen":"Pizza King"},{"ar_title_meal":"وجبه شيشي طاووق ","en_title_meal":"SHESH TAWOQ","ar_description":"شيشي طاووق ساخن مغطى بالجبنه الموزويلا","en_description":"COVERED BY MOZIRLLA CHEESS","image":null,"price":"123","ar_title_kitchen":"بيتزا هت","en_title_kitchen":"MCdonalds"}]
     */

    private String code;
    private String Status;
    private String message;
    private int count;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ar_title_meal : وجبه شيشي طاووق
         * en_title_meal : SHESH TAWOQ
         * ar_description : شيشي طاووق ساخن مغطى بالجبنه الموزويلا
         * en_description : COVERED BY MOZIRLLA CHEESS
         * image : null
         * price : 123
         * ar_title_kitchen : بيتزا كينج
         * en_title_kitchen : Pizza King
         */

        private String ar_title;
        private String en_title;
        private String ar_description;
        private String en_description;
        private String image;
        private String price;
        private String ar_title_kitchen;
        private String en_title_kitchen;
        private int id;
        private int kitchen_id;
        private int deliverypriceperkilo;
        private int delivermaxbill;
        private double distance;
        private  double totalDelivery ;
        double distanceToUser ;
        double lat , lng ;


        public String getAr_title_meal() {
            return ar_title;
        }

        public void setAr_title_meal(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title_meal() {
            return en_title;
        }

        public void setEn_title_meal(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getAr_title_kitchen() {
            return ar_title_kitchen;
        }

        public void setAr_title_kitchen(String ar_title_kitchen) {
            this.ar_title_kitchen = ar_title_kitchen;
        }

        public String getEn_title_kitchen() {
            return en_title_kitchen;
        }

        public void setEn_title_kitchen(String en_title_kitchen) {
            this.en_title_kitchen = en_title_kitchen;
        }

        public int getMeal_id() {
            return id;
        }

        public void setMeal_id(int id) {
            this.id = id;
        }

        public int getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(int kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public int getDeliverypriceperkilo() {
            return deliverypriceperkilo;
        }

        public void setDeliverypriceperkilo(int deliverypriceperkilo) {
            this.deliverypriceperkilo = deliverypriceperkilo;
        }

        public int getDelivermaxbill() {
            return delivermaxbill;
        }

        public void setDelivermaxbill(int delivermaxbill) {
            this.delivermaxbill = delivermaxbill;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public double getTotalDelivery() {
            return totalDelivery;
        }

        public void setTotalDelivery(double totalDelivery) {
            this.totalDelivery = totalDelivery;
        }

        public double getDistanceToUser() {
            return distanceToUser;
        }

        public void setDistanceToUser(double distanceToUser) {
            this.distanceToUser = distanceToUser;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }
}
