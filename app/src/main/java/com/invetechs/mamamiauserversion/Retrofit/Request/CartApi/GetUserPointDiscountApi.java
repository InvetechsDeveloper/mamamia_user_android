package com.invetechs.mamamiauserversion.Retrofit.Request.CartApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.UserPointResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetUserPointDiscountApi
{
    @FormUrlEncoded
    @POST("PointsUser")
    Call<UserPointResponse> getUserDiscount(
            @Field("user_id") int user_id,
            @Field("kitchen_id") int kitchen_id
    );
}

