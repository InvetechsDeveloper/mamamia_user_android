package com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RejectOrder {

    @FormUrlEncoded
    @POST("ignororder")
    retrofit2.Call<LoginResponse> rejectOrderApi(@Field("order_id") String order_id ,
                                                 @Field("type") String type

    );  // reject order
}
