package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgetPasswordApi {

    @FormUrlEncoded
    @POST("foroget_passworod")
    Call<LoginResponse> forgetPass(@Field("mobilenumber") String mobilenumber ,// forget password
                                   @Field("category") String category );  // forget password

} // interface of foregetPassword
