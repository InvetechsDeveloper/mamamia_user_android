package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class ReviewResponse
{


    /**
     * code : 1313
     * Status : success
     * message : code sent successfully
     * count : 9
     * data : [{"user_id":87,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"omar ahlawii"},{"user_id":29,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"omaaar"},{"user_id":63,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"Mahmoud Badr"},{"user_id":22,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"eslam Ali"},{"user_id":4,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"slammed"},{"user_id":12,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"karim"},{"user_id":38,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"omaaaar"},{"user_id":17,"rate":"3.0","review":"مطعم مميز جدا","created_at":"2018-10-09 04:14:00","name":"Yasser Ahmed"},{"user_id":127,"rate":"3.5","review":"very good","created_at":"2018-10-17 07:31:36","name":"Omar"}]
     */

    private String code;
    private String Status;
    private String message;
    private int count;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 87
         * rate : 3.0
         * review : مطعم مميز جدا
         * created_at : 2018-10-09 04:14:00
         * name : omar ahlawii
         */

        private int user_id;
        private float rate;
        private String review;
        private String date;
        private String name;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public float getRate() {
            return rate;
        }

        public void setRate(float rate) {
            this.rate = rate;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getCreated_at() {
            return date;
        }

        public void setCreated_at(String date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
} // class of ReviewResponse

