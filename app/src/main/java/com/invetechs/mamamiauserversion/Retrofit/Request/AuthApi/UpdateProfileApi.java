package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UpdateProfileApi {

    @Multipart
    @POST("UpdateProfile")
    Call<LoginResponse> update(
            @Part MultipartBody.Part file,
            @Part("user_id") RequestBody id,
            @Part("mobilenumber") RequestBody mobilenumber,
            @Part("name") RequestBody user_name,
            @Part("email") RequestBody email ,
            @Part("password") RequestBody password


    ); // name and file ----> contain Image

    @FormUrlEncoded
    @POST("UpdateProfile")
    Call<LoginResponse> updateWithoutImage(
            @Field("user_id") int id,
            @Field("mobilenumber") String mobilenumber,
            @Field("name") String user_name,
            @Field("email") String email ,
            @Field("password") String password


    ); // name and file ----> without Image
}
