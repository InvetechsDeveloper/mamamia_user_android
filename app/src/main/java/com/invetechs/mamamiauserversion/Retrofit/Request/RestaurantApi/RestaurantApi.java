package com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AllRestaurantResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RestaurantDetailsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestaurantApi
{
    @FormUrlEncoded
    @POST("getKitchens")
    Call<AllRestaurantResponse> getRestaurants(
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("category") String category, // 0 all , 1 --> restaurants , 2 ---> houseFood
            @Field("tags") String tags  // 1 --> banquet , 2 --> dessert, 3 --> meal, 4 --> beverge,5 --> offer , 0 --> all
    ); // get all restaurant in area range

    @FormUrlEncoded
    @POST("RestaurantDetails")
    Call<RestaurantDetailsResponse> getRestaurantsDetailss(
            @Field("id") int id,
            @Field("category") int category, // 0 --> all meal, 1 --> breakFast, 2 --> Launch, 3 --> dinner ,
            // 4--->all banquet , 6 ---> wedding , 7 ---> engagment , 8 ---> aqiqa
             @Field("tags") int tags, // 1 --> banquet , 2 --> dessert, 3 --> meal, 4 --> beverge,5 --> offer
             @Field("search") String search // if user enter text search

    ); // get restaurant details
}
