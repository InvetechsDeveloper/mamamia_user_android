package com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrdersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OrdersApi {

    @FormUrlEncoded
    @POST("OrdersByUser")
    Call<OrdersResponse> showOrders(@Field("user_id") int user_id );
}
