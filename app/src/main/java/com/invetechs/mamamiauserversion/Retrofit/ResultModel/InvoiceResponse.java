package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class InvoiceResponse {

    private String code;
    private String Status;
    private String message;
    private int count;
    private List<ArchivesBean> Archives;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ArchivesBean> getArchives() {
        return Archives;
    }

    public void setArchives(List<ArchivesBean> Archives) {
        this.Archives = Archives;
    }

    public static class ArchivesBean {

        private String totalprice;
        private String userplace;
        private int id;
        private String status;
        private String type;
        private String created_at;
        private String ar_orderDate;
        private String ar_orderTime;
        private String en_orderDate;
        private String en_orderTime;
        private String ar_title;
        private int count;
        private String en_title;

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getAr_orderDate() {
            return ar_orderDate;
        }

        public void setAr_orderDate(String ar_orderDate) {
            this.ar_orderDate = ar_orderDate;
        }

        public String getAr_orderTime() {
            return ar_orderTime;
        }

        public void setAr_orderTime(String ar_orderTime) {
            this.ar_orderTime = ar_orderTime;
        }

        public String getEn_orderDate() {
            return en_orderDate;
        }

        public void setEn_orderDate(String en_orderDate) {
            this.en_orderDate = en_orderDate;
        }

        public String getEn_orderTime() {
            return en_orderTime;
        }

        public void setEn_orderTime(String en_orderTime) {
            this.en_orderTime = en_orderTime;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }
    }
}
