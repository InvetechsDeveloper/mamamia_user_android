package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class BanquetOfferResponse
{

    /**
     * code : 200
     * Status : success
     * message : User Banquet
     * data : {"partyname":"sjdjsh","partydate":"الأربعاء، ٢٤ أكتوبر ٢٠١٨","status":"2","Offers":[{"kitchen_id":142,"ar_title":"ماكدونالدز","en_title":"MCdonalds","partyname":"sjdjsh","codeorder":"871540382491","id":582,"partydate":"الأربعاء، ٢٤ أكتوبر ٢٠١٨","totalprice":null}]}
     */

    private String code;
    private String Status;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * partyname : sjdjsh
         * partydate : الأربعاء، ٢٤ أكتوبر ٢٠١٨
         * status : 2
         * Offers : [{"kitchen_id":142,"ar_title":"ماكدونالدز","en_title":"MCdonalds","partyname":"sjdjsh","codeorder":"871540382491","id":582,"partydate":"الأربعاء، ٢٤ أكتوبر ٢٠١٨","totalprice":null}]
         */

        private String partyname;
        private String partydate;
        private String status;
        private List<OffersBean> Offers;

        public String getPartyname() {
            return partyname;
        }

        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public String getPartydate() {
            return partydate;
        }

        public void setPartydate(String partydate) {
            this.partydate = partydate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<OffersBean> getOffers() {
            return Offers;
        }

        public void setOffers(List<OffersBean> Offers) {
            this.Offers = Offers;
        }

        public static class OffersBean {
            /**
             * kitchen_id : 142
             * ar_title : ماكدونالدز
             * en_title : MCdonalds
             * partyname : sjdjsh
             * codeorder : 871540382491
             * id : 582
             * partydate : الأربعاء، ٢٤ أكتوبر ٢٠١٨
             * totalprice : null
             */

            private int kitchen_id;
            private String ar_title;
            private String en_title;
            private String partyname;
            private String codeorder;
            private int id;
            private String partydate;
            private Object totalprice;

            public int getKitchen_id() {
                return kitchen_id;
            }

            public void setKitchen_id(int kitchen_id) {
                this.kitchen_id = kitchen_id;
            }

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getPartyname() {
                return partyname;
            }

            public void setPartyname(String partyname) {
                this.partyname = partyname;
            }

            public String getCodeorder() {
                return codeorder;
            }

            public void setCodeorder(String codeorder) {
                this.codeorder = codeorder;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPartydate() {
                return partydate;
            }

            public void setPartydate(String partydate) {
                this.partydate = partydate;
            }

            public Object getTotalprice() {
                return totalprice;
            }

            public void setTotalprice(Object totalprice) {
                this.totalprice = totalprice;
            }
        }
    }
} // class of BanquetOfferResponse
