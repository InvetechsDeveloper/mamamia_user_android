package com.invetechs.mamamiauserversion.Retrofit.Request.AdvertismentApi;


import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AdvertismentResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface AdvertisMentApi {

    @FormUrlEncoded
    @POST("Advertisement")
    Call<AdvertismentResponse> getAds(
            @Field("user_id") int user_id , // get advertisments
            @Field("mac_address") String mac_address ,// get advertisments
            @Field("type") String type); // get advertisments
} // interface of AdvertisMentApi

