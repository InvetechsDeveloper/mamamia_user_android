package com.invetechs.mamamiauserversion.Retrofit.Request.MenuAndSearchApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MealDetailsResponse;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MealDetailsApi
{
    @FormUrlEncoded
    @POST("MealDetails")
    Call<MealDetailsResponse> getMealDetails(
            @Field("id") int id

    ); // get meal , offer , dessert , beverge details by item ID
}
