package com.invetechs.mamamiauserversion.Retrofit.UrlConnection;



import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitConnection
{
    private static Retrofit retrofit;
    static Gson gson;
    private  static  String token = "" ;
    private  static OkHttpClient.Builder httpClient;
    public static final String MY_PREFS_NAME = "MyPrefsFile";


    public RetrofitConnection() {
    } // default constructor

    public RetrofitConnection(Activity activity) {
        try
        {

            SharedPreferences prefs = activity.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
            if (prefs != null)
            {
                token = prefs.getString("token","");

            }
            Log.e("QS","retrofit : token"+token);
        }
        catch (Exception e)
        {
            Log.e("QS","retrofit : token Exception"+e.toString());
        }
    } // param construcotr

    public  static Retrofit ConnectWith()
    {

        if(httpClient == null) {
            httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request request = original.newBuilder()
                            .header("Authorization", "Bearer " + token)
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                }
            });
        }
                OkHttpClient client = httpClient.build();

        gson = new GsonBuilder()
                .setLenient()
                .create();

        if(retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .baseUrl("https://mammamiaa.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    } // function of connectWith




} // class of RetrofiUrlConnection
