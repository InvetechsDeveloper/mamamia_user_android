package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApi {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("mobilenumber") String mobile_number ,
                              @Field("password") String password  ,
                              @Field("category") String category );
}
