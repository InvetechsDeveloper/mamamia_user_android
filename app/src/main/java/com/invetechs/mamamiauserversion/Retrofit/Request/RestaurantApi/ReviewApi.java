package com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.ReviewResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ReviewApi
{
    @FormUrlEncoded
    @POST("storeReview")
    Call<ReviewResponse> addReview(
            @Field("user_id") int user_id,
            @Field("kitchen_id") int kitchen_id,
            @Field("rate") float rate,
             @Field("review") String review

    ); // add review api
    @FormUrlEncoded
    @POST("getReview")
    Call<ReviewResponse> getReview(
            @Field("kitchen_id") int kitchen_id,
     @Field("user_id") int user_id

    ); // get kitchen review api


} // interface of ReviewApi
