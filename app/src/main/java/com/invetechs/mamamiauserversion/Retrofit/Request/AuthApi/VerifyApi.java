package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface VerifyApi {
    @FormUrlEncoded
    @POST("verify_verificationcode")
    retrofit2.Call<LoginResponse> verify(@Field("user_id") int user_id,
                                         @Field("token") String token
    );  // user VerifyAPi
}
