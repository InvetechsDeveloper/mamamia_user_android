package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

public class CheckResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     */

    private String code;
    private String Status;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
