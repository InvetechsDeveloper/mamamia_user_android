package com.invetechs.mamamiauserversion.Retrofit.Notifications;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NotificationApi {

    @FormUrlEncoded
    @POST("NotificationsByVendor")
    Call<NotificationResponse> showNotifications(@Field("user_id") int user_id);


}
