package com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.KitchenRulesResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RulesApi
{
    @FormUrlEncoded
    @POST("RulesByKitchen")
    Call<KitchenRulesResponse> getRules(
            @Field("kitchen_id") int kitchen_id

    ); // get kitchen rules api
} // interface of RulesApi
