package com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi;



import com.invetechs.mamamiauserversion.Retrofit.ResultModel.UserPointsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PointsApi
{
    @FormUrlEncoded
    @POST("PointsByUser")
    Call<UserPointsResponse> getUserPoints(
            @Field("user_id") int user_id

    ); //getUser Points api
} // interface of pointsApi
