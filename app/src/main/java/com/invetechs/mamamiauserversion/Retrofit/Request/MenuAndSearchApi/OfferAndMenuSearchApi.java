package com.invetechs.mamamiauserversion.Retrofit.Request.MenuAndSearchApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.SearchMenuResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OfferAndMenuSearchApi
{
    @FormUrlEncoded
    @POST("getMeals")
    Call<SearchMenuResponse> getMenuSearch(
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("category") String category, // 1 -- > restaurant , 2 --> housefood
            @Field("tags") String tags, // 1 -- > banquet ,2 -- > dessert ,3 -- > meal  ,4 -- > beverge  ,5 -- > offer
            @Field("search") String search // user text ex :  وجبة  فراخ

    ); // get all menu in area range based on search

}
