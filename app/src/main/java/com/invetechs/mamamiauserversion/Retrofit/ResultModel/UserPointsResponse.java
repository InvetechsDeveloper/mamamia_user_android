package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class UserPointsResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * data : [{"kitchen_id":140,"en_title":"Pizza King","ar_title":"بيتزا كينج","points":60,"discount":"1.00"},{"kitchen_id":141,"en_title":"KFC","ar_title":"كنتاكى","points":100,"discount":"3.00"},{"kitchen_id":142,"en_title":"MCdonalds","ar_title":"ماكدونالدز","points":40,"discount":null}]
     */

    private String code;
    private String Status;
    private String message;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * kitchen_id : 140
         * en_title : Pizza King
         * ar_title : بيتزا كينج
         * points : 60
         * discount : 1.00
         */

        private int kitchen_id;
        private String en_title;
        private String ar_title;
        private int points;
        private String discount;

        public int getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(int kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
} // class of UserPointsResponse

