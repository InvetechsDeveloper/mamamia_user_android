package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

public class CancelOrderDetailsResponse {


    private String code;
    private String Status;
    private String message;
    private OneorderBean oneorder;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OneorderBean getOneorder() {
        return oneorder;
    }

    public void setOneorder(OneorderBean oneorder) {
        this.oneorder = oneorder;
    }

    public static class OneorderBean {

        private String codeorder;
        private int user_id;
        private String component;
        private String en_partyname;
        private String partytype;
        private String partylocation;
        private String guestnum;
        private String partytime;
        private String partydate;
        private String usernote;
        private String status;
        private String created_at;
        private String orderDate;
        private String orderTime;
        private UserBean user;

        public String getCodeorder() {
            return codeorder;
        }

        public void setCodeorder(String codeorder) {
            this.codeorder = codeorder;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public String getEn_partyname() {
            return en_partyname;
        }

        public void setEn_partyname(String en_partyname) {
            this.en_partyname = en_partyname;
        }

        public String getPartytype() {
            return partytype;
        }

        public void setPartytype(String partytype) {
            this.partytype = partytype;
        }

        public String getPartylocation() {
            return partylocation;
        }

        public void setPartylocation(String partylocation) {
            this.partylocation = partylocation;
        }

        public String getGuestnum() {
            return guestnum;
        }

        public void setGuestnum(String guestnum) {
            this.guestnum = guestnum;
        }

        public String getPartytime() {
            return partytime;
        }

        public void setPartytime(String partytime) {
            this.partytime = partytime;
        }

        public String getPartydate() {
            return partydate;
        }

        public void setPartydate(String partydate) {
            this.partydate = partydate;
        }

        public String getUsernote() {
            return usernote;
        }

        public void setUsernote(String usernote) {
            this.usernote = usernote;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(String orderTime) {
            this.orderTime = orderTime;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {

            private int id;
            private String name;
            private Object email;
            private String mobilenumber;
            private String image;
            private String status;
            private String isverified;
            private String category;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getMobilenumber() {
                return mobilenumber;
            }

            public void setMobilenumber(String mobilenumber) {
                this.mobilenumber = mobilenumber;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getIsverified() {
                return isverified;
            }

            public void setIsverified(String isverified) {
                this.isverified = isverified;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
