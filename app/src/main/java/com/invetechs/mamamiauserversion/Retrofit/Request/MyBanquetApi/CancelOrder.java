package com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CancelOrder {

    @FormUrlEncoded
    @POST("CancelOffersBanquetByUser")
    retrofit2.Call<LoginResponse> canceNewlOrder(@Field("codeorder") String codeorder
    );  // reject order
}
