package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class RestaurantDetailsResponse
{


    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * data : {"kitchen":[{"id":140,"en_title":"Pizza King","ar_title":"بيتزا كينج","lat":30.077519,"lng":31.342752,"image":"1540475249.jpg","total_rate":21.5,"count_rate":7}],"images":[{"id":74,"image":"11539611448.jpg","kitchen_id":140,"created_at":"2018-10-15 14:50:48","updated_at":"2018-10-15 14:50:48"},{"id":75,"image":"21539611449.jpg","kitchen_id":140,"created_at":"2018-10-15 14:50:49","updated_at":"2018-10-15 14:50:49"},{"id":76,"image":"31539611449.jpg","kitchen_id":140,"created_at":"2018-10-15 14:50:49","updated_at":"2018-10-15 14:50:49"},{"id":77,"image":"41539611449.jpg","kitchen_id":140,"created_at":"2018-10-15 14:50:49","updated_at":"2018-10-15 14:50:49"}],"meal":[{"id":62,"image":"1542018093.jpg","ar_title":"حاواوشى","en_title":"hawashe","ar_description":"حاواوشى","en_description":"COVERED BY MOZIRLLA CHEESS","price":"25","Additions":[{"id":3,"ar_title":"ارز","en_title":"rice","price":"200"},{"id":4,"ar_title":"جبنة","en_title":"Chesse","price":"200"}]},{"id":85,"image":"1549461343.jpg","ar_title":"شيش طاووك بالجبنة موزيللا","en_title":"SHESH TAWOQ","ar_description":"شيش طاووك بالجبنة موزيللا","en_description":"COVERED BY MOZIRLLA CHEESS","price":"123","Additions":[]}]}
     */

    private String code;
    private String Status;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<KitchenBean> kitchen;
        private List<ImagesBean> images;
        private List<MealBean> meal;

        public List<KitchenBean> getKitchen() {
            return kitchen;
        }

        public void setKitchen(List<KitchenBean> kitchen) {
            this.kitchen = kitchen;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<MealBean> getMeal() {
            return meal;
        }

        public void setMeal(List<MealBean> meal) {
            this.meal = meal;
        }

        public static class KitchenBean {
            /**
             * id : 140
             * en_title : Pizza King
             * ar_title : بيتزا كينج
             * lat : 30.077519
             * lng : 31.342752
             * image : 1540475249.jpg
             * total_rate : 21.5
             * count_rate : 7
             */

            private int id;
            private String en_title;
            private String ar_title;
            private double lat;
            private double lng;
            private String image;
            private double total_rate;
            private int count_rate;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public double getTotal_rate() {
                return total_rate;
            }

            public void setTotal_rate(double total_rate) {
                this.total_rate = total_rate;
            }

            public int getCount_rate() {
                return count_rate;
            }

            public void setCount_rate(int count_rate) {
                this.count_rate = count_rate;
            }
        }

        public static class ImagesBean {
            /**
             * id : 74
             * image : 11539611448.jpg
             * kitchen_id : 140
             * created_at : 2018-10-15 14:50:48
             * updated_at : 2018-10-15 14:50:48
             */

            private int id;
            private String image;
            private int kitchen_id;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getKitchen_id() {
                return kitchen_id;
            }

            public void setKitchen_id(int kitchen_id) {
                this.kitchen_id = kitchen_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class MealBean {
            /**
             * id : 62
             * image : 1542018093.jpg
             * ar_title : حاواوشى
             * en_title : hawashe
             * ar_description : حاواوشى
             * en_description : COVERED BY MOZIRLLA CHEESS
             * price : 25
             * Additions : [{"id":3,"ar_title":"ارز","en_title":"rice","price":"200"},{"id":4,"ar_title":"جبنة","en_title":"Chesse","price":"200"}]
             */

            private int id;
            private String image;
            private String ar_title;
            private String en_title;
            private String ar_description;
            private String en_description;
            private String price;
            private List<AdditionsBean> Additions;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getAr_description() {
                return ar_description;
            }

            public void setAr_description(String ar_description) {
                this.ar_description = ar_description;
            }

            public String getEn_description() {
                return en_description;
            }

            public void setEn_description(String en_description) {
                this.en_description = en_description;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public List<AdditionsBean> getAdditions() {
                return Additions;
            }

            public void setAdditions(List<AdditionsBean> Additions) {
                this.Additions = Additions;
            }

            public static class AdditionsBean {

                private int id;
                private String ar_title;
                private String en_title;
                private String price;
                private boolean isChecked = false;

                public boolean isChecked() {
                    return isChecked;
                }

                public void setChecked(boolean checked) {
                    isChecked = checked;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getAr_title() {
                    return ar_title;
                }

                public void setAr_title(String ar_title) {
                    this.ar_title = ar_title;
                }

                public String getEn_title() {
                    return en_title;
                }

                public void setEn_title(String en_title) {
                    this.en_title = en_title;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }
            }
        }
    }
}
