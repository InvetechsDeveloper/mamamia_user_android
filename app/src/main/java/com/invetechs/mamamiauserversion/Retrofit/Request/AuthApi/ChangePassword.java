package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ChangePassword {


    @FormUrlEncoded
    @POST("change_passworod")
    retrofit2.Call<LoginResponse> change_password(@Field("user_id") int user_id,
                                                  @Field("password") String password
    );  // user change password
}
