package com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MyBanquerOrderResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MyBanquetOrders {

    @FormUrlEncoded
    @POST("OrdersBanquetByUser")
    Call<MyBanquerOrderResponse> showMyBanquerOrders(@Field("user_id") int user_id);
}
