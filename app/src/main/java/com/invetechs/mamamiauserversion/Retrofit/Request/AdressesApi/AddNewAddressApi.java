package com.invetechs.mamamiauserversion.Retrofit.Request.AdressesApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddNewAddressApi
{

    @FormUrlEncoded
    @POST("insertAddressesByUser")
    Call<AddressResponse> addNewAddress(@Field("user_id") int user_id ,
                                        @Field("address") String address,
                                        @Field("lat") double lat,
                                        @Field("lng") double lng,
                                        @Field("note") String note
    ); // add new address

    @FormUrlEncoded
    @POST("getAddressesByUser")
    Call<AddressResponse> selectUserAddresses(@Field("user_id") int user_id
    ); // selectUserAddresses


    @FormUrlEncoded
    @POST("updateAddressesByUser")
    Call<AddressResponse> updateUserAddress(@Field("id") int address_id ,
                                        @Field("user_id") int user_id ,
                                        @Field("address") String address,
                                        @Field("lat") double lat,
                                        @Field("lng") double lng,
                                        @Field("note") String note
    ); // update address

    @FormUrlEncoded
    @POST("deleteAddressesByUser")
    Call<AddressResponse> deleteUserAddress(@Field("id") int address_id
    ); // delete UserAddresse
}
