package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class MealDetailsResponse
{

    private String code;
    private String Status;
    private String message;
    private DataBean data;
    private List<AdditionsBean> Additions;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public List<AdditionsBean> getAdditions() {
        return Additions;
    }

    public void setAdditions(List<AdditionsBean> Additions) {
        this.Additions = Additions;
    }

    public static class DataBean {


        private int id;
        private String ar_title;
        private String en_title;
        private String ar_description;
        private String en_description;
        private String image;
        private String component;
        private String price;
        private int kitchentag_id;
        private int user_id;
        private String status;
        private int neededtime;
        private Object created_at;
        private String updated_at;
        private int customer_num;
        private List<CategoryBean> category;
        private List<ImagesBean> images;
        private List<HoursBean> hours;
        private List<OutletsBean> Outlets;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getKitchentag_id() {
            return kitchentag_id;
        }

        public void setKitchentag_id(int kitchentag_id) {
            this.kitchentag_id = kitchentag_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getNeededtime() {
            return neededtime;
        }

        public void setNeededtime(int neededtime) {
            this.neededtime = neededtime;
        }

        public Object getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Object created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getCustomer_num() {
            return customer_num;
        }

        public void setCustomer_num(int customer_num) {
            this.customer_num = customer_num;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<HoursBean> getHours() {
            return hours;
        }

        public void setHours(List<HoursBean> hours) {
            this.hours = hours;
        }

        public List<OutletsBean> getOutlets() {
            return Outlets;
        }

        public void setOutlets(List<OutletsBean> Outlets) {
            this.Outlets = Outlets;
        }

        public static class CategoryBean {
            /**
             * id : 1
             */

            private int id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }

        public static class ImagesBean {
            /**
             * image : 1542018093.jpg
             */

            private String image;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }

        public static class HoursBean {
            /**
             * from : 1:00
             * to : 6:00
             */

            private String from;
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class OutletsBean {
            /**
             * ar_title : بيتزا كينج
             * en_title : Pizza King
             */

            private String ar_title;
            private String en_title;

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }
        }
    }

    public static class AdditionsBean {
        /**
         * id : 4
         * ar_title : جبنة
         * en_title : Chesse
         * price : 200
         */

        private int id;
        private String ar_title;
        private String en_title;
        private String price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
