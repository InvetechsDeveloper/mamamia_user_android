package com.invetechs.mamamiauserversion.Retrofit.Request.CartApi;

import com.invetechs.mamamiauserversion.Model.CartApiModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CheckOutApi
{

    @POST("OrderCart")
    Call<CartApiModel> checKOut(@Body CartApiModel cart);
} // interface of CheckOutApi
