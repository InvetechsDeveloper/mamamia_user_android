package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class KitchenRulesResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * data : [{"money":"50.00","points":5,"discount":"1.00"},{"money":"100.00","points":10,"discount":"2.00"}]
     */

    private String code;
    private String Status;
    private String message;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * money : 50.00
         * points : 5
         * discount : 1.00
         */

        private String money;
        private String points;
        private String discount;

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
}
