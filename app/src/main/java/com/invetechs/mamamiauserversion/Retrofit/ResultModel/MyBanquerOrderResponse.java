package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class MyBanquerOrderResponse {

    private String code;
    private String Status;
    private String message;
    private DataBean NewOrder;
    private List<DataBean> data;
    private List<DataBean> CancelledOrders;


    public List<DataBean> getCancelledOrders() {
        return CancelledOrders;
    }

    public void setCancelledOrders(List<DataBean> cancelledOrders) {
        CancelledOrders = cancelledOrders;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getNewOrder() {
        return NewOrder;
    }

    public void setNewOrder(DataBean newOrder) {
        NewOrder = newOrder;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class NewOrderBean {

        private String partyname;
        private int id;
        private String partydate;
        private String status;
        private int codeorder;




        public String getPartyname() {
            return partyname;
        }

        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPartydate() {
            return partydate;
        }

        public void setPartydate(String partydate) {
            this.partydate = partydate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getCodeorder() {
            return codeorder;
        }

        public void setCodeorder(int codeorder) {
            this.codeorder = codeorder;
        }
    }

    public static class DataBean {

        private String ar_title;
        private String en_title;
        private String partyname;
        private int id;
        private String partydate;
        private String status;
        private String totalprice;
        private String codeorder;


        public DataBean() {
        }

        public DataBean(String partyname, int id, String partydate , String status , String codeorder) {
            this.partyname = partyname;
            this.id = id;
            this.partydate = partydate;
            this.status = status;
            this.codeorder = codeorder;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getPartyname() {
            return partyname;
        }

        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPartydate() {
            return partydate;
        }

        public void setPartydate(String partydate) {
            this.partydate = partydate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getCodeorder() {
            return codeorder;
        }

        public void setCodeorder(String codeorder) {
            this.codeorder = codeorder;
        }
    }
}
