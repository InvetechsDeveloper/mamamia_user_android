package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.Comparator;
import java.util.List;

public class AllRestaurantResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * data : [{"ar_title":"بيتزا هت","en_title":"MCdonalds","minicharge":"50","lat":"30.078392","lng":"31.339374","total_rate":null,"count_rate":null,"deliveryflag":"1","deliverypriceperkilo":"100"},{"ar_title":"بيتزا كينج","en_title":"Pizza King","minicharge":"50","lat":"30.077519","lng":"31.342752","total_rate":null,"count_rate":null,"deliveryflag":"1","deliverypriceperkilo":"100"},{"ar_title":"بيتزا هت","en_title":"KFC","minicharge":"50","lat":"30.073072","lng":"31.345873","total_rate":null,"count_rate":null,"deliveryflag":"1","deliverypriceperkilo":"100"},{"ar_title":"بيتزا هت","en_title":"MCdonalds","minicharge":"50","lat":"30.094201","lng":"31.335362","total_rate":null,"count_rate":null,"deliveryflag":"1","deliverypriceperkilo":"100"},{"ar_title":"بيتزا كينج","en_title":"Pizza King","minicharge":"50","lat":"30.111153","lng":"31.345889","total_rate":null,"count_rate":null,"deliveryflag":"1","deliverypriceperkilo":"100"}]
     */

    private String code;
    private String Status;
    private String message;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ar_title : بيتزا هت
         * en_title : MCdonalds
         * minicharge : 50
         * lat : 30.078392
         * lng : 31.339374
         * total_rate : null
         * count_rate : null
         * deliveryflag : 1
         * deliverypriceperkilo : 100
         */

        private int id;
        private String ar_title;
        private String en_title;
        private String minicharge;
        private double lat;
        private double lng;
        private double total_rate;
        private int count_rate;
        private String deliveryflag;
        private String deliverypriceperkilo;
        private  String delivermaxbill;
        private  String image;
        private double totalDelivery ;
        private double distanceToUser ;

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getMinicharge() {
            return minicharge;
        }

        public void setMinicharge(String minicharge) {
            this.minicharge = minicharge;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        public double getTotal_rate() {
            return total_rate;
        }

        public void setTotal_rate(double total_rate) {
            this.total_rate = total_rate;
        }

        public int getCount_rate() {
            return count_rate;
        }

        public void setCount_rate(int count_rate) {
            this.count_rate = count_rate;
        }

        public String getDeliveryflag() {
            return deliveryflag;
        }

        public void setDeliveryflag(String deliveryflag) {
            this.deliveryflag = deliveryflag;
        }

        public String getDeliverypriceperkilo() {
            return deliverypriceperkilo;
        }

        public void setDeliverypriceperkilo(String deliverypriceperkilo) {
            this.deliverypriceperkilo = deliverypriceperkilo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDelivermaxbill() {
            return delivermaxbill;
        }

        public void setDelivermaxbill(String delivermaxbill) {
            this.delivermaxbill = delivermaxbill;
        }

        public double getTotalDelivery() {
            return totalDelivery;
        }

        public void setTotalDelivery(double totalDelivery) {
            this.totalDelivery = totalDelivery;
        }

        public double getDistanceToUser() {
            return distanceToUser;
        }

        public void setDistanceToUser(double distanceToUser) {
            this.distanceToUser = distanceToUser;
        }


    }
}
