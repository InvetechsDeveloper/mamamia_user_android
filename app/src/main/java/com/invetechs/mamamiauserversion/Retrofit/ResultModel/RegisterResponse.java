package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

public class RegisterResponse
{

    /**
     * code : 200
     * Status : success
     * message : Register new user done successfully
     * userdata : {"name":"ibrahim","mobilenumber":"56","email":"ib@gmail.com","category":"user","image":"1538993962.png","status":0,"isverified":0,"updated_at":"2018-10-08 10:19:22","created_at":"2018-10-08 10:19:22","id":84,"verificationcode":6188}
     */

    private String code;
    private String Status;
    private String message;
    private UserdataBean userdata;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserdataBean getUserdata() {
        return userdata;
    }

    public void setUserdata(UserdataBean userdata) {
        this.userdata = userdata;
    }

    public static class UserdataBean {
        /**
         * name : ibrahim
         * mobilenumber : 56
         * email : ib@gmail.com
         * category : user
         * image : 1538993962.png
         * status : 0
         * isverified : 0
         * updated_at : 2018-10-08 10:19:22
         * created_at : 2018-10-08 10:19:22
         * id : 84
         * verificationcode : 6188
         */

        private String name;
        private String mobilenumber;
        private String email;
        private String category;
        private String image;
        private int status;
        private int isverified;
        private String updated_at;
        private String created_at;
        private int id;
        private String verificationcode;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getIsverified() {
            return isverified;
        }

        public void setIsverified(int isverified) {
            this.isverified = isverified;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getVerificationcode() {
            return verificationcode;
        }

        public void setVerificationcode(String verificationcode) {
            this.verificationcode = verificationcode;
        }
    }
}
