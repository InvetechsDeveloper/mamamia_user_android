package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ProfileApi {

    @FormUrlEncoded
    @POST("Getvendorprofile")
    Call<LoginResponse> show_profile(@Field("id") int id);
}
