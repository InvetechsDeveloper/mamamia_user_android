package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

public class UserPointResponse
{

    /**
     * code : 200
     * Status : success
     * message : code sent successfully
     * discount : 30.00
     */

    private String code;
    private String Status;
    private String message;
    private  String point;
    private float vat ;
    private double minCharge;
    private double discountPerPoint;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public double getMinCharge() {
        return minCharge;
    }

    public void setMinCharge(double minCharge) {
        this.minCharge = minCharge;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }

    public double getDiscountPerPoint() {
        return discountPerPoint;
    }

    public void setDiscountPerPoint(double discountPerPoint) {
        this.discountPerPoint = discountPerPoint;
    }
}
