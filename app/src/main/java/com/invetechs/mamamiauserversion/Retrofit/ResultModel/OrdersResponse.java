package com.invetechs.mamamiauserversion.Retrofit.ResultModel;

import java.util.List;

public class OrdersResponse {

    private String code;
    private String Status;
    private String message;
    private int count;
    private List<DataBean> data;
    private List<DataBean> NewOrders;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<DataBean> getNewOrders() {
        return NewOrders;
    }

    public void setNewOrders(List<DataBean> newOrders) {
        NewOrders = newOrders;
    }

    public static class DataBean {
        private int id;
        private String userplace;
        private String totalprice;
        private String status;
        private MealBean meal;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public MealBean getMeal() {
            return meal;
        }

        public void setMeal(MealBean meal) {
            this.meal = meal;
        }

        public static class MealBean {
            /**
             * ar_title : بوفتيك
             * en_title : bofteek
             */

            private String ar_title;
            private String en_title;

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }
        }
    }
}
