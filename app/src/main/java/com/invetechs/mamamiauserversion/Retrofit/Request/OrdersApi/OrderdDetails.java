package com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.CancelOrderDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrderDetailsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OrderdDetails {

    @FormUrlEncoded
    @POST("Orderdetails")
    Call<OrderDetailsResponse> getOneOrderDetailsApi(@Field("id") String id,
    @Field("type") String type);


    @FormUrlEncoded
    @POST("OrderDetailsCancelled")
    Call<CancelOrderDetailsResponse> getOneOrderCancelDetailsApi(@Field("codeorder") String codeorder);


}
