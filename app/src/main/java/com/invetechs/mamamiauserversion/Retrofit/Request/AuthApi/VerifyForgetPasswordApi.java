package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface VerifyForgetPasswordApi {

    @FormUrlEncoded
    @POST("verify_verificationcode_forget")
    retrofit2.Call<LoginResponse> verifyForgetPass(@Field("mobilenumber") String mobilenumber,
                                                   @Field("token") String token ,   // user VerifypasswordAPi
                                                   @Field("category") String category);  // user VerifypasswordAPi
}
