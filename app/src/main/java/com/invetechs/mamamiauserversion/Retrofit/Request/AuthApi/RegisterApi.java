package com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RegisterResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RegisterApi
{
    @Multipart
    @POST("registration")
    Call<RegisterResponse> registerApiWithImage(
            @Part MultipartBody.Part file,
            @Part("name") RequestBody name,
            @Part("mobilenumber") RequestBody  mobilenumber,
            @Part("password") RequestBody  password,
            @Part("email") RequestBody  email,
            @Part("category") RequestBody category

    ); // register with image

    @FormUrlEncoded
    @POST("registration")
    Call<RegisterResponse> registerApiWithOutImage(
            @Field("name") String name,
            @Field("mobilenumber") String mobilenumber,
            @Field("password") String password,
            @Field("email") String email,
            @Field("category") String category


    ); // register with image
}
