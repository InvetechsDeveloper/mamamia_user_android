package com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi;

import com.invetechs.mamamiauserversion.Retrofit.ResultModel.BanquetOfferResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MyBanquetOfferApi
{
    @FormUrlEncoded
    @POST("OffersBanquetByUser")
    Call<BanquetOfferResponse> getBanquetOffer(
            @Field("user_id") int user_id
    ); // get offers

    @FormUrlEncoded
    @POST("AcceptOfferBanquetByUser")
    Call<BanquetOfferResponse> acceptBanquetOffer(
            @Field("codeorder") String codeorder,
            @Field("kitchen_id") int kitchen_id
    ); // accept offer

    @FormUrlEncoded
    @POST("RefusedOfferBanquetByUser")
    Call<BanquetOfferResponse> refuseBanquetOffer(
            @Field("codeorder") String codeorder,
            @Field("kitchen_id") int kitchen_id
    ); // refuse offer
} // interface of MyBanquetOfferApi
