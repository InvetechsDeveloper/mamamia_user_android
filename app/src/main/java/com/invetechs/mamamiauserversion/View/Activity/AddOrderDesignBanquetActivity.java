package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.OrderObserver;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

public class AddOrderDesignBanquetActivity extends Language {

    // bind views
    @BindView(R.id.et_order_type)
    EditText et_order_type;

    // vars
    String banquetComponents = "";
    ShowDialog toastDialog;
    OrderObserver orderObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order_design_banquet);
        ButterKnife.bind(this);
        initLanguage();
        getBanquetComponent();
        toastDialog = new ShowDialog();

        orderObserver = OrderObserver.getInstance();

        Log.e("QP", "observer onCreate : " + orderObserver.getOrder_data());
        orderObserver.getOrder_data();
        if (banquetComponents.equals("")) {
            et_order_type.setText(orderObserver.getOrder_data());
        }

    } // function of onCreate


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void getBanquetComponent() {
        Intent intent = getIntent();
        if (intent != null)
        {
            banquetComponents = intent.getStringExtra("component");
            Log.i("QP", "component " + banquetComponents);

            if (banquetComponents == null)
                banquetComponents = "";
        }
        if (!banquetComponents.equals(""))
            et_order_type.setText(banquetComponents);

    } // function of getBanquetCompnent

    public void SaveBanquetDetails(View view) {

        banquetComponents = et_order_type.getText().toString();
        orderObserver.setOrder_data(banquetComponents);

        Log.e("QP", "observer SaveBanquetDetails : " + orderObserver.getOrder_data());

        if (banquetComponents.equals("")) {
            toastDialog.initDialog(getString(R.string.pleaseAddBanquetCompnent), AddOrderDesignBanquetActivity.this);
        } //

        else {

            Intent intent = new Intent(getApplicationContext(), DesignBanquet.class);
            intent.putExtra("component", banquetComponents);
            orderObserver.setOrder_data(banquetComponents);
            startActivity(intent);
            finish();
        } //
    } // function of save banquet details button

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }
} // class of AddOrderDesignBanquetActivity
