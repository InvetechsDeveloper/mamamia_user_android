package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.MenuAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Model.RulesModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.RestaurantApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RestaurantDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BanquetDetailsActivity extends Language {

    // bind views

    @BindView(R.id.viewAllBanquet)
    View viewAllBanquet;

    @BindView(R.id.viewWedding)
    View viewWedding;

    @BindView(R.id.viewEngagment)
    View viewEngagment;

    @BindView(R.id.viewAqiqa)
    View viewAqiqa;

    @BindView(R.id.restaurant_menu_banquet)
    RecyclerView recyclerView;

    @BindView(R.id.txt_restaurantName)
    TextView txt_restaurantName;

    @BindView(R.id.txt_restaaurantTotalPeopleRate)
    TextView txt_restaaurantTotalPeopleRate;

    @BindView(R.id.txt_restaurantDistance)
    TextView txt_restaurantDistance;

    @BindView(R.id.txt_restaurantRate)
    TextView txt_restaurantRate;

    @BindView(R.id.rateBar_restaurant)
    AppCompatRatingBar rateBar_restaurant;

    @BindView(R.id.searchMenu)
    SearchView searchViewMenu;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbar_layout;

    // vars

    Handler handler;
    CustomDialogProgress progress;
    int kitchenId = 0;
    String restaurantName = "", restaurantImage = "";
    int restaurantId = 0, totalPeopleRate = 0, deliveryMaxBill = 0;
    double rate, distanceWithKilo, latitude, longitude, distanceWithMeter, totalDelivery = 0;
    List<RestaurantDetailsResponse.DataBean.MealBean> menuListApi = new ArrayList<>(), menuList = new ArrayList<>();
    List<RestaurantDetailsResponse.DataBean.ImagesBean> imageList = new ArrayList<>();
    int counterOfCallingImageSlider = 0;
    ShowDialog toastDialog;
    MenuAdapter adapter; // fill this menu with  dessert , offers , meals , beverge
    ArrayList<RulesModel> arrayList = new ArrayList<>();
    SharedPreferences preferences;
    ToolBarConfig toolBarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banquet_details_scrolling);
        ButterKnife.bind(this);
        initLanguage();
        getRestaurantId();

        getMenuFromApi(4, 1, "0"); // get all meals default

        initMenu(); // menu list
        setToolBarConfig();
        toastDialog = new ShowDialog();


        searchMenu();

    } // function of onCreate


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void searchMenu() {
        searchViewMenu.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getMenuFromApi(0, 0, query);
                searchViewMenu.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //getMenuFromApi(0,0,newText);
                return false;
            }
        });
    } // function of searchOnRestaurants

    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.restaurant));
        toolBarConfig.addNotification();
        toolBarConfig.initCartBadge();
    } // function of setToolBarConfig

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume


    private void getRestaurantId() {
        Intent intent = getIntent();
        if (intent != null) {
            restaurantId = intent.getIntExtra("id", 0);
            deliveryMaxBill = Integer.parseInt(intent.getStringExtra("maxbill"));
            totalDelivery = intent.getDoubleExtra("totalDelivery", 0);
            distanceWithKilo = intent.getDoubleExtra("distanceWithKilo", 0);
            latitude = intent.getDoubleExtra("lat", 0);
            longitude = intent.getDoubleExtra("lng", 0);
        }

    } // function of getRestaurantId

    @OnClick(R.id.review)
    public void makeReview() {

        if (toolBarConfig.getPrefUserId() == 0) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        } // user not login
        else {
            Intent reviewIntent = new Intent(this, ReviewActivity.class);
            reviewIntent.putExtra("id", kitchenId);
            startActivity(reviewIntent);
        } // user already login

    } // review function


    private void initMenu() {
        adapter = new MenuAdapter(this, menuList, BanquetDetailsActivity.this, restaurantId, deliveryMaxBill, totalDelivery, distanceWithKilo);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    } // function of init menu

    private void getMenuFromApi(final int timeCategory, final int typeCategory, final String searchText) // timeCategory mean launch , dinner ,..  , typeCategory mean meal , dessert ,..
    {

        if (menuList.size() > 0 && menuListApi.size() > 0) {
            menuListApi.clear();
            menuList.clear();
        }
        progress = new CustomDialogProgress();
        progress.init(this);


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(BanquetDetailsActivity.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final RestaurantApi restaurantApiApi = retrofit.create(RestaurantApi.class);

                Call<RestaurantDetailsResponse> getConnection = restaurantApiApi.getRestaurantsDetailss(restaurantId, timeCategory, typeCategory, searchText);

                getConnection.enqueue(new Callback<RestaurantDetailsResponse>() {
                    @Override
                    public void onResponse(Call<RestaurantDetailsResponse> call, Response<RestaurantDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();

                            response.body();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {

                                menuListApi = response.body().getData().getMeal();

                                if (response.body().getData().getKitchen().get(0).getCount_rate() != 0)
                                    rate = response.body().getData().getKitchen().get(0).getTotal_rate() /
                                            response.body().getData().getKitchen().get(0).getCount_rate();
                                else rate = 0;

                                preferences = getSharedPreferences("user", MODE_PRIVATE);

                                if (preferences.getString("language", "ar").equals("ar")) ;
                                restaurantName = response.body().getData().getKitchen().get(0).getAr_title();
                                if (preferences.getString("language", "ar").equals("en"))
                                    restaurantName = response.body().getData().getKitchen().get(0).getEn_title();

                                kitchenId = response.body().getData().getKitchen().get(0).getId();


                                totalPeopleRate = response.body().getData().getKitchen().get(0).getCount_rate();


                                distanceWithMeter = distance(response.body().getData().getKitchen().get(0).getLat()
                                        , response.body().getData().getKitchen().get(0).getLng(), latitude, longitude);

                                restaurantImage = response.body().getData().getKitchen().get(0).getImage();

                                imageList = response.body().getData().getImages();

                                Log.i("QP", " images List : " + imageList.size());
                                if (counterOfCallingImageSlider == 0)
                                    imageSlider(imageList);

                                fillMenuList();
                                setRestaurantData();

                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<RestaurantDetailsResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }

        }.start();
    } // function of getMenuFromApi

    public void enableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                toolbar_layout.getLayoutParams();
        params.setScrollFlags(
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        );
        toolbar_layout.setLayoutParams(params);
    }

    public void disableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                toolbar_layout.getLayoutParams();
        params.setScrollFlags(0);
        toolbar_layout.setLayoutParams(params);
    }


    private void fillMenuList() {
        app_bar.setExpanded(true);
        if (menuListApi.size() <= 1) {
            disableScroll();
        } else {
            enableScroll();
        }
        for (int i = 0; i < menuListApi.size(); i++) {
            menuList.add(menuListApi.get(i));
        }
        adapter.notifyDataSetChanged();
        if (menuListApi.size() <= 0) {
            toastDialog.initDialog(getString(R.string.notFound), BanquetDetailsActivity.this);

        } // not found meals
    } // function of fillMenuList

    private void setRestaurantData() {
        int precision1 = 10; //keep 1 digits
        rate = Math.floor(rate * precision1 + .5) / precision1;

        distanceWithKilo = distanceWithMeter / 1000;
        int precision = 10; //keep 1 digits
        distanceWithKilo = Math.floor(distanceWithKilo * precision + .5) / precision;

        txt_restaurantName.setText(restaurantName);
        txt_restaaurantTotalPeopleRate.setText(" (" + totalPeopleRate + ") ");
        txt_restaurantDistance.setText(distanceWithKilo + " " + getResources().getString(R.string.km));
        txt_restaurantRate.setText(rate + "");
        rateBar_restaurant.setRating((float) rate);


    } // function of setRestaurantData

    public float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    } // distance between restaurant and user

    public void AllButton(View view) {
        viewAllBanquet.setVisibility(View.VISIBLE);
        viewWedding.setVisibility(View.INVISIBLE);
        viewEngagment.setVisibility(View.INVISIBLE);
        viewAqiqa.setVisibility(View.INVISIBLE);
        getMenuFromApi(4, 1, "0");
    } //choose all

    public void WeddingButton(View view) {
        viewAllBanquet.setVisibility(View.INVISIBLE);
        viewWedding.setVisibility(View.VISIBLE);
        viewEngagment.setVisibility(View.INVISIBLE);
        viewAqiqa.setVisibility(View.INVISIBLE);
        getMenuFromApi(6, 1, "0");
    } // choose wedding

    public void EngagmentLButton(View view) {
        viewAllBanquet.setVisibility(View.INVISIBLE);
        viewWedding.setVisibility(View.INVISIBLE);
        viewEngagment.setVisibility(View.VISIBLE);
        viewAqiqa.setVisibility(View.INVISIBLE);
        getMenuFromApi(7, 1, "0");
    } // choose engagment

    public void AqiqaButton(View view) {
        viewAllBanquet.setVisibility(View.INVISIBLE);
        viewWedding.setVisibility(View.INVISIBLE);
        viewEngagment.setVisibility(View.INVISIBLE);
        viewAqiqa.setVisibility(View.VISIBLE);
        getMenuFromApi(8, 1, "0");
    } // choose aqiqa


    public void imageSlider(List<RestaurantDetailsResponse.DataBean.ImagesBean> urls) {

        SliderLayout sliderShow = findViewById(R.id.slider);

        Log.i("QP", " images url : " + urls.size());

        for (int i = 0; i < urls.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image("https://mammamiaa.com/cpanel/upload/kitchen/" + urls.get(i).getImage());
            sliderShow.addSlider(textSliderView);
            Log.e("QP", "Image : " + urls.get(i).getImage());
        }
        counterOfCallingImageSlider = 1;

    } // imageSlider function

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
}
