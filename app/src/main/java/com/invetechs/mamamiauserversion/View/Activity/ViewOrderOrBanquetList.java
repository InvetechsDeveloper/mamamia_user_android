package com.invetechs.mamamiauserversion.View.Activity;

import android.content.SharedPreferences;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ViewOrderOrBanquetList extends Language {

    // bind views
    @BindView(R.id.tv_order_type)
    TextView tv_order_type;

    // vars
    String component;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order_or_banquet_list);
        ButterKnife.bind(this);
        initLanguage();
        getOrderList();
        setTextWithData();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void setTextWithData() {
        tv_order_type.setText(component);
    } // set text with data

    private void getOrderList() {
        if (getIntent() != null) {
            component = getIntent().getStringExtra("order_list");

        }
    } // get order list and show to user


}
