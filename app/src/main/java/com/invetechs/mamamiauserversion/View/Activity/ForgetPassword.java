package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Language.MyContextWrapper;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.ChangePassword;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.ForgetPasswordApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.VerifyForgetPasswordApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgetPassword extends AppCompatActivity {

    // Bind views

    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;

    @BindView(R.id.tv_resend_code)
    TextView tv_resend_code;

    @BindView(R.id.et_phone_number)
    TextInputEditText et_phone_number;

    @BindView(R.id.et_new_password)
    TextInputEditText etNewPassword;

    @BindView(R.id.et_confirm_new_password)
    TextInputEditText etConfirmNewPassword;

    @BindView(R.id.et_code)
    TextInputEditText etCode;


    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    // vars

    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String code, mobile, category = "user";
    int id;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    ShowDialog toastDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        hideKeyboard();
        toastDialog = new ShowDialog();
        initLanguage();
        initToolBar();
    } // on create function

    private void initToolBar() {
        setSupportActionBar(tool_bar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

//        if (sharedPreferences.getString("language", "ar").equals("ar")) {
//
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.arabic_back);
//
//        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.english_back);
//
//        }

        tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    } // function of initToolBar

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);

            et_phone_number.setTypeface(null);
            etNewPassword.setTypeface(null);
            etConfirmNewPassword.setTypeface(null);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
            et_phone_number.setTypeface(null);
            etNewPassword.setTypeface(null);
            etConfirmNewPassword.setTypeface(null);

        }


    } // initialize language and font

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    @OnClick(R.id.tv_resend_code)
    public void resendCode() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend_code.setText(getString(R.string.second_remaining) + millisUntilFinished / 1000);
                tv_resend_code.setEnabled(false);

            }

            public void onFinish() {
                tv_resend_code.setText(getString(R.string.resend_code));
                toastDialog.initDialog(getString(R.string.code_send), ForgetPassword.this);
                resendingCode();
            }

        }.start();
    } // click on resend code

    private boolean ValidateMobile() {

        String Mobile = et_phone_number.getText().toString().trim();

        if (Mobile.length() < 10) {
            et_phone_number.setError(getString(R.string.error_valid_mobile));
            return false;
        }
        if (Mobile.length() > 10) {
            et_phone_number.setError(getString(R.string.enter_valid_number));
            return false;
        }

        if (Mobile.length() == 10) {
            et_phone_number.setError(null);
        }

        return true;
    } // validation for mobile phone

    public boolean ValidatePassword() {

        String newPassword = etNewPassword.getText().toString().trim();
        String confirmPassword = etConfirmNewPassword.getText().toString().trim();

        if (newPassword.isEmpty()) {
            etNewPassword.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (newPassword.length() < 4) {
            etNewPassword.setError(getString(R.string.valid_password_number));
            return false;

        }

        if (confirmPassword.isEmpty()) {
            etConfirmNewPassword.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (!newPassword.equals(confirmPassword)) {
            toastDialog.initDialog(getString(R.string.password_not_match), ForgetPassword.this);
            return false;

        }
        return true;

    }  // validation for password

    public boolean ValidateVerificationCode() {

        code = etCode.getText().toString().trim();

        if (code.isEmpty()) {
            etCode.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (code.length() != 4) {
            etCode.setError(getString(R.string.invalid_code));
            return false;
        }

        return true;

    } // validate for code

    @OnClick(R.id.btn_proceed)
    public void enterPhoneNumber() {
        if (!ValidateMobile()) {
            return;
        }

        String phone = et_phone_number.getText().toString().trim();
        mobile = phone;
        forgetPassword();
    } // proceed after enter phone number

    @OnClick(R.id.btn_confirm)
    public void enterCode() {
        if (!ValidateVerificationCode()) {
            return;
        }
        verify();

    } // entet code number

    @OnClick(R.id.btn_save)
    public void savePassword() {
        if (!ValidatePassword()) {
            return;
        }
        updatePassword();
    } // enter new password and confirm password

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void resendingCode() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitConnection.ConnectWith();
                final ForgetPasswordApi userApi = retrofit.create(ForgetPasswordApi.class);

                final Call<LoginResponse> getInterestConnection = userApi.forgetPass(mobile, category);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();

                            if (code.equals("200")) {

                                toastDialog.initDialog(getString(R.string.code_send), ForgetPassword.this);

                            } // login success

                            else if (code.equals("1313")) {

                                toastDialog.initDialog(getString(R.string.invalid_mobile), ForgetPassword.this);
                                progress.dismiss();
                            } // user need admin confirmation

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), ForgetPassword.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // resend code after one muinite

    private void forgetPassword() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(ForgetPassword.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ForgetPasswordApi userApi = retrofit.create(ForgetPasswordApi.class);

                final Call<LoginResponse> getInterestConnection = userApi.forgetPass(mobile, category);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String message = response.body().getMessage();
                            String code = response.body().getCode();
                            LoginResponse.UserdataBean user_data = response.body().getUserdata();
                            String verification = String.valueOf(user_data.getVerificationcode());

                            if (code.equals("200")) {
                                progress.dismiss();
                                viewFlipper.showNext();
                                toastDialog.initDialog(getString(R.string.code_send), ForgetPassword.this);
                                Log.i("QP", "mobile number : " + mobile);

                            } // login success

                            else {
                                toastDialog.initDialog(message, ForgetPassword.this);
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), ForgetPassword.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
            }

        }.start();
    } // function of forget password

    private void verify() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final VerifyForgetPasswordApi userApi = retrofit.create(VerifyForgetPasswordApi.class);

                final Call<LoginResponse> getInterestConnection = userApi.verifyForgetPass(mobile, code, category);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {
                            Log.i("QP", "mobile  : " + mobile);
                            Log.i("QP", "token  : " + code);

                            String code = response.body().getCode();
                            if (code.equals("200")) {
                                LoginResponse.UserdataBean user = response.body().getUserdata();
                                id = user.getId();
                                viewFlipper.showNext();
                                progress.dismiss();

                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.wrong_token), ForgetPassword.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), ForgetPassword.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // verfy

    private void updatePassword() {

        final String pass = etNewPassword.getText().toString();
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ChangePassword userApi = retrofit.create(ChangePassword.class);

                 final Call<LoginResponse> getInterestConnection = userApi.change_password(id, pass);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "exception : " + code);
                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.password_saved), ForgetPassword.this);
                                progress.dismiss();
                                Intent intent = new Intent(ForgetPassword.this, LoginActivity.class);
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("password", pass);
                                editor.apply();
                                startActivity(intent);
                                finish();
                            } // update password

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), ForgetPassword.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
            }

        }.start();
    } // function of updatePassword

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        try {
            progress.dismiss();
            toastDialog.dismissDialog();

        } catch (Exception e) {

        }

    }


}
