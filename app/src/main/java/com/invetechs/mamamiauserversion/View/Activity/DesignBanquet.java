package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.OrderObserver;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.BanquetApi.DesignBanquetApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.CheckResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Fragment.DatePickerFragment;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.View.Fragment.TimePickerFragment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DesignBanquet extends Language
        implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    // Bind views
    @BindView(R.id.btn_view_order)
    Button btn_view_order;

    @BindView(R.id.tv_party_time)
    TextView tv_party_time;

    @BindView(R.id.tv_party_date)
    TextView tv_party_date;

    @BindView(R.id.tv_party_location)
    TextView tv_party_location;

    @BindView(R.id.banq_partyName)
    EditText ed_banq_partyName;

    @BindView(R.id.banq_partyType)
    Spinner sp_banq_partyType;

    @BindView(R.id.banq_numberOfPersons)
    EditText ed_banq_numberOfPersons;

    @BindView(R.id.banq_customerComment)
    EditText ed_banq_customerComment;

    @BindView(R.id.date)
    RelativeLayout relative_date;

    @BindView(R.id.party_time)
    RelativeLayout relative_time;

    @BindView(R.id.linearLocation)
    LinearLayout linearLocation;


    // variables
    String banquetNumOfPersons = "";
    String banquetCompnent = "", banquetPartyName = "", banquetPartyLocation = "",
            banquetPartyTime = "", banquetPartyDate = "", banquetCustomerComment = "", banquetType = "";
    double lat, lng;
    String choosenDate = "", currentDate = "";
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    OrderObserver orderObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_banquet);
        ButterKnife.bind(this);
        orderObserver = OrderObserver.getInstance();
        initLanguage();
        hideKeyboard();
        getDataFromAdressActivity();
        getBanquetComponent();
        getSharedtLocation();
        initSpinnerWithBanquetTypes();
        toastDialog = new ShowDialog();
        setToolBarConfig();
        getObservableData();

    } // function of onCreate

    private void getObservableData() {

        try {
            orderObserver.getOrder_data();
            orderObserver.getCustomer_comments();
            orderObserver.getParty_date();
            orderObserver.getParty_time();
            orderObserver.getPerson_number();
            orderObserver.getParty_name();

            Log.i("QP", "getOrder_data " + orderObserver.getOrder_data());


            if (banquetPartyName.equals(""))
                ed_banq_partyName.setText(orderObserver.getParty_name());

            if (banquetCustomerComment.equals(""))
                ed_banq_customerComment.setText(orderObserver.getCustomer_comments());

            if (banquetPartyDate.equals(""))
                tv_party_date.setText(orderObserver.getParty_date());

            if (banquetPartyTime.equals(""))
                tv_party_time.setText(orderObserver.getParty_time());


            if (banquetType.equals(""))
                banquetType.equals(orderObserver.getParty_type());

            Log.i("QP", " get person number : " + banquetNumOfPersons);
            Log.i("QP", " get party time : " + banquetPartyTime);

            if (banquetNumOfPersons.equals(""))
                ed_banq_numberOfPersons.setText(orderObserver.getPerson_number());

        } catch (Exception e) {
        }
    } // get observable saved data

    private void getDataFromAdressActivity() {
        Intent intent = getIntent();
        if (intent != null) {
            String party_name = intent.getStringExtra("banquet_name");
            String banquetCompnent = intent.getStringExtra("component");
            Log.i("QP", "party_name " + party_name);
            Log.i("QP", "component " + banquetCompnent);
            ed_banq_partyName.setText(party_name);
        }

    }

    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.designBanquet));
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void initSpinnerWithBanquetTypes() {

        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add(getString(R.string.wedding));
        spinnerArray.add(getString(R.string.engagment));
        spinnerArray.add(getString(R.string.aqiqa));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/GESSTwoMedium.otf");
                    ((TextView) v).setTypeface(externalFont);

                } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
                    ((TextView) v).setTypeface(externalFont);

                }

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/GESSTwoMedium.otf");
                    ((TextView) v).setTypeface(externalFont);

                } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
                    ((TextView) v).setTypeface(externalFont);

                }

                return v;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_banq_partyType.setAdapter(adapter);
    } // function of initSpinnerWithBanquetTypes

    private void getDataSaved() {

        banquetPartyName = ed_banq_partyName.getText().toString();
        if (!ed_banq_numberOfPersons.getText().toString().isEmpty())
            banquetNumOfPersons = ed_banq_numberOfPersons.getText().toString();
        banquetPartyLocation = tv_party_location.getText().toString();
        banquetPartyTime = tv_party_time.getText().toString();
        banquetPartyDate = tv_party_date.getText().toString();
        banquetCustomerComment = ed_banq_customerComment.getText().toString();
        banquetType = sp_banq_partyType.getSelectedItem().toString();

    } // function of getDataFromUser

    private boolean getDataFromUser() {

        banquetPartyName = ed_banq_partyName.getText().toString();
        if (!ed_banq_numberOfPersons.getText().toString().isEmpty())
            banquetNumOfPersons = ed_banq_numberOfPersons.getText().toString();
        banquetPartyLocation = tv_party_location.getText().toString();
        banquetPartyTime = tv_party_time.getText().toString();
        banquetPartyDate = tv_party_date.getText().toString();
        banquetCustomerComment = ed_banq_customerComment.getText().toString();
        banquetType = sp_banq_partyType.getSelectedItem().toString();


        Log.i("QP", "party time = " + orderObserver.getParty_time());

        if (orderObserver.getOrder_data().isEmpty()) {
            toastDialog.initDialog(getString(R.string.pleaseAddBanquetCompnent), DesignBanquet.this);
            return false;
        } else if (banquetPartyName.equals("")) {
            toastDialog.initDialog(getString(R.string.enterPartyName), DesignBanquet.this);
            return false;
        } else if (banquetPartyLocation.equals("")) {
            toastDialog.initDialog(getString(R.string.enterPartyLocation), DesignBanquet.this);
            return false;
        } else if (banquetPartyTime.equals("")) {
            toastDialog.initDialog(getString(R.string.enterPartyTime), DesignBanquet.this);
            return false;
        } else if (banquetPartyDate.equals("")) {
            toastDialog.initDialog(getString(R.string.enterPartyDate), DesignBanquet.this);
            return false;
        }

        return true;
    } // function of getDataFromUser

    private void getBanquetComponent() {
        Intent intent = getIntent();
        if (intent != null) {
            banquetCompnent = intent.getStringExtra("component");

            if (banquetCompnent == null)

                banquetCompnent = "";

        }

        OrderObserver orderObserver = OrderObserver.getInstance();

        orderObserver.getOrder_data();

        if (!banquetCompnent.equals(""))
            btn_view_order.setText(getString(R.string.view));


        else {

            if (orderObserver.getOrder_data().equals(""))
                btn_view_order.setText(getString(R.string.Add_order_design_banquet));
            else
                btn_view_order.setText(getString(R.string.view));

//            ed_banq_partyName.setEnabled(false);
//            ed_banq_numberOfPersons.setEnabled(false);
//            ed_banq_customerComment.setEnabled(false);
//            relative_time.setOnClickListener(null);
//            relative_date.setOnClickListener(null);
//            linearLocation.setOnClickListener(null);
//            sp_banq_partyType.setEnabled(false);
        }

    } // function of getBanquetCompnent

    private void getSharedtLocation() {
        SharedPreferences prefs = getSharedPreferences("AddressPref", MODE_PRIVATE);
        String address = prefs.getString("address", "location");
        lat = prefs.getFloat("lat", 0);
        lng = prefs.getFloat("lng", 0);
        tv_party_location.setText(address);
    } // function of getCurrentLocation

    @OnClick(R.id.linearLocation)
    public void locationButton() {
        Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
        intent.putExtra("flag", "designBanquet");
        intent.putExtra("banquet_name", ed_banq_partyName.getText().toString());
        intent.putExtra("component", orderObserver.getOrder_data());

        getDataSaved();
        Log.i("QP", "date : " + banquetPartyDate);
        Log.i("QP", "component : " + banquetCompnent);

        orderObserver.setParty_date(banquetPartyDate);
        orderObserver.setOrder_data(banquetCompnent);
        orderObserver.setParty_date(banquetPartyDate);
        orderObserver.setParty_time(banquetPartyTime);
        orderObserver.setParty_name(banquetPartyName);
        orderObserver.setParty_type(banquetType);
        orderObserver.setPerson_number(banquetNumOfPersons);
        Log.i("QP", " set person number : " + banquetNumOfPersons);
        orderObserver.setCustomer_comments(banquetCustomerComment);
        startActivity(intent);
        //   finish();
    } // function of  locationButton

    @OnClick(R.id.btn_view_order)
    public void addNewOrder() {

        //  getObservableData();

        Intent addOrder = new Intent(this, AddOrderDesignBanquetActivity.class);
        addOrder.putExtra("component", banquetCompnent);

        getDataSaved();
        Log.i("QP", "date : " + banquetPartyDate);
        Log.i("QP", "order data : " + banquetCompnent);
        orderObserver.setOrder_data(banquetCompnent);
        orderObserver.setParty_date(banquetPartyDate);
        orderObserver.setParty_time(banquetPartyTime);
        orderObserver.setParty_name(banquetPartyName);
        orderObserver.setParty_type(banquetType);
        orderObserver.setPerson_number(banquetNumOfPersons);
        Log.i("QP", " set person number : " + banquetNumOfPersons);
        orderObserver.setCustomer_comments(banquetCustomerComment);
        startActivity(addOrder);
        // finish();
    } // intent to add new order activity


    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click

    @OnClick(R.id.party_time)
    public void openTimePicker() {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "time picker");
        orderObserver.setParty_time(tv_party_time.getText().toString());

    } // open time picker dialog

    @OnClick(R.id.date)
    public void openDatePicker() {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");
        orderObserver.setParty_date(banquetPartyDate);

    } // open date picker dialog

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        Calendar datetime = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);

        if (tv_party_date.getText().equals(""))
            toastDialog.initDialog(getString(R.string.pleaseAddDateFirst), this); // user doesn't choose date

        else {

            Calendar currentCalender = Calendar.getInstance();
            currentDate = DateFormat.getDateInstance((DateFormat.FULL)).format(currentCalender.getTime());

            Log.i("QN", currentDate + "\n" + choosenDate);
            if (currentDate.equals(choosenDate)) {
                if (datetime.getTimeInMillis() > c.getTimeInMillis()) {

                    Log.i("QN", "after");
                    tv_party_time.setText(hourOfDay + ":" + minute);
                    view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            it's after current
                } else {
                    Log.i("QN", "before");
                    toastDialog.initDialog(getString(R.string.chooseVaildTime), this);
                    tv_party_time.setText("");
//            it's before current'
                }
            } // choose today
            else {

                tv_party_time.setText(hourOfDay + ":" + minute);
                view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } // choose day after today

        } // user choose date
    } // set text with time

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        choosenDate = DateFormat.getDateInstance((DateFormat.FULL)).format(calendar.getTime());
        tv_party_date.setText(choosenDate);
        tv_party_time.setText("");

    } // set text with date

    public void SendBanquetButton(View view) {

        if (!getDataFromUser()) {
            return;
        } else {
            sendBanquetToApi();

        }
    } // function of sendBanquet Button

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void sendBanquetToApi() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final DesignBanquetApi banquetApi = retrofit.create(DesignBanquetApi.class);

                final Call<CheckResponse> getInterestConnection = banquetApi.designBanquet(toolBarConfig.getPrefUserId(), lat, lng
                        , banquetCompnent, banquetPartyName, banquetType, banquetPartyLocation, banquetNumOfPersons,
                        banquetPartyTime, banquetPartyDate,
                        banquetCustomerComment);


                getInterestConnection.enqueue(new Callback<CheckResponse>() {
                    @Override
                    public void onResponse(Call<CheckResponse> call, Response<CheckResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                progress.dismiss();

                                orderObserver.setOrder_data("");
                                orderObserver.setParty_name("");
                                orderObserver.setParty_type("");
                                orderObserver.setPerson_number("");
                                orderObserver.setParty_date("");
                                orderObserver.setParty_time("");
                                orderObserver.setCustomer_comments("");

                                Log.i("QP", " set person number : " + banquetNumOfPersons);

                                toastDialog.initDialog(getString(R.string.banquetAdded), DesignBanquet.this);

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                                        intent.putExtra("fragmentFlag","home");
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 2000);


                            } // banquet add successfully
                            else if (code.equals("1313")) {
                                progress.dismiss();
                                toastDialog.initDialog(getString(R.string.userHaveAlreadyBanquet), DesignBanquet.this);
                                // user have already banquet
                            } // user must finish your currently banquet to be able to design another banquet


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<CheckResponse> call, Throwable t) {
                        progress.dismiss();
                        toastDialog.initDialog(getString(R.string.retry), DesignBanquet.this);
                        Log.i("QP", "error : " + t.toString());

                    } // on Failure
                });

            }

        }.start();

    } // function of sendBanquetToApi

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
} // class of Design Banquet
