package com.invetechs.mamamiauserversion.View.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AdvertismentApi.AdvertisMentApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AdvertismentResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.AddAddressActivity;
import com.invetechs.mamamiauserversion.View.Activity.BanquetActivity;
import com.invetechs.mamamiauserversion.View.Activity.ChooseAddressActivity;
import com.invetechs.mamamiauserversion.View.Activity.GeneralSearchFilter;
import com.invetechs.mamamiauserversion.View.Activity.LoginActivity;
import com.invetechs.mamamiauserversion.View.Activity.OfferActivity;
import com.invetechs.mamamiauserversion.View.Activity.RestaurantActivity;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {

    // Bind views
    @BindView(R.id.searcView_home)
    TextView searchView;

    @BindView(R.id.rest)
    TextView rest;

    @BindView(R.id.house)
    TextView house;

    @BindView(R.id.dessert)
    TextView dessert;

    @BindView(R.id.banquet)
    TextView banquet;

    @BindView(R.id.bt_offers)
    Button bt_offers;

    @BindView(R.id.bt_currentAddress)
    Button bt_address;

    @BindView(R.id.bt_mapActivity)
    Button bt_mapActivity;


    // vars
    LocationManager locationManager;
    Location location; // location
    double latitude = 0; // latitude
    double longitude = 0; // longitude
    Context mContext;
    boolean enableGps = false;
    String address = "" , type = "Android";
    ConnectivityManager connectivityManager;
    SharedPreferences.Editor editor;
    SharedPreferences editorAddress;
    double prefLat, prefLng;
    SharedPreferences sharedPreferences;
    ShowDialog toastDialog = new ShowDialog();
    Handler handler;
    CustomDialogProgress progress;
    SliderLayout sliderShow;
    List<AdvertismentResponse.DataBean> imageList = new ArrayList<>();
    ToolBarConfig toolBarConfig;
    AlertDialog alert;
    TextSliderView textSliderView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
        mContext = getActivity();
        initLanguage();
        getLcationSaved(); // get user location if  choose address  from his addresses

        checkPermission();
        try {
            Log.i("QP","try requestLocationUpdates");

        } catch (Exception e) {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
        }

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Log.i("QP","try isLocationEnabled");
            isLocationEnabled();

        } catch (Exception e) {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
        }

        toastDialog = new ShowDialog();
        toolBarConfig = new ToolBarConfig(getActivity());

        getAdverisMent();

        initSliderImage(view);
        return view;

    } // onCreateView function

    private  void  initSliderImage()
    {
        textSliderView = new TextSliderView(getContext());
    } // function of initSliderImage

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            rest.setTypeface(font);
            house.setTypeface(font);
            dessert.setTypeface(font);
            banquet.setTypeface(font);
            bt_offers.setTypeface(font);
            bt_mapActivity.setTypeface(font);
            bt_address.setTypeface(font);
            searchView.setTypeface(font);

            Log.i("QP", "ar");

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            rest.setTypeface(font);
            house.setTypeface(font);
            dessert.setTypeface(font);
            banquet.setTypeface(font);
            bt_offers.setTypeface(font);
            bt_mapActivity.setTypeface(font);
            bt_address.setTypeface(font);
            searchView.setTypeface(font);
            Log.i("QP", "en");
        }

    }//initialize language

    private void getLcationSaved() {
        editorAddress = getContext().getSharedPreferences("AddressPref", MODE_PRIVATE);
        prefLat = editorAddress.getFloat("lat", 0);
        prefLng = editorAddress.getFloat("lng", 0);
    } // function of getLcationSaved

    @OnClick(R.id.searcView_home)
    public void searchButton() {
        if (checkInternetConnectionAndGPS() == true) {
            Intent intent = new Intent(getActivity(), GeneralSearchFilter.class);
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            startActivity(intent);
        }
    } // search button

    @OnClick(R.id.bt_offers)
    public void offerButton() {
        if (checkInternetConnectionAndGPS() == true) {
            Intent intent = new Intent(getActivity(), OfferActivity.class);
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            intent.putExtra("flag", "offer");
            startActivity(intent);
        }
    } // offer button

    @OnClick(R.id.restarantButton)
    public void restarantButton() {
        if (checkInternetConnectionAndGPS() == true) {
            intentToAllRestaurants("restaurant");
        }
    } // restarant button

    @OnClick(R.id.houseFoodButton)
    public void houseFoodButton() {

        if (checkInternetConnectionAndGPS() == true) {
            intentToAllRestaurants("housefood");
        }

    } // houseFood button

    @OnClick(R.id.dessertButton)
    public void dessertButton() {
        if (checkInternetConnectionAndGPS() == true) {
            intentToAllRestaurants("dessert");
        }
    } // dessert button

    @OnClick(R.id.banquetButton)
    public void banquetButton() {
        if (checkInternetConnectionAndGPS() == true) {
            Intent intent = new Intent(getActivity(), BanquetActivity.class);
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            intent.putExtra("flag", "banquet");
            startActivity(intent);
        }
    } // banquet button

    private void intentToAllRestaurants(String flag) {
        Intent intent = new Intent(getActivity(), RestaurantActivity.class);
        intent.putExtra("lat", latitude);
        intent.putExtra("lng", longitude);
        intent.putExtra("flag", flag);
        startActivity(intent);
    } // function of intentToAllRestaurants

    private boolean checkInternetConnectionAndGPS() {
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            if (enableGps == false || latitude == 0 || longitude == 0) {
                isLocationEnabled();
                return false;
            } else {
                return true;
            }

        } // check internet connection
        else {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
            return false;
        }
    } // function check internet connection and confirm gps open

    @OnClick(R.id.bt_mapActivity)
    public void saveLoctionButton() {

        if(toolBarConfig.getPrefUserId() != 0) {
            Intent intent = new Intent(getContext(), AddAddressActivity.class);
            intent.putExtra("id", 0);
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            intent.putExtra("note", "0");
            intent.putExtra("address", address);
            intent.putExtra("flag", "add");
            startActivity(intent);
        }
        else
        {
           Intent intent = new Intent(getContext(),LoginActivity.class);
           getContext().startActivity(intent);
        }
    } // sace location button

    @OnClick(R.id.bt_currentAddress)
    public void currentLocationButton() {

        if(toolBarConfig.getPrefUserId() != 0) {
            Intent intent = new Intent(getContext(), ChooseAddressActivity.class);
            intent.putExtra("fragmentFlag","home");
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(getContext(),LoginActivity.class);
            getContext().startActivity(intent);
        }
    } // current loaction button , display addresses saved before

    private void getLocation() {
        try {

            Log.i("QP", "locationManger : " + locationManager);

            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 10);
                Log.i("QP", "permission false");
                return;
            } else {
                // Write you code here if permission already given.
                Log.i("QP", "permission true");
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                //Log.i("QP","Location : "+location.getLatitude()+" : "+location.getLongitude());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if (prefLat != 0 && prefLng != 0) {
                    latitude = prefLat;
                    longitude = prefLng;
                }
                getAddressFromLatandLng(latitude, longitude);

            } // location not equal null
            else
                {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    if (prefLat != 0 && prefLng != 0) {
                        latitude = prefLat;
                        longitude = prefLng;
                    }
                    Log.i("QP", "Location : " + latitude + " : " + longitude);
                    getAddressFromLatandLng(latitude, longitude);

                } // location not equal null
                else {
                    Log.i("QP", "Location null on network and gps");
                    getCurrentLocation();
                }
            }
        } catch (SecurityException e) {
            Log.i("QP", "locationManger :exception " + e.toString());
        }
    } // function get location

    private void getCurrentLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            checkPermission();
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }

        if (bestLocation != null) {
            latitude = bestLocation.getLatitude();
            longitude = bestLocation.getLongitude();
            if (prefLat != 0 && prefLng != 0) {
                latitude = prefLat;
                longitude = prefLng;
            }
            getAddressFromLatandLng(latitude, longitude);
            Log.i("QP", "current Location : " + bestLocation);
        } // bestlocation not equal null
        else {
            Log.i("QP", "current Location : Null"); //

        } // location equal null
    } // function of getCurrentLocation

    @Override
    public void onResume() {
        super.onResume();
        Log.i("QP", "onResume");
//      if(enableGps == false)
//      {
//          isLocationEnabled();
//
//      }
    } // onResume fragment


    private void getAddressFromLatandLng(double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned
            address = addresses.get(0).getAddressLine(0);
            editor = getContext().getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
            editor.putString("address", address);
            editor.putFloat("lat", (float) lat);
            editor.putFloat("lng", (float) lng);
            editor.apply();
            bt_address.setText(address);
        } catch (Exception e) {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
            return;
        }

      /*  String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
*/


    } // function of getAddressFromLatandLng

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            //locationManager.removeUpdates(locationListenerGPS);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }; // locationlistner

    private void isLocationEnabled() {

        Log.i("QP", "isLocationEnabled");
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.enableLocation));
            alertDialog.setMessage(getString(R.string.settingLocationMenu));

            alertDialog.setPositiveButton(getString(R.string.locationSetting), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            }); // positive button

            alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            }); // negative button
            alert = alertDialog.create();
            alert.show();
        }
        else {
          /*  AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.confirmLocation));
            alertDialog.setMessage(getString(R.string.yourLocationEnabledContinue));
            alertDialog.setNegativeButton(getString(R.string.conttinue), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {*/
                    getLocation();
//                    dialog.cancel();
                    enableGps = true;
            /*    }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();*/
        }

    }// function check permission

    private void checkPermission() {
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("QP","checkPermission");
            return;
        }
    } //checkPermission


    public  void  imageSlider (List<AdvertismentResponse.DataBean>  urls)
    {

         sliderShow = getActivity().findViewById(R.id.slider);

        Log.i("QP"," images url : "+urls.size());

        for(int i = 0 ; i< urls.size() ; i++) {
            textSliderView = new TextSliderView (getContext());
            textSliderView
                    .image("https://mammamiaa.com/cpanel/upload/advertisement/"+urls.get(i).getImage());
            sliderShow.addSlider(textSliderView);
            Log.e("QP","Image : "+urls.get(i).getImage());
        }

    } // imageSlider function

    public static String getMacAddr() {
        try
        {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    // res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";

} // function to get mac address of the mobile phone

    private void getAdverisMent()
    {

        if(imageList.size() > 0)imageList.clear();

        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AdvertisMentApi advertisMentApi = retrofit.create(AdvertisMentApi.class);

                try {
                    WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(getContext().WIFI_SERVICE);
                    WifiInfo wInfo = wifiManager.getConnectionInfo();
                }
                catch (Exception e)
                {

                }

                Call<AdvertismentResponse> getConnection = advertisMentApi.getAds(toolBarConfig.getPrefUserId() , getMacAddr() , type);

                Log.i("QP", "mac address =  : " + getMacAddr() );
                Log.i("QP", "id =  : " + toolBarConfig.getPrefUserId() );
                Log.i("QP", "type =  : " + type);

                getConnection.enqueue(new Callback<AdvertismentResponse>() {
                    @Override
                    public void onResponse(Call<AdvertismentResponse> call, Response<AdvertismentResponse> response) {
                        try {

                            String code = response.body().getCode();

                            response.body();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {

                                    imageList = response.body().getData();
                                    imageSlider(imageList);

                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdvertismentResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });


            }

        }.start();
    } // function of getAdverisMent

    private  void  initSliderImage(View view)
    {
        textSliderView = new TextSliderView(getContext());
        sliderShow = view.findViewById(R.id.slider);
    } // function of initSliderImage

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListenerGPS);
        }
       Thread.interrupted();
        sliderShow.removeAllSliders();
        sliderShow.stopAutoCycle();
    }



} // class of HomeFragment
