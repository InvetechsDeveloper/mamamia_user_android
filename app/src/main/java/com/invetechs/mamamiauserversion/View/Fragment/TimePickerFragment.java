package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import com.invetechs.mamamiauserversion.R;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {

    // vars
    int hour;
    int minute;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog =new TimePickerDialog(getActivity()  ,  R.style.DialogTheme ,
                (TimePickerDialog.OnTimeSetListener) getActivity() , hour , minute , android.text.format.DateFormat.is24HourFormat(getActivity()));
        return timePickerDialog;
        }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }
}
