package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.invetechs.mamamiauserversion.R;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    // vars
    int years;
    int month;
    int day;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        years = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext()  ,  R.style.DialogTheme , (DatePickerDialog.OnDateSetListener) getActivity(),years,month,day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return datePickerDialog;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }
}
