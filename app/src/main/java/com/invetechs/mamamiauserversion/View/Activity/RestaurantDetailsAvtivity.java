package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.MenuAdapter;
import com.invetechs.mamamiauserversion.Control.RulesAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.RestaurantApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.RulesApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.KitchenRulesResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RestaurantDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.menu.BottomNavigationViewHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RestaurantDetailsAvtivity extends Language {

    // bind views

    @BindView(R.id.viewAll)
    View viewAll;

    @BindView(R.id.viewBreakfast)
    View viewBreakfast;

    @BindView(R.id.viewLaunch)
    View viewLaunch;

    @BindView(R.id.viewDinner)
    View viewDinner;

    @BindView(R.id.tapMeal)
    LinearLayout tapMeal;

    @BindView(R.id.tabpPoints)
    LinearLayout tabpPoints;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.restaurant_menu)
    RecyclerView recyclerView;

    RulesAdapter adapterRules;

    @BindView(R.id.searcView_menu)
    RelativeLayout searcView_menu;

    @BindView(R.id.txt_restaurantName)
    TextView txt_restaurantName;

    @BindView(R.id.txt_restaaurantTotalPeopleRate)
    TextView txt_restaaurantTotalPeopleRate;

    @BindView(R.id.txt_restaurantDistance)
    TextView txt_restaurantDistance;

    @BindView(R.id.txt_restaurantRate)
    TextView txt_restaurantRate;

    @BindView(R.id.rateBar_restaurant)
    AppCompatRatingBar rateBar_restaurant;

    @BindView(R.id.searchMenu)
    SearchView searchViewMenu;

    @BindView(R.id.relative_slider)
    RelativeLayout relative_slider;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbar_layout;

    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences preferences;
    String restaurantName ="";
    int restaurantId =0 , totalPeopleRate=0 , kitchenId = 0 ,deliveryMaxBill = 0;
    double totalDelivery = 0;
    double rate , distanceWithKilo , latitude , longitude , distanceWithMeter;
    List<RestaurantDetailsResponse.DataBean.MealBean> menuListApi = new ArrayList<>() , menuList = new ArrayList<>() ;
    List<RestaurantDetailsResponse.DataBean.ImagesBean> imageList = new ArrayList<>();
    int counterOfCallingImageSlider = 0;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    MenuAdapter adapter; // fill this menu with  dessert , offers , meals , beverge
    List<KitchenRulesResponse.DataBean> ruleListApi = new ArrayList<>();
    List<KitchenRulesResponse.DataBean> ruleList = new ArrayList<>();
    String type = "restaurant";
    private static int firstVisibleInListview;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);
        ButterKnife.bind(this);
        initLanguage();
        getRestaurantId();
        if(type.equals("dessert")) {
            getMenuFromApi(0, 2, "0"); // get desserts

            tapMeal.setVisibility(View.GONE);
        }
        else {
            tapMeal.setVisibility(View.VISIBLE);
            getMenuFromApi(0, 3, "0"); // get all meals default
        }

        initMenu(); // menu list
        bottomNavigation();
        setToolBarConfig();
        toastDialog = new ShowDialog();

        searchMenu();
        hideKeyboard();

        AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

    } // onCreate function



    public void turnOffToolbarScrolling() {

        //turn off scrolling
        AppBarLayout.LayoutParams toolbarLayoutParams = (AppBarLayout.LayoutParams) app_bar.getLayoutParams();
        toolbarLayoutParams.setScrollFlags(0);

        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) app_bar.getLayoutParams();
        appBarLayoutParams.setBehavior(null);
        app_bar.setLayoutParams(appBarLayoutParams);
    }

    public void turnOnToolbarScrolling() {

        //turn on scrolling
        AppBarLayout.LayoutParams toolbarLayoutParams = (AppBarLayout.LayoutParams) app_bar.getLayoutParams();
        toolbarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) app_bar.getLayoutParams();
        appBarLayoutParams.setBehavior(new AppBarLayout.Behavior());
        app_bar.setLayoutParams(appBarLayoutParams);
    }



    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click


    private void searchMenu() {
        searchViewMenu.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getMenuFromApi(0,0,query);
                searchViewMenu.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //getMenuFromApi(0,0,newText);
                return false;
            }
        });
    } // function of searchOnRestaurants

    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this,"none");
        toolBarConfig.setTitle(getString(R.string.Meals));
        toolBarConfig.setBack(this);
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void getRestaurantId()
    {
        Intent intent = getIntent();
        if(intent != null)
        {
            restaurantId = intent.getIntExtra("id",0);
            deliveryMaxBill =Integer.parseInt(intent.getStringExtra("maxbill"));
            totalDelivery =intent.getDoubleExtra("totalDelivery",0);
            distanceWithKilo =intent.getDoubleExtra("distanceWithKilo",0);
            latitude = intent.getDoubleExtra("lat",0);
            longitude = intent.getDoubleExtra("lng",0);
            type = intent.getStringExtra("type");
        }

    } // function of getRestaurantId

    @OnClick(R.id.review)
    public void makeReview() {

        if(toolBarConfig.getPrefUserId() == 0)
        {
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
            finish();
        } // user not login
        else
        {
            Intent reviewIntent = new Intent(this, ReviewActivity.class);
            reviewIntent.putExtra("id", kitchenId);
            startActivity(reviewIntent);
        } // user already login

    } // review function

    private void initMenu()
    {
        Log.i("QW","Restaurant Details : "+totalDelivery);
        adapter = new MenuAdapter(this,menuList,RestaurantDetailsAvtivity.this,restaurantId,deliveryMaxBill,totalDelivery,distanceWithKilo);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    } // function of init menu


    public void AllButton(View view) {
        view.getId();
        viewAll.setVisibility(View.VISIBLE); // choose all
        viewBreakfast.setVisibility(View.INVISIBLE);
        viewLaunch.setVisibility(View.INVISIBLE);
        viewDinner.setVisibility(View.INVISIBLE);
        getMenuFromApi(0, 3, "0");
    } // all meals

    public void BreakfastButton(View view) {

        viewAll.setVisibility(View.INVISIBLE);
        viewBreakfast.setVisibility(View.VISIBLE); //choose breakfast
        viewLaunch.setVisibility(View.INVISIBLE);
        viewDinner.setVisibility(View.INVISIBLE);
        getMenuFromApi(1, 3, "0");

    } // breakfast meals

    public void LaunchButton(View view) {
        viewAll.setVisibility(View.INVISIBLE);
        viewBreakfast.setVisibility(View.INVISIBLE);
        viewLaunch.setVisibility(View.VISIBLE); // choose launch
        viewDinner.setVisibility(View.INVISIBLE);
        getMenuFromApi(2, 3, "0");
    } // launch button

    public void DinnerButton(View view) {

        viewAll.setVisibility(View.INVISIBLE);
        viewBreakfast.setVisibility(View.INVISIBLE);
        viewLaunch.setVisibility(View.INVISIBLE);
        viewDinner.setVisibility(View.VISIBLE); // choose dinner
        getMenuFromApi(3, 3, "0");
    } // dinner button

    private void bottomNavigation() {

        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);

        if(type.equals("dessert"))
            bottomNavigationView.setSelectedItemId(R.id.menuDessert);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuDessert:

                                app_bar.setExpanded(true);
                                if (ruleList.size() > 0) {
                                    ruleList.clear();
                                    adapterRules.notifyDataSetChanged();
                                } // clear array of points

                                tapMeal.setVisibility(View.GONE); // tap contain brakFast , launch , dinner
                                tabpPoints.setVisibility(View.GONE);// tap points
                                searcView_menu.setVisibility(View.VISIBLE); // search
                                //tool_bar_rules.setVisibility(View.GONE);// toolbar contain rules text
                                toolBarConfig.setTitle(getString(R.string.Desserts));

                                initMenu(); // menu list
                                getMenuFromApi(0, 2, "0");
                                Log.i("QP", "dessert");
                                break;
                            case R.id.menuMeal:
                                app_bar.setExpanded(true);
                                tapMeal.setVisibility(View.VISIBLE);
                                tabpPoints.setVisibility(View.GONE);
                                searcView_menu.setVisibility(View.VISIBLE);
                                //tool_bar_rules.setVisibility(View.GONE);
                                toolBarConfig.setTitle(getString(R.string.Meals));

                                if (ruleList.size() > 0) {
                                    ruleList.clear();
                                    adapterRules.notifyDataSetChanged();
                                }
                                initMenu(); // menu list
                                getMenuFromApi(0, 3, "0");
                                break;

                            case R.id.menuOffer:
                                app_bar.setExpanded(true);
                                tapMeal.setVisibility(View.GONE);
                                tabpPoints.setVisibility(View.GONE);
                                searcView_menu.setVisibility(View.VISIBLE);
                                //tool_bar_rules.setVisibility(View.GONE);
                                toolBarConfig.setTitle(getString(R.string.Offers));

                                if (ruleList.size() > 0) {
                                    ruleList.clear();
                                    adapterRules.notifyDataSetChanged();
                                }

                                initMenu(); // menu list
                                getMenuFromApi(0, 5, "0");
                                break;
                            case R.id.menuBeverge:
                                app_bar.setExpanded(true);
                                tapMeal.setVisibility(View.GONE);
                                tabpPoints.setVisibility(View.GONE);
                                searcView_menu.setVisibility(View.VISIBLE);
                                //tool_bar_rules.setVisibility(View.GONE);
                                toolBarConfig.setTitle(getString(R.string.beverges));

                                if (ruleList.size() > 0) {
                                    ruleList.clear();
                                    adapterRules.notifyDataSetChanged();
                                }

                                initMenu(); // menu list
                                getMenuFromApi(0, 4, "0");
                                break;
                            case R.id.menuPoinst:
                                app_bar.setExpanded(true);
                                tabpPoints.setVisibility(View.VISIBLE);
                                tapMeal.setVisibility(View.GONE);
                                searcView_menu.setVisibility(View.GONE);
                                //tool_bar_rules.setVisibility(View.VISIBLE);
                                toolBarConfig.setTitle(getString(R.string.Points));

                                initRecyclerView();
                                getRulesFromApi();

                                if (menuList.size() > 0) {
                                    menuList.clear();
                                    adapter.notifyDataSetChanged();
                                }

                                break;
                        }
                        return true;
                    }
                });
    } // function of bottomNavigation

    private void initRecyclerView() {

        adapterRules = new RulesAdapter(this, ruleList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterRules);

    } //initialize recycler view points

    private void fillRuleList() {
        app_bar.setExpanded(true);
        if (ruleListApi.size() <= 5)
        {
            disableScroll();
        }
        else
        {
            enableScroll();
        }
        if(ruleList.size() > 0) ruleList.clear();

        for (int i = 0; i < ruleListApi.size(); i++) {
            ruleList.add(ruleListApi.get(i));
        }

        adapterRules.notifyDataSetChanged();
        if (ruleList.size() <= 0) {
            toastDialog.initDialog(getString(R.string.notFoundRule), RestaurantDetailsAvtivity.this);
            tabpPoints.setVisibility(View.INVISIBLE);
        } // not found rules for this kitchen
    } // function of fillRuleList

    private void getRulesFromApi() {

        progress = new CustomDialogProgress();
        progress.init(this);


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final RulesApi rulesApi = retrofit.create(RulesApi.class);

                Call<KitchenRulesResponse> getConnection = rulesApi.getRules(kitchenId);

                getConnection.enqueue(new Callback<KitchenRulesResponse>() {
                    @Override
                    public void onResponse(Call<KitchenRulesResponse> call, Response<KitchenRulesResponse> response) {
                        try {

                            String code = response.body().getCode();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {
                                ruleListApi = response.body().getData();
                                fillRuleList();
                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<KitchenRulesResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }

        }.start();
    } // function of getRulesFromApi

    private void getMenuFromApi(final int timeCategory, final int typeCategory, final String textSearch) // timeCategory mean launch , dinner ,..  , typeCategory mean meal , dessert ,..
    {
        Log.i("QP", "  restaurantId : "+restaurantId);
        if(menuList.size() > 0 && menuListApi.size() >0)
        {
            menuListApi.clear();
            menuList.clear();

        }
        if(imageList.size() > 0)imageList.clear();

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final RestaurantApi restaurantApiApi = retrofit.create(RestaurantApi.class);

                Call<RestaurantDetailsResponse> getConnection = restaurantApiApi.getRestaurantsDetailss(restaurantId, timeCategory, typeCategory, textSearch);

                getConnection.enqueue(new Callback<RestaurantDetailsResponse>() {
                    @Override
                    public void onResponse(Call<RestaurantDetailsResponse> call, Response<RestaurantDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();
                            response.body();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {
                                menuListApi = response.body().getData().getMeal();
                                kitchenId = response.body().getData().getKitchen().get(0).getId();

                                if (response.body().getData().getKitchen().get(0).getCount_rate() != 0)
                                    rate = response.body().getData().getKitchen().get(0).getTotal_rate() /
                                            response.body().getData().getKitchen().get(0).getCount_rate();
                                else rate = 0;


                                kitchenId = response.body().getData().getKitchen().get(0).getId();

                                if(  response.body().getData().getKitchen().get(0).getCount_rate() != 0)
                                    rate = response.body().getData().getKitchen().get(0).getTotal_rate() /
                                            response.body().getData().getKitchen().get(0).getCount_rate();
                                else rate = 0;

                                preferences = getSharedPreferences("user", MODE_PRIVATE);

                                if (preferences.getString("language", "ar").equals("ar")) ;
                                restaurantName = response.body().getData().getKitchen().get(0).getAr_title();
                                if (preferences.getString("language", "ar").equals("en"))
                                    restaurantName = response.body().getData().getKitchen().get(0).getEn_title();


                                totalPeopleRate = response.body().getData().getKitchen().get(0).getCount_rate();


                                distanceWithMeter = distance(response.body().getData().getKitchen().get(0).getLat()
                                        , response.body().getData().getKitchen().get(0).getLng(), latitude, longitude);

                                imageList = response.body().getData().getImages();

                                Log.i("QP", " images List : " + imageList.size());
                                if (counterOfCallingImageSlider == 0)
                                    imageSlider(imageList);


                                fillMenuList();
                                setRestaurantData();
                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<RestaurantDetailsResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });


            }

        }.start();
    } // function of getMenuFromApi

    public void enableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                toolbar_layout.getLayoutParams();
        params.setScrollFlags(
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        );
        toolbar_layout.setLayoutParams(params);
    }

    public void disableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                toolbar_layout.getLayoutParams();
        params.setScrollFlags(0);
        toolbar_layout.setLayoutParams(params);
    }

    private void fillMenuList() {

        app_bar.setExpanded(true);
        if (menuListApi.size() <= 1)
        {
          disableScroll();
        }
        else
        {
            enableScroll();
        }

        for (int i = 0; i < menuListApi.size(); i++) {
            menuList.add(menuListApi.get(i));
        }
        adapter.notifyDataSetChanged();

        if(menuList.size() <= 0)
        {
            toastDialog.initDialog(getString(R.string.notFound),RestaurantDetailsAvtivity.this);

        } // not found meals
    } // function of fillMenuList

    private void setRestaurantData() {

        int precision1 = 10; //keep 1 digits
        rate = Math.floor(rate * precision1 + .5) / precision1;

        distanceWithKilo = distanceWithMeter / 1000;
        int precision = 10; //keep 1 digits
        distanceWithKilo = Math.floor(distanceWithKilo * precision + .5) / precision;

        txt_restaurantName.setText(restaurantName);
        txt_restaaurantTotalPeopleRate.setText(" (" + totalPeopleRate + ") ");
        txt_restaurantDistance.setText(distanceWithKilo + " " + getResources().getString(R.string.km));
        txt_restaurantRate.setText(rate + "");
        rateBar_restaurant.setRating((float) rate);


    } // function of setRestaurantData


    public float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    } // distance between restaurant and user

    public void imageSlider(List<RestaurantDetailsResponse.DataBean.ImagesBean> urls) {

        SliderLayout sliderShow = findViewById(R.id.slider);

        Log.i("QP", " images url : " + urls.size());

        for (int i = 0; i < urls.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image("https://mammamiaa.com/cpanel/upload/kitchen/" + urls.get(i).getImage());
            sliderShow.addSlider(textSliderView);
            Log.e("QP", "Image : " + urls.get(i).getImage());
        }
        counterOfCallingImageSlider = 1;

    } // imageSlider function


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }


}
