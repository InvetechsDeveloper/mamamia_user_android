package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class LanguageFragment extends Fragment {

    // bind views
    @BindView(R.id.radio_arabic)
    RadioButton radio_arabic;

    @BindView(R.id.radio_group)
    RadioGroup radio_group;

    @BindView(R.id.radio_englis)
    RadioButton radio_englis;

    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    //vars
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Boolean isArabic = true, isEnglish = true;
    ShowDialog toastDialog = new ShowDialog();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.language_fragment, container, false);
        ButterKnife.bind(this, view);
        checkRadioButtons();
        initLanguage();
        return view;
    }

    @OnClick(R.id.btn_confirm)
    public void setLanguage() {

        if (radio_group.getCheckedRadioButtonId() == -1)
        {
            toastDialog.initDialog(getString(R.string.select_language),getContext());
            return ;
        }

        if (isArabic == true) {
            preferences.edit().putString("language", "ar").apply();
            Intent arabic = new Intent(getContext(), MenuActivity.class);
            arabic.putExtra("fragmentFlag","home");
            arabic.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            arabic.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(arabic);

        } else if (isEnglish == true) {
            preferences.edit().putString("language", "en").apply();
            Intent english = new Intent(getContext(), MenuActivity.class);
            english.putExtra("fragmentFlag","home");
            english.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            english.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(english);
        }

    } // dont change language

    private void checkRadioButtons() {
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radio_arabic:
                        isArabic = true;
                        isEnglish = false;

                        break;

                    case R.id.radio_englis:
                        isEnglish = true;
                        isArabic = false;
                        break;

                }
            }
        });

    }

    private void initLanguage() {

        preferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = preferences.edit();

        if (preferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            radio_englis.setTypeface(font);
            radio_arabic.setTypeface(font);
            btnConfirm.setTypeface(font);
        } else if (preferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            radio_englis.setTypeface(font);
            radio_arabic.setTypeface(font);
            btnConfirm.setTypeface(font);
        }

    }//initialize language

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }
}
