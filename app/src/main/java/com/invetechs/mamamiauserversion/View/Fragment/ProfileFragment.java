package com.invetechs.mamamiauserversion.View.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.ProfileApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.UpdateProfileApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.AddAddressActivity;
import com.invetechs.mamamiauserversion.View.Activity.DesignBanquet;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {

    // Bind views

    @BindView(R.id.textinput_name)
    TextInputLayout textinputName;

    @BindView(R.id.img_profile)
    ImageView img_profile;

    @BindView(R.id.et_name)
    TextInputEditText et_name;

    @BindView(R.id.textinput_phone)
    TextInputLayout textinputPhone;

    @BindView(R.id.et_phone_number)
    TextInputEditText et_phone_number;

    @BindView(R.id.textinput_email)
    TextInputLayout textinputEmail;

    @BindView(R.id.et_email)
    TextInputEditText et_email;

    @BindView(R.id.textinput_password)
    TextInputLayout textinput_password;

    @BindView(R.id.et_password)
    TextInputEditText et_password;

    @BindView(R.id.textinput_addresses)
    TextInputLayout textinput_addresses;

    @BindView(R.id.et_addresses)
    TextInputEditText et_addresses;

    @BindView(R.id.btn_edit)
    Button btnEdit;

    //  vars

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int id = 0;
    String password = "";
    String name = "";
    String email = "";
    String mobileNumber = "";
    String newPassword = "";
    String imagePath = "";
    File file;
    LoginResponse.UserdataBean user;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    public static final int GET_FROM_GALLERY = 10;
    private static final int REQUEST_CAMERA = 1888;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        initLanguage();
        img_profile.setClickable(false);
        getUserID_Password();
        getDataFromApi();
        return view;
    }

    @OnClick(R.id.et_addresses)
    public void clickAddresses() {
        Log.i("QP", "addresses clicked");
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddressesFragment()).commit();
    } // function of addresses button

    private void setViewData() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.pizza_back);
        requestOptions.error(R.drawable.pizza_back);

        if (!user.getImage().equals(null))
            Glide.with(getContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load("http://mammamiaa.com/cpanel/upload/user/" + user.getImage())
                    .into(img_profile);

        et_name.setText(user.getName());
        et_phone_number.setText(user.getMobilenumber());
        et_email.setText(user.getEmail());
        et_password.setText(password);

    } // set view data from api

    private void getUserID_Password() {

        SharedPreferences prefs = getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        id = prefs.getInt("id", 0);
        password = prefs.getString("password", "");

        Log.i("QP", "profile id" + id);
        Log.i("QP", "profile password" + password);

    } // function get the id of user to send it to api

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            textinputName.setTypeface(font);
            et_name.setTypeface(font);
            textinputPhone.setTypeface(font);
            et_phone_number.setTypeface(font);
            textinputEmail.setTypeface(font);
            textinput_password.setTypeface(font);
            et_password.setTypeface(font);
            textinput_addresses.setTypeface(font);
            et_addresses.setTypeface(font);
            btnEdit.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            textinputName.setTypeface(font);
            et_name.setTypeface(font);
            textinputPhone.setTypeface(font);
            et_phone_number.setTypeface(font);
            textinputEmail.setTypeface(font);
            textinput_password.setTypeface(font);
            et_password.setTypeface(font);
            textinput_addresses.setTypeface(font);
            et_addresses.setTypeface(font);
            btnEdit.setTypeface(font);
        }


    }//initialize language

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getDataFromApi() {

        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ProfileApi profileApi = retrofit.create(ProfileApi.class);

                final Call<LoginResponse> getInterestConnection = profileApi.show_profile(id);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();
                            response.body();
                            Log.i("QP", "profile code" + code);

                            if (code.equals("200")) {
                                user = response.body().getUserdata();
                                setViewData();

                            } // get user data successfully

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();

    } // show profile data

    @OnClick(R.id.img_profile)
    public void changeProfileImage() {
        int PERMISSION_REQUEST_CODE = 1;
        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            // ask user for permission to access storage // allow - deni
        }

        startActivityForResult(new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                , GET_FROM_GALLERY);

    } // click imageView

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }// function of getRealPathFromURI

    public void onSelectFromGalleryResult(Intent data) {
        if (data != null) {

            Uri imageUri = data.getData();
            img_profile.setImageURI(imageUri);
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                imagePath = getRealPathFromURI(imageUri);
            }

            // upload image
        }
    } // onSelectFromGalleryResult

    public void onCaptureImageResult(Intent data) {
        if (data != null) {
            Uri imageUri = data.getData();
            img_profile.setImageURI(imageUri);
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.ACTION_IMAGE_CAPTURE};
            Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                imagePath = getRealPathFromURI(imageUri);
            }
            Log.i("QP", imagePath);
        }
    } // onCaptureImageResult

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GET_FROM_GALLERY) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    } // on activity result fucntion

    @OnClick(R.id.btn_edit)
    public void editProfile() {
        et_name.setEnabled(true);
        et_phone_number.setEnabled(true);
        et_email.setEnabled(true);
        et_password.setEnabled(true);
        img_profile.setClickable(true);
        textinput_addresses.setVisibility(View.GONE);


        if (!ValidatePassword() | !ValidateUserName() | !ValidateMobile()) {
            return;
        } else {
            name = et_name.getText().toString();
            email = et_email.getText().toString();
            mobileNumber = et_phone_number.getText().toString().trim();
            newPassword = et_password.getText().toString().trim();
            if (!email.equals("")) {
                if (!ValidateEmail()) {
                    return;
                }
            }

            if (btnEdit.getText().toString().equals(getString(R.string.btn_save))) {
                updateProfileData();
                et_name.setEnabled(false);
                et_phone_number.setEnabled(false);
                et_email.setEnabled(false);
                et_password.setEnabled(false);
                img_profile.setClickable(false);
                textinput_addresses.setVisibility(View.VISIBLE);
                btnEdit.setText(getString(R.string.edit));


            } else {
                btnEdit.setText(getString(R.string.btn_save));
            }

        }

    } // click on edit profile

    public void updateProfileData() {

        progress = new CustomDialogProgress();
        progress.init(getContext());


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
                Log.i("QP", "file : " + imagePath);

                if (!imagePath.equals("")) {
                    file = new File(imagePath);
                    Log.i("QP", "file with image : " + imagePath);

                    // RequestBody userIdRequest = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userID));
                    RequestBody requestBody = RequestBody.create(MediaType.parse("image"), file);

                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                    Retrofit retrofit = RetrofitConnection.ConnectWith();

                    RequestBody namePart = RequestBody.create(MediaType.parse("text/plain"), name);
                    RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(id));
                    RequestBody mobilePart = RequestBody.create(MediaType.parse("text/plain"), mobileNumber);
                    RequestBody passwordPart = RequestBody.create(MediaType.parse("text/plain"), password);
                    RequestBody emailPart = RequestBody.create(MediaType.parse("text/plain"), email);

                    UpdateProfileApi updateProfile = retrofit.create(UpdateProfileApi.class);

                    Call<LoginResponse> getConnection = updateProfile.update(fileToUpload, idPart, mobilePart,
                            namePart, emailPart,
                            passwordPart);

                    getConnection.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            try {

                                //  Log.i("QP", "exception : " + response.body());
                                String code = response.body().getCode();
                                Log.i("QP", "code : " + code);
                                //  Log.i("QP", "response : " + response.body());

                                if (code.equals("200")) {
                                    LoginResponse.UserdataBean user = response.body().getUserdata();
                                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("name", user.getName());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("password", newPassword);
                                    editor.putString("mobilenumber", user.getMobilenumber());
                                    editor.putString("profile_image", user.getImage());
                                    editor.apply();
                                    toastDialog.initDialog(getString(R.string.profile_updated), getActivity());
                                    progress.dismiss();

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            Intent intent = new Intent(getActivity(), MenuActivity.class);
                                            intent.putExtra("fragmentFlag", "home");
                                            getActivity().startActivity(intent);

                                        }
                                    }, 2000);


                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.unique), getActivity());
                                    progress.dismiss();

                                }
                                progress.dismiss();
                            } catch (Exception e) {

                                Log.i("QP", "exception with image : " + e.toString());
                                progress.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {

                            Log.i("QP", "error : " + t.toString());
                            toastDialog.initDialog(getString(R.string.retry), getActivity());
                            progress.dismiss();
                        }
                    });
                } // if file not equal null

                else {

                    Log.i("QP", "file without image : " + imagePath);
                    RetrofitConnection connection = new RetrofitConnection(getActivity());
                    Retrofit retrofit = RetrofitConnection.ConnectWith();
                    UpdateProfileApi updateProfile = retrofit.create(UpdateProfileApi.class);

                    Call<LoginResponse> getConnection = updateProfile.updateWithoutImage(id, mobileNumber, name, email, newPassword);

                    Log.i("QP", "file : " + "\nid : " + id
                            + "\nmobile : " + mobileNumber
                            + "\nname : " + name
                            + "\nemail : " + email
                            + "\nnewPassword : " + newPassword);


                    getConnection.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            try {
                                Log.i("QP", "responce : " + response.body());
                                String code = response.body().getCode();

                                response.body();
                                Log.i("QP", "code : " + code);
                                Log.i("QP", "response : " + response.body());

                                if (code.equals("200")) {
                                    LoginResponse.UserdataBean user = response.body().getUserdata();
                                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("name", user.getName());
                                    editor.putString("image", user.getImage());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("password", newPassword);
                                    editor.putString("mobilenumber", user.getMobilenumber());
                                    editor.apply();
                                    toastDialog.initDialog(getString(R.string.profile_updated), getActivity());
                                    progress.dismiss();

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            Intent intent = new Intent(getActivity(), MenuActivity.class);
                                            intent.putExtra("fragmentFlag", "home");
                                            getActivity().startActivity(intent);

                                        }
                                    }, 2000);

                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.unique), getActivity());
                                    progress.dismiss();

                                }
                            } catch (Exception e) {

                                Log.i("QP", "exception : " + e.toString());
                                progress.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {

                            Log.i("QP", "error : " + t.toString());
                            toastDialog.initDialog(getString(R.string.retry), getActivity());
                            progress.dismiss();
                        }
                    });
                } // else file equal null
            }
        }.start();
    } // updateProfileData

    private boolean ValidateEmail() {
        String email = et_email.getText().toString().trim();

        if (email.isEmpty()) {
            et_email.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError(getString(R.string.error_invalid_email));
            return false;
        }
        return true;

    } // validation on email

    private boolean ValidateMobile() {

        String Mobile = et_phone_number.getText().toString().trim();

        if (Mobile.length() < 10 || Mobile.length() > 10) {
            et_phone_number.setError(getString(R.string.error_valid_mobile));
        }
        if (Mobile.length() == 10) {
            et_phone_number.setError(null);
        }

        if (Mobile.length() == 0) {
            et_phone_number.setError(getString(R.string.field_cant_empty));
        }

        return true;
    } // validation on mobile

    public boolean ValidatePassword() {

        password = et_password.getText().toString().trim();

        if (password.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (password.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;

        }

        return true;


    } // validation on password

    private boolean ValidateUserName() {

        name = et_name.getText().toString().trim();

        if (name.isEmpty()) {
            et_name.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (et_name.length() < 5) {
            et_name.setError(getString(R.string.enter_full_name));
            return false;
        } else {

            return true;
        }
    } // validation on username

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    } // on destroy


} // class of profileFragment
