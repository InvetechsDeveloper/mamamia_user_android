package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.FirebaseApp;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.NotificationstAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Notifications.NotificationApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.InvoiceResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.NotificationResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.pushnotifications.PushNotifications;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class NotificationsActivity extends Language {

    // Bind views
    @BindView(R.id.notification_recycler_view)
    RecyclerView notification_recycler_view;


    // vars
    ImageView image_notification;
    List<NotificationResponse.DataBean> arrayList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    NotificationstAdapter adapter;
    SharedPreferences sharedPreferences;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int user_id = 0;
    ToolBarConfig toolBarConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        setToolBarConfig();
        initLanguage();
        getNotifications();
        getUserId();
        image_notification = findViewById(R.id.img_notifications);
        image_notification.setOnClickListener(null);

    }


    private void setToolBarConfig() {

        toolBarConfig = new ToolBarConfig(this, "notification", this);
        toolBarConfig.setTitle(getString(R.string.notifications));
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();
        Log.i("QW", "notification activity notification");

    } // function of setToolBarConfig

    private void getUserId() {
        prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        user_id = prefs.getInt("id", 0);
        Log.i("QP", " invoice userId : " + user_id);

    } // getUserId function

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }

    } // initialize language and font

    private void initRecyclerView() {

        layoutManager = new LinearLayoutManager(this);
        adapter = new NotificationstAdapter(this, arrayList);
        notification_recycler_view.setHasFixedSize(true);
        notification_recycler_view.setLayoutManager(layoutManager);
        notification_recycler_view.setAdapter(adapter);

    } // initialize recycler view

    private void getNotifications() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final NotificationApi notificationApi = retrofit.create(NotificationApi.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.showNotifications(user_id);

                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            response.body();

                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                arrayList = response.body().getData();

                                initRecyclerView();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), NotificationsActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // show notifications

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }

}
