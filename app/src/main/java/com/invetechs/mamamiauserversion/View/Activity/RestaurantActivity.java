package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.RestaurantsAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.RestaurantApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AllRestaurantResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RestaurantActivity extends Language {


    // bind views
    @BindView(R.id.recycler_view_restaurants)
    RecyclerView recyclerView;

    @BindView(R.id.searcView_restaurant)
    SearchView searcView_restaurant;

    // vars
    RestaurantsAdapter adapter;
    List<AllRestaurantResponse.DataBean> restaurantList = new ArrayList<>(), restaurants = new ArrayList<>();
    double latitude = 0, longitude = 0;
    String flag = "", restaurantCategory = "0", restaurantTag = "0";
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    double distanceWithKilo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        ButterKnife.bind(this);
        initLanguage();
        getLatAndLong(); // get lat & lng from homeFragment
        getRestaurantFromApi(); // get restaurants from api
        initListOfRestaurants();
        setToolBarConfig();
        searchOnRestaurants();
        toastDialog = new ShowDialog();
    } // onCreate function


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void searchOnRestaurants() {
        searcView_restaurant.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    } // function of searchOnRestaurants

    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.restaurants));

        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void getLatAndLong() {
        Intent intent = getIntent();
        if (intent != null) {
            latitude = intent.getDoubleExtra("lat", 0);
            longitude = intent.getDoubleExtra("lng", 0);
            flag = intent.getStringExtra("flag");

            if (flag.equals("restaurant")) {
                restaurantCategory = "1"; // restaurant
                restaurantTag = "0"; // all kind of meals
            } else if (flag.equals("housefood")) {
                restaurantCategory = "2"; // housefood
                restaurantTag = "0"; // all kind of meals
            } else if (flag.equals("dessert")) {
                restaurantCategory = "0"; // restaurant & housefood
                restaurantTag = "2"; //dessert only
            }
        }

    } // function of getLatAndLong


    private void initListOfRestaurants() {
        adapter = new RestaurantsAdapter(this, restaurantList, "restaurant", latitude, longitude, RestaurantActivity.this, flag);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    } // function of initListOfRestaurants

    private void fillListWithRestaurant() {
        for (int i = 0; i < restaurants.size(); i++) {
            restaurants.get(i).setDistanceToUser(convertDistanceToKilo(latitude, longitude, restaurants.get(i).getLat(), restaurants.get(i).getLng()));
        }
        sortRestaurantsWithDistance();
    } // function of fillListWithRestaurant

    private void getRestaurantFromApi() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final RestaurantApi restaurantApiApi = retrofit.create(RestaurantApi.class);

                Call<AllRestaurantResponse> getConnection = restaurantApiApi.getRestaurants(latitude, longitude, restaurantCategory, restaurantTag);

                getConnection.enqueue(new Callback<AllRestaurantResponse>() {
                    @Override
                    public void onResponse(Call<AllRestaurantResponse> call, Response<AllRestaurantResponse> response) {
                        try {

                            String code = response.body().getCode();

                            response.body();
                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {

                                restaurants = response.body().getData();
                                fillListWithRestaurant();

                            } else if (code.equals("1313")) {

                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<AllRestaurantResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });


            }

        }.start();
    } // get restaurants from api


    private void sortRestaurantsWithDistance() {
        Collections.sort(restaurants, new Comparator<AllRestaurantResponse.DataBean>() {
            @Override
            public int compare(AllRestaurantResponse.DataBean bo1, AllRestaurantResponse.DataBean bo2) {
                return (bo1.getDistanceToUser() > bo2.getDistanceToUser() ? 1 : -1);
            }
        });

        for (int i = 0; i < restaurants.size(); i++) {
            restaurantList.add(restaurants.get(i));
            adapter.notifyDataSetChanged();
            Log.i("QP", "item " + i + " : " + restaurantList.get(i).getDistanceToUser());
        }

    } // function of sortRestaurantsWithDistance

    private double convertDistanceToKilo(double sLat, double sLng, double eLat, double eLng) {
        final double distanceWithMeter = distance(sLat, sLng, eLat, eLng);

        distanceWithKilo = distanceWithMeter / 1000;
        int precision = 10; //keep 1 digits
        distanceWithKilo = Math.floor(distanceWithKilo * precision + .5) / precision;

        return distanceWithKilo;
    } // function of sortRestaurantWithNearest

    public float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    } // distance between restaurant and user

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
} // class of RestaurantActivity
