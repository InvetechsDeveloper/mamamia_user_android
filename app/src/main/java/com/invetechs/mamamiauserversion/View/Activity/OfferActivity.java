package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.OfferAndSearchMenuAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MenuAndSearchApi.OfferAndMenuSearchApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AllRestaurantResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.SearchMenuResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OfferActivity extends Language {


    @BindView(R.id.recycler_view_offers)
    RecyclerView recyclerView;

    // vars

    OfferAndSearchMenuAdapter adapter ;
    List<SearchMenuResponse.DataBean> menuList = new ArrayList<>() , menuApiList = new ArrayList<>()
            ,sortedMenuList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog ;
    double latitude =0, longitude =0;
    double distanceWithKilo;
    String categories ="0" , tags ="5"; //  5 default value display offers or all category, category 1 --> restaurant , 2 --> houseFood ,
    // tags : 1 --> banquet, 2 --> dessert, 3 --> meal , 4 --> beverge, 5--> offer
    String searchText =  "0"; // search in all without write text
    String flag = "offer";
    ToolBarConfig toolBarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        ButterKnife.bind(this);
        initLanguage();
        initOfferList();
        getIntentFromSearchOrOffer();
        setToolBarConfig();
        toastDialog = new ShowDialog();



    }//onCreate function


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP","language arabic" + sharedPreferences);


        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void setToolBarConfig()
    {
         toolBarConfig = new ToolBarConfig(this,"none");

         if(flag.equals("offer"))
        toolBarConfig.setTitle(getString(R.string.offers));
         else if (flag.equals("search"))
             toolBarConfig.setTitle(getString(R.string.searchResult));


        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig
    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void getIntentFromSearchOrOffer()
    {
        Intent intent = getIntent();
        if(intent != null)
        {
            flag = intent.getStringExtra("flag");
            if(flag.equals("offer"))
            {
                categories = "0"; // all restarant and house food
                tags = "5"; // offer
                searchText="0";
                latitude = intent.getDoubleExtra("lat",0);
                longitude = intent.getDoubleExtra("lng",0);
            } // get intent from offer button
            else if (flag.equals("search"))
            {
                categories = intent.getStringExtra("categories");
                tags = intent.getStringExtra("tags");
                searchText = intent.getStringExtra("searchText");
                latitude = intent.getDoubleExtra("lat",0);
                longitude = intent.getDoubleExtra("lng",0);

            } // getintent from search
            getMenuFromApi(categories,tags,searchText);
        } // open activity from search display
        else
        {
            getMenuFromApi(categories,tags,searchText);
        } // open activity from offer button on home page display offers
        Log.i("QP","category1 : "+categories
                +" tag1 : "+tags+" searchText1 : "+searchText);
    } // get intent from search

    private void initOfferList()
    {
        adapter = new OfferAndSearchMenuAdapter(this,sortedMenuList,OfferActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    } // function of initOfferList

    private void sortRestaurantsWithDistance()
    {
        Collections.sort(menuList, new Comparator<SearchMenuResponse.DataBean>() {
            @Override
            public int compare(SearchMenuResponse.DataBean bo1, SearchMenuResponse.DataBean bo2) {
                return (bo1.getDistanceToUser() > bo2.getDistanceToUser() ? 1 : -1);
            }
        });

        for(int i=0;i<menuList.size();i++)
        {
            sortedMenuList.add(menuList.get(i));
            adapter.notifyDataSetChanged();
            Log.i("QP","item "+i+" : "+sortedMenuList.get(i).getDistanceToUser());
        }
        adapter.notifyDataSetChanged();

    } // function of sortRestaurantsWithDistance

    private void getMenuFromApi(final String category, final String tag, final String searchText) // timeCategory mean launch , dinner ,..  , typeCategory mean meal , dessert ,..
    {
               Log.i("QP","category : "+category
               +" tag : "+tag+" searchText : "+searchText);
        if(menuList.size() > 0 && menuApiList.size() >0)
        {
            menuApiList.clear();
            menuList.clear();
        }

        progress = new CustomDialogProgress();
        progress.init(this);


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(OfferActivity.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final OfferAndMenuSearchApi menuApi = retrofit.create(OfferAndMenuSearchApi.class);

                Call<SearchMenuResponse> getConnection = menuApi.getMenuSearch(latitude,longitude,category,tag,searchText);

                getConnection.enqueue(new Callback<SearchMenuResponse>() {
                    @Override
                    public void onResponse(Call<SearchMenuResponse> call, Response<SearchMenuResponse> response) {
                        try {

                            String code = response.body().getCode();

                            response.body();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {

                                menuApiList = response.body().getData();
                                fillMenuList();
                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchMenuResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });
            }

        }.start();
    } // function of getMenuFromApi

    private void fillMenuList()
    {
        for(int i=0;i<menuApiList.size();i++)
        {
            menuApiList.get(i).setDistanceToUser(convertDistanceToKilo(latitude,longitude,menuApiList.get(i).getLat(),menuApiList.get(i).getLng()));
            menuList.add(menuApiList.get(i));

        }
        sortRestaurantsWithDistance();


        if(menuList.size() <= 0)
        {
            toastDialog.initDialog(getString(R.string.notFound),OfferActivity.this);
        }
    } // function of fillMenuList

    private double convertDistanceToKilo(double sLat , double sLng , double eLat , double eLng)
    {
        final double distanceWithMeter =distance(sLat,sLng,eLat,eLng);

        distanceWithKilo =  distanceWithMeter / 1000 ;
        int precision = 10; //keep 1 digits
        distanceWithKilo= Math.floor(distanceWithKilo * precision +.5)/precision;

        return distanceWithKilo;
    } // function of sortRestaurantWithNearest

    public float distance (double lat_a, double lng_a, double lat_b, double lng_b )
    {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b-lat_a);
        double lngDiff = Math.toRadians(lng_b-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    } // distance between restaurant and user

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }

}// class of OfferActivity
