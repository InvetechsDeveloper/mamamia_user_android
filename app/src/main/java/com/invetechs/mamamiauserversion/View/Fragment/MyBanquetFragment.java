package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Control.MyBanquetOffersAdapter;
import com.invetechs.mamamiauserversion.Control.MyBanquetOrdersAdapter;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.MyBanquetOfferApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.BanquetOfferResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.MyBanquetOrders;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MyBanquerOrderResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class MyBanquetFragment extends Fragment {

    // Bind views

    @BindView(R.id.tv_offers)
    TextView tv_offers;

    @BindView(R.id.tv_orders)
    TextView tv_orders;

    @BindView(R.id.party_date)
    TextView party_date;

    @BindView(R.id.offer_title)
    TextView offer_title;

    @BindView(R.id.tv_banquet_name)
    TextView tv_banquet_name;

    @BindView(R.id.tv_party_date)
    TextView tv_party_date;

    @BindView(R.id.offers_view)
    View offers_view;

    @BindView(R.id.orders_view)
    View orders_view;

    @BindView(R.id.offers_linear)
    LinearLayout offers_linear;

    @BindView(R.id.orders_linear)
    LinearLayout orders_linear;

    @BindView(R.id.offers_recycler_view)
    RecyclerView offers_recycler_view;

    @BindView(R.id.orders_recycler_view)
    RecyclerView orders_recycler_view;


    @BindView(R.id.layoutoffer1)
    RelativeLayout relativeLayout1;

    @BindView(R.id.layoutoffer2)
    RelativeLayout relativeLayout2;

    // vars
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    List<BanquetOfferResponse.DataBean.OffersBean> myBanquetOffersArrayListApi = new ArrayList<>();
    List<BanquetOfferResponse.DataBean.OffersBean> myBanquetOffersArrayList = new ArrayList<>();
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    // offers
    LinearLayoutManager layoutManager;
    MyBanquetOffersAdapter banquetOffersAdapter;

    // orders
    private List<MyBanquerOrderResponse.DataBean> order_data_array_list = new ArrayList<>();
    private List<MyBanquerOrderResponse.DataBean> order_array_first = new ArrayList<>();
    private MyBanquerOrderResponse.DataBean new_order;
    LinearLayoutManager layoutManager_orders;
    MyBanquetOrdersAdapter ordersAdapter;
    private List<MyBanquerOrderResponse.DataBean> cancelled_array_list = new ArrayList<>();
    SharedPreferences sharedPreferences;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_banquet_fragment, container, false);
        ButterKnife.bind(this, view);
        initLanguage();
        initOrdersRecyclerView();
        initOffersRecyclerView();
        getBanquetOffersFromApi();
        return view;
    }

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            tv_offers.setTypeface(font);
            tv_orders.setTypeface(font);
            party_date.setTypeface(font);
            offer_title.setTypeface(font);
            tv_banquet_name.setTypeface(font);
            tv_party_date.setTypeface(font);

            Log.i("QP", "ar");

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            tv_offers.setTypeface(font);
            tv_orders.setTypeface(font);
            party_date.setTypeface(font);
            offer_title.setTypeface(font);
            tv_banquet_name.setTypeface(font);
            tv_party_date.setTypeface(font);
            Log.i("QP", "en");
        }

    }//initialize language

    @OnClick(R.id.rel_offers)
    public void clickOffers() {
        if (myBanquetOffersArrayListApi.size() > 0) {
            myBanquetOffersArrayList.clear();
            myBanquetOffersArrayListApi.clear();
        }
        offers_view.setVisibility(View.VISIBLE);
        orders_view.setVisibility(View.GONE);
        offers_linear.setVisibility(View.VISIBLE);
        orders_linear.setVisibility(View.GONE);
        getBanquetOffersFromApi();
    } // show view line when click on offers


    @OnClick(R.id.rel_orders)
    public void clickOrders() {
        if (order_data_array_list.size() > 0) {

            order_data_array_list.clear();
            order_array_first.clear();
            cancelled_array_list.clear();
        }
        orders_view.setVisibility(View.VISIBLE);
        offers_view.setVisibility(View.GONE);
        orders_linear.setVisibility(View.VISIBLE);
        offers_linear.setVisibility(View.GONE);
        getMyBanquetOrders();

    } // show view line when click on orders

    private void initOffersRecyclerView() {

        layoutManager = new LinearLayoutManager(getContext());
        banquetOffersAdapter = new MyBanquetOffersAdapter(getContext(), myBanquetOffersArrayList);
        offers_recycler_view.setHasFixedSize(true);
        offers_recycler_view.setLayoutManager(layoutManager);
        offers_recycler_view.setAdapter(banquetOffersAdapter);
    } // offers recyclerview

    private void initOrdersRecyclerView() {

        layoutManager_orders = new LinearLayoutManager(getContext());
        ordersAdapter = new MyBanquetOrdersAdapter(getContext(), order_data_array_list);
        orders_recycler_view.setHasFixedSize(true);
        orders_recycler_view.setLayoutManager(layoutManager_orders);
        orders_recycler_view.setAdapter(ordersAdapter);

    }

    public int getPrefUserId() {
        int userId = 0;
        SharedPreferences preferences = getContext().getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        }
        catch (Exception e)
        {
            Log.i("QP" , "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId


    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    private void getBanquetOffersFromApi() {
        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOfferApi banquetOfferApi = retrofit.create(MyBanquetOfferApi.class);

                final Call<BanquetOfferResponse> getInterestConnection = banquetOfferApi.getBanquetOffer(getPrefUserId());

                getInterestConnection.enqueue(new Callback<BanquetOfferResponse>() {
                    @Override
                    public void onResponse(Call<BanquetOfferResponse> call, Response<BanquetOfferResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {

                                tv_banquet_name.setText(response.body().getData().getPartyname());
                                tv_party_date.setText(response.body().getData().getPartydate());
                                myBanquetOffersArrayListApi = response.body().getData().getOffers();
                                fillOffers();
                            } //
                            else if (code.equals("1313")) {
                                if (myBanquetOffersArrayListApi.size() > 0)
                                    myBanquetOffersArrayListApi.clear();
                                if (myBanquetOffersArrayList.size() > 0)
                                    myBanquetOffersArrayList.clear();
                                banquetOffersAdapter.notifyDataSetChanged();

                                relativeLayout1.setVisibility(View.INVISIBLE);
                                relativeLayout2.setVisibility(View.INVISIBLE);
                                toastDialog.initDialog(getString(R.string.youHaveNotBanquets), getActivity());
                                offer_title.setText(R.string.youHaveNotBanquets);
                            }


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BanquetOfferResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();

    } // function of getBanquetOffersFromApi


    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getMyBanquetOrders() {
        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(getActivity());
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOrders ordersApi = retrofit.create(MyBanquetOrders.class);

                final Call<MyBanquerOrderResponse> getInterestConnection = ordersApi.showMyBanquerOrders(getPrefUserId());

                getInterestConnection.enqueue(new Callback<MyBanquerOrderResponse>() {
                    @Override
                    public void onResponse(Call<MyBanquerOrderResponse> call, Response<MyBanquerOrderResponse> response) {
                        try {
                            String code = response.body().getCode();

                            if (code.equals("200")) {
                                if (response.body().getNewOrder() != null) {
                                    new_order = new MyBanquerOrderResponse.DataBean(response.body().getNewOrder().getPartyname(),
                                            response.body().getNewOrder().getId(), response.body().getNewOrder().getPartydate(),
                                            response.body().getNewOrder().getStatus(), response.body().getNewOrder().getCodeorder());

                                    order_data_array_list.add(new_order);
                                    ordersAdapter.notifyDataSetChanged();

                                }
                                cancelled_array_list = response.body().getCancelledOrders();
                                order_array_first = response.body().getData();

                                fillOrders();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<MyBanquerOrderResponse
                            > call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }
        }.start();

    }

    private void fillOrders() {
        for (int i = 0; i < order_array_first.size(); i++) {

            order_data_array_list.add(order_array_first.get(i));
        }

        for (int i = 0; i < cancelled_array_list.size(); i++) {
            order_data_array_list.add(cancelled_array_list.get(i));
        }

        ordersAdapter.notifyDataSetChanged();
    }

    private void fillOffers() {
        if (myBanquetOffersArrayListApi.size() > 0) {


            for (int i = 0; i < myBanquetOffersArrayListApi.size(); i++) {
                myBanquetOffersArrayList.add(myBanquetOffersArrayListApi.get(i));
            } // for loop
            banquetOffersAdapter.notifyDataSetChanged();
        } // if user have offer on his banquet

        else {
            toastDialog.initDialog(getString(R.string.noOffers), getActivity());
            offer_title.setText(R.string.noOffers);
        }
    } // function of fillOffers

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }

} // class of MyBanquetFragment
