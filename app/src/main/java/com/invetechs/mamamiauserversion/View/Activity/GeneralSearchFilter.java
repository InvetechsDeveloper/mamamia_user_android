package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;


import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

public class GeneralSearchFilter extends Language {


    @BindView(R.id.ch_housefood)
    CheckBox ch_housefood;

    @BindView(R.id.ch_restaurant)
    CheckBox ch_restaurant;

    @BindView(R.id.ch_meal)
    CheckBox ch_meal;

    @BindView(R.id.ch_dessert)
    CheckBox ch_dessert;

    @BindView(R.id.ch_offer)
    CheckBox ch_offer;

    @BindView(R.id.ch_banquet)
    CheckBox ch_banquet;

    @BindView(R.id.ch_bevrage)
    CheckBox ch_bevrage;

    @BindView(R.id.searchEdit)
    TextInputEditText ed_searchEdit;

    double latitude = 0, longitude = 0;

    String categories = "0", tags = "0"; //  0 default value mean all tags or all category, category 1 --> restaurant , 2 --> houseFood ,
    // tags : 1 --> banquet, 2 --> dessert, 3 --> meal , 4 --> beverge, 5--> offer
    String searchText = "0"; // search in all without write text
    ArrayList<String> filterListCategories = new ArrayList<>();
    ArrayList<String> filterListTags = new ArrayList<>();
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_search_filter);

        ButterKnife.bind(this);
        initLanguage();
        getFilterFromUser();

        getLatLngFromHomeScreen();

        setToolBarConfig();

        toastDialog = new ShowDialog();

    } // onCreate function

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void setToolBarConfig()
    {

        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.toolbar_title.setVisibility(View.GONE);

        toolBarConfig.mamamia_logo.setVisibility(View.VISIBLE);
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void getLatLngFromHomeScreen() {
        Intent intent = getIntent();
        if (intent != null) {
            latitude = intent.getDoubleExtra("lat", 0);
            longitude = intent.getDoubleExtra("lng", 0);
        }
    }

    private void getFilterFromUser()
    {
        ch_housefood.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) searchBasedOnUserFilter("house", 1); //Log.i("QP","checked");
                else searchBasedOnUserFilter("house", 0);//Log.i("QP","unChecked");
            }
        }); // houseFood

        ch_restaurant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("rest", 1);
                else searchBasedOnUserFilter("rest", 0);
            }
        }); // restaurant


        ch_meal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("meal", 1);
                else searchBasedOnUserFilter("meal", 0);
            }
        }); // meal

        ch_dessert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("dessert", 1);
                else searchBasedOnUserFilter("dessert", 0);
            }
        }); // dessert

        ch_offer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("offer", 1);
                else searchBasedOnUserFilter("offer", 0);
            }
        }); // offer

        ch_banquet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("banquet", 1);
                else searchBasedOnUserFilter("banquet", 0);
            }
        }); // banquet

        ch_bevrage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) searchBasedOnUserFilter("bevrage", 1);
                else searchBasedOnUserFilter("bevrage", 0);
            }
        }); // bevrage

    } // getFilterFromUser

    private void searchBasedOnUserFilter(String category, int checked) {
        if (category.equals("house")) {
            if (checked == 1) {
                filterListCategories.add(getResources().getString(R.string.housefood));
                updateFilterListCategory();
            } // houseFood checked
            else if (checked == 0) {
                filterListCategories.remove(getResources().getString(R.string.housefood));
                updateFilterListCategory();
            } // houseFood unChecked
        } // houseFood


        if (category.equals("rest")) {
            if (checked == 1) {
                filterListCategories.add(getResources().getString(R.string.restaurant));
                updateFilterListCategory();
            } // restaaurant checked
            else if (checked == 0) {
                filterListCategories.remove(getResources().getString(R.string.restaurant));
                updateFilterListCategory();
            } // restaaurant unChecked
        } // restaaurant

        if (category.equals("meal")) {
            if (checked == 1) {
                filterListTags.add(getResources().getString(R.string.meal));
                updateFilterListTag();
            } // meal checked
            else if (checked == 0) {
                filterListTags.remove(getResources().getString(R.string.meal));
                updateFilterListTag();
            } // meal unChecked
        } // meal

        if (category.equals("dessert")) {
            if (checked == 1) {
                filterListTags.add(getResources().getString(R.string.dessert));
                updateFilterListTag();
            } // dessert checked
            else if (checked == 0) {
                filterListTags.remove(getResources().getString(R.string.dessert));
                updateFilterListTag();
            } // dessert unChecked
        } // dessert
        if (category.equals("offer")) {
            if (checked == 1) {
                filterListTags.add(getResources().getString(R.string.offers));
                updateFilterListTag();
            } // offer checked
            else if (checked == 0) {
                filterListTags.remove(getResources().getString(R.string.offers));
                updateFilterListTag();
            } // offer unChecked
        } // offer
        if (category.equals("banquet")) {
            if (checked == 1) {
                filterListTags.add(getResources().getString(R.string.banquet));
                updateFilterListTag();
            } // banquet checked
            else if (checked == 0) {
                filterListTags.remove(getResources().getString(R.string.banquet));
                updateFilterListTag();
            } // banquet unChecked

        } // banquet
        if (category.equals("bevrage")) {
            if (checked == 1) {
                filterListTags.add(getResources().getString(R.string.beverage));
                updateFilterListTag();
            } // bevrage checked
            else if (checked == 0) {
                filterListTags.remove(getResources().getString(R.string.beverage));
                updateFilterListTag();
            } // bevrage unChecked
        } // bevrage
    } // search Based on filter chosen by user

    private void updateFilterListCategory() {

        String filterTags = "";

        if (!filterListCategories.isEmpty()) {
            categories = "";
            for (int i = 0; i < filterListCategories.size(); i++) {
                filterTags = filterTags + " , " + filterListCategories.get(i);
                categories = categories + "," + convertNameToId(filterListCategories.get(i));
            }

        } else {
            categories = "0";
        }
        Log.i("QP", "category : " + categories);
    } // function of updateFilterListCategory restaurant or housefood

    private void updateFilterListTag() {
        String filterTags = "";
        if (!filterListTags.isEmpty()) {
            tags = "";
            for (int i = 0; i < filterListTags.size(); i++) {
                filterTags = filterTags + " , " + filterListTags.get(i);
                tags = tags + "," + convertNameToId(filterListTags.get(i));
            }

        } else {
            tags = "0";
        }
        Log.i("QP", "tag : " + tags);
    } // function of updateFilterListTag banquet , dessert , meal , beverge or offer

    private int convertNameToId(String name) {
        int id = 0;
        // category
        if (name.equals(getResources().getString(R.string.restaurant)))
            id = 1;
        if (name.equals(getResources().getString(R.string.housefood)))
            id = 2;

        //tags
        if (name.equals(getResources().getString(R.string.banquet)))
            id = 1;
        if (name.equals(getResources().getString(R.string.dessert)))
            id = 2;
        if (name.equals(getResources().getString(R.string.meal)))
            id = 3;
        if (name.equals(getResources().getString(R.string.beverage)))
            id = 4;
        if (name.equals(getResources().getString(R.string.offers)))
            id = 5;

        return id;
    } // function convert category or tag to id

    public void searchButton(View view) {

        searchText = ed_searchEdit.getText().toString();
        if (searchText.isEmpty()) searchText = "0";

        Intent intent = new Intent(getApplicationContext(), OfferActivity.class);
        intent.putExtra("tags", tags);
        intent.putExtra("categories", categories);
        intent.putExtra("searchText", searchText);
        intent.putExtra("lat", latitude);
        intent.putExtra("lng", longitude);
        intent.putExtra("flag", "search");
        startActivity(intent);
    } // function of button search

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }

} // class of GeneralSearchFilter
