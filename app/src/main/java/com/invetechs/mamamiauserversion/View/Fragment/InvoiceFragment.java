package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Control.InvoiceAdapter;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.InvoiceApi.InvoicesApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.InvoiceResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class InvoiceFragment extends Fragment {

    // Bind views
    @BindView(R.id.rec_view_invoice)
    RecyclerView rec_view_invoice;

    // vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog  toastDialog = new ShowDialog();
    private List<InvoiceResponse.ArchivesBean> archiveArrayList = new ArrayList<>();
    int user_id = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invoice_fragment,container,false);
        ButterKnife.bind(this,view);
        initLanguage();
        getUserId();
        getInvoices();
        initRecyclerView();
        return view;
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getUserId() {
        prefs = getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        user_id = prefs.getInt("id", 0);
        Log.i("QP", " invoice userId : " + user_id);

    } // getUserId function


    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        }


    } //initialization of language

    private void getInvoices()
    {
        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(getActivity());
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final InvoicesApi ordersApi = retrofit.create(InvoicesApi.class);

                final Call<InvoiceResponse> getInterestConnection = ordersApi.showInvoices(user_id);

                getInterestConnection.enqueue(new Callback<InvoiceResponse>() {
                    @Override
                    public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                        try
                        {
                            String code = response.body().getCode();
                            Log.i("QP","code" + code);
                            response.body();
                            Log.i("QP","response"+ response.body().getCode());

                            if (code.equals("200"))
                            {
                                archiveArrayList =  response.body().getArchives();

                                initRecyclerView();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry),getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    }

    private void initRecyclerView() {

        LinearLayoutManager  linearLayoutManager = new LinearLayoutManager(getContext());
        InvoiceAdapter     adapter = new InvoiceAdapter(getContext(), archiveArrayList);
        rec_view_invoice.setHasFixedSize(true);
        rec_view_invoice.setAdapter(adapter);
        rec_view_invoice.setLayoutManager(linearLayoutManager);

    } // initialize recycler view

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }
}
