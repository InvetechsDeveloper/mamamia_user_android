package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.CancelOrder;
import com.invetechs.mamamiauserversion.Retrofit.Request.MyBanquetApi.MyBanquetOfferApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi.OrderdDetails;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.BanquetOfferResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.CancelOrderDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrderDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OrdersAndOffersDetails extends Language {

    // bind views
    @BindView(R.id.tv_banquet_name)
    TextView tv_banquet_name;

    @BindView(R.id.tv_restaurant_name)
    TextView tv_restaurant_name;

    @BindView(R.id.tv_order_number)
    TextView tv_order_number;

    @BindView(R.id.tv_order_status)
    TextView tv_order_status;

    @BindView(R.id.tv_customer_name)
    TextView tv_customer_name;

    @BindView(R.id.tv_customer_number)
    TextView tv_customer_number;

    @BindView(R.id.tv_party_location)
    TextView tv_party_location;

    @BindView(R.id.tv_party_date)
    TextView tv_party_date;

    @BindView(R.id.tv_party_type)
    TextView tv_party_type;

    @BindView(R.id.tv_order_time)
    TextView tv_order_time;

    @BindView(R.id.tv_order_date)
    TextView tv_order_date;

    @BindView(R.id.tv_person_number)
    TextView tv_person_number;

    @BindView(R.id.et_customer_comments)
    EditText et_customer_comments;

    @BindView(R.id.et_vendor_comments)
    EditText et_vendor_comments;

    @BindView(R.id.tv_preparation_time)
    TextView tv_preparation_time;

    @BindView(R.id.tv_distance)
    TextView tv_distance;

    @BindView(R.id.tv_delivery_fees)
    TextView tv_delivery_fees;

    @BindView(R.id.tv_totally_price)
    TextView tv_totally_price;

    @BindView(R.id.btn_accept)
    Button btn_accept;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;

    @BindView(R.id.btn_refuse)
    Button btn_refuse;

    @BindView(R.id.restaurant_linear)
    LinearLayout restaurant_linear;

    @BindView(R.id.vendor_comments_linear)
    LinearLayout vendor_comments_linear;

    @BindView(R.id.preparation_time_linear)
    LinearLayout preparation_time_linear;

    @BindView(R.id.distance_linear)
    LinearLayout distance_linear;

    @BindView(R.id.fees_linear)
    LinearLayout fees_linear;

    @BindView(R.id.total_linear)
    LinearLayout total_linear;

    // vars

    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    SharedPreferences.Editor editor;
    String order_id = "", status = "", banquet_name = "", restaurant_name = "", orderNumber = "", orderStatus = "",
            customerName = "", customerNumber = "", party_location = "", party_date = "", party_type = "",
            orderTime = "", orderDate = "", person_numbers = "", customerComments = "", vendor_comments = "",
            preparationTime = "", distance = "", diliveryFees = "",
            totallyPrice = "";
    OrderDetailsResponse.OneorderBean oneorder;
    private CancelOrderDetailsResponse.OneorderBean cancel_oneorder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_and_offers_details);
        ButterKnife.bind(this);
        initLanguage();
        getOrderId();
        setToolBarConfig();
        if (status.equals("2") || status.equals("3") || status.equals("-4")) {
            getDetailsCancelApi(order_id);
        } else if (status.equals("offer")) {
            Log.i("QP", "offer");
            getOrderDetailsApi(order_id);
        } else {

            getOrderDetailsApi(order_id);
        }

        toastDialog = new ShowDialog();
    } // function of onCreate


    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.order_details));

        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    private void getDetailsCancelApi(final String id) {

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit


                RetrofitConnection connection = new RetrofitConnection(OrdersAndOffersDetails.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final OrderdDetails orderApi = retrofit.create(OrderdDetails.class);

                final Call<CancelOrderDetailsResponse> getInterestConnection = orderApi.getOneOrderCancelDetailsApi(id);

                getInterestConnection.enqueue(new Callback<CancelOrderDetailsResponse>() {
                    @Override
                    public void onResponse(Call<CancelOrderDetailsResponse> call, Response<CancelOrderDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code from cancel" + code);

                            if (code.equals("200")) {
                                cancel_oneorder = response.body().getOneorder();
                                getDetailsDataCancelled();

                            } // found order
                            else if (code.equals("1313")) {

                            } // not found

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<CancelOrderDetailsResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), OrdersAndOffersDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
            }

        }.start();
    }

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }

    } // initialize language and font

    private void getOrderId() {
        Intent order_num = getIntent();
        if (order_num != null) {
            order_id = order_num.getStringExtra("id");
            status = order_num.getStringExtra("status");
            Log.i("QP", "Order id = " + order_id);
        }
    } // get order id to show details of order

    @OnClick(R.id.btn_view_list)
    public void openList() {

        if (status.equals("2") || status.equals("3") || status.equals("-4")) {
            intentOrder(cancel_oneorder.getComponent());
        } else {
            intentOrder(oneorder.getComponent());

        }

    } // show order list

    private void intentOrder(String component) {
        Intent intent = new Intent(this, ViewOrderOrBanquetList.class);
        intent.putExtra("order_list", component);
        startActivity(intent);
    }

    private void getDetailsData() {

        if (sharedPreferences.getString("language", "ar").equals("en")) {
            banquet_name = oneorder.getEn_partyname();
            if (oneorder.getKitchen() != null) {
                restaurant_name = oneorder.getKitchen().getEn_title();
            }
        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {
            banquet_name = oneorder.getAr_partyname();
            if (oneorder.getKitchen() != null) {
                restaurant_name = oneorder.getKitchen().getEn_title();

            }

        }

        orderNumber = oneorder.getId();
        orderStatus = oneorder.getStatus();
        if (orderStatus.equals("-2")) {
            tv_order_status.setText(R.string.rejected);
        } else if (orderStatus.equals("1")) {
            tv_order_status.setText(R.string.accepted);
        }
        if (status.equals("offer")) {
            tv_order_status.setText(getString(R.string.newOrder));
            btn_accept.setVisibility(View.VISIBLE);
            btn_refuse.setVisibility(View.VISIBLE);

        } // if user open from offers

        customerName = oneorder.getUser().getName();
        customerNumber = oneorder.getUser().getMobilenumber();
        party_location = oneorder.getPartylocation();
        party_date = oneorder.getPartydate();
        party_type = oneorder.getPartytype();
        orderTime = oneorder.getOrdertime();
        orderDate = oneorder.getOrderdate();
        person_numbers = oneorder.getGuestnum();
        customerComments = oneorder.getUsernote();
        vendor_comments = oneorder.getVendornote();
        preparationTime = oneorder.getNeededtime();
        distance = oneorder.getDistancetouser();
        diliveryFees = oneorder.getDeliverytotalprice();
        totallyPrice = oneorder.getTotalprice();
        setTextWithData();

    }

    @OnClick(R.id.btn_accept)
    public void btnAccept() {
        acceptBanquetOfferApi(oneorder.getCodeorder(), oneorder.getKitchen_id());
    } // function of button accept

    @OnClick(R.id.btn_refuse)
    public void btnRefused() {
        refuseBanquetOfferApi(oneorder.getCodeorder(), oneorder.getKitchen_id());

    } // function of button refuse

    private void acceptBanquetOfferApi(final String orderCode, final int kitchenId) {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(OrdersAndOffersDetails.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOfferApi banquetOfferApi = retrofit.create(MyBanquetOfferApi.class);

                final Call<BanquetOfferResponse> getInterestConnection = banquetOfferApi.acceptBanquetOffer(orderCode, kitchenId);

                getInterestConnection.enqueue(new Callback<BanquetOfferResponse>() {
                    @Override
                    public void onResponse(Call<BanquetOfferResponse> call, Response<BanquetOfferResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.banquetAccepted), OrdersAndOffersDetails.this);

                                Intent intent = new Intent(OrdersAndOffersDetails.this, MenuActivity.class);
                                intent.putExtra("fragmentFlag","home");
                                startActivity(intent);
                            } //


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BanquetOfferResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), OrdersAndOffersDetails.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function  of acceptBanquetOfferApi

    private void refuseBanquetOfferApi(final String orderCode, final int kitchenId) {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MyBanquetOfferApi banquetOfferApi = retrofit.create(MyBanquetOfferApi.class);

                final Call<BanquetOfferResponse> getInterestConnection = banquetOfferApi.refuseBanquetOffer(orderCode, kitchenId);
                getInterestConnection.enqueue(new Callback<BanquetOfferResponse>() {
                    @Override
                    public void onResponse(Call<BanquetOfferResponse> call, Response<BanquetOfferResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.banquetRefused), OrdersAndOffersDetails.this);

                                Intent intent = new Intent(OrdersAndOffersDetails.this, MenuActivity.class);
                                intent.putExtra("fragmentFlag","home");
                                startActivity(intent);

                            } //


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BanquetOfferResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), OrdersAndOffersDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function  of refuseBanquetOfferApi

    private void setTextWithData() {

        tv_banquet_name.setText(banquet_name);
        tv_restaurant_name.setText(restaurant_name);
        tv_order_number.setText(orderNumber);
        tv_customer_name.setText(customerName);
        tv_customer_number.setText(customerNumber);
        tv_party_location.setText(party_location);
        tv_party_date.setText(party_date);
        tv_party_type.setText(party_type);
        tv_order_time.setText(orderTime);
        tv_order_date.setText(orderDate);
        tv_person_number.setText(person_numbers);
        et_customer_comments.setText(customerComments);
        et_vendor_comments.setText(vendor_comments);
        tv_preparation_time.setText(preparationTime);
        tv_distance.setText(distance);
        tv_delivery_fees.setText(diliveryFees);
        tv_totally_price.setText(totallyPrice);

    } // set texts with the data from api

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getOrderDetailsApi(final String id) {

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final OrderdDetails orderApi = retrofit.create(OrderdDetails.class);

                final Call<OrderDetailsResponse> getInterestConnection = orderApi.getOneOrderDetailsApi(id, "user");

                getInterestConnection.enqueue(new Callback<OrderDetailsResponse>() {
                    @Override
                    public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                oneorder = response.body().getOneorder();
                                getDetailsData();

                            } // found order
                            else if (code.equals("1313")) {

                            } // not found

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), OrdersAndOffersDetails.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // function of getOrderDetailsApi

    private void getDetailsDataCancelled() {

        restaurant_linear.setVisibility(View.GONE);
        vendor_comments_linear.setVisibility(View.GONE);
        preparation_time_linear.setVisibility(View.GONE);
        distance_linear.setVisibility(View.GONE);
        fees_linear.setVisibility(View.GONE);
        total_linear.setVisibility(View.GONE);


        if (sharedPreferences.getString("language", "ar").equals("en")) {
            banquet_name = cancel_oneorder.getEn_partyname();

        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {
            banquet_name = cancel_oneorder.getEn_partyname();
        }

        orderNumber = cancel_oneorder.getCodeorder();
        orderStatus = cancel_oneorder.getStatus();
        if (orderStatus.equals("2") || orderStatus.equals("3")) {
            btn_cancel.setVisibility(View.VISIBLE);
            tv_order_status.setText(R.string.newOrder);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelOrder(order_id);
                }
            });
        } else if (orderStatus.equals("-4")) {
            tv_order_status.setText(R.string.canceled);
        }

        customerName = cancel_oneorder.getUser().getName();
        customerNumber = cancel_oneorder.getUser().getMobilenumber();
        party_location = cancel_oneorder.getPartylocation();
        party_date = cancel_oneorder.getPartydate();
        party_type = cancel_oneorder.getPartytype();
        orderTime = cancel_oneorder.getOrderTime();
        orderDate = cancel_oneorder.getOrderDate();
        person_numbers = cancel_oneorder.getGuestnum();
        customerComments = cancel_oneorder.getUsernote();
        setTextWithCancelData();

    }

    private void cancelOrder(final String code_order) {

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                RetrofitConnection connection = new RetrofitConnection(OrdersAndOffersDetails.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final CancelOrder cancelOrder = retrofit.create(CancelOrder.class);

                final Call<LoginResponse> getInterestConnection = cancelOrder.canceNewlOrder(code_order);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.order_cancelled), OrdersAndOffersDetails.this);

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(OrdersAndOffersDetails.this, MenuActivity.class);
                                        intent.putExtra("fragmentFlag","home");
                                        startActivity(intent);
                                        finish();


                                    }
                                }, 2000);


                            } //  success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), OrdersAndOffersDetails.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // cancel order

    private void setTextWithCancelData() {


        tv_banquet_name.setText(banquet_name);
        tv_order_number.setText(orderNumber);
        tv_customer_name.setText(customerName);
        tv_customer_number.setText(customerNumber);
        tv_party_location.setText(party_location);
        tv_party_date.setText(party_date);
        tv_party_type.setText(party_type);
        tv_order_time.setText(orderTime);
        tv_order_date.setText(orderDate);
        tv_person_number.setText(person_numbers);
        et_customer_comments.setText(customerComments);

    } // set details with cancelData

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
}
