package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Control.PointsAdapter;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.PointsApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.UserPointsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class PointsFragment extends Fragment {

    // Bind views
    @BindView(R.id.recycler_view_points)
    RecyclerView recycler_view_points;

    @BindView(R.id.rest)
    TextView rest;

    @BindView(R.id.points)
    TextView points;

    @BindView(R.id.discount)
    TextView discount;

    // Vvars

    Handler handler;
    CustomDialogProgress progress;
    PointsAdapter adapter;
    SharedPreferences sharedPreferences;
    ShowDialog toastDialog = new ShowDialog();
    List<UserPointsResponse.DataBean> pointsListApi = new ArrayList<>();
    List<UserPointsResponse.DataBean> pointsList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.points_fragment, container, false);
        ButterKnife.bind(this, view);
        checkLanguage();
        initRecyclerView();
        getUsersPointFromApi(getPrefUserId());
        return view;
    } // function of onCreateView

    public int getPrefUserId()
    {
        int userId = 0;
        SharedPreferences preferences = getContext().getSharedPreferences("MyPrefsFile",MODE_PRIVATE);
        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        }
        catch (Exception e)
        {
            Log.i("QP" , "Exception" + e.getMessage());
        }

        return  userId;
    } // function of getPrefUserId

    private void checkLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            rest.setTypeface(font);
            points.setTypeface(font);
            discount.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            rest.setTypeface(font);
            points.setTypeface(font);
            discount.setTypeface(font);

        }
    } // checkLanguage


    private void initRecyclerView() {


        adapter = new PointsAdapter(getContext(), pointsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_view_points.setHasFixedSize(true);
        recycler_view_points.setLayoutManager(linearLayoutManager);
        recycler_view_points.setAdapter(adapter);

    } //initialize recycler view

    private void fillPointsList() {
        for (int i = 0; i < pointsListApi.size(); i++) {
            pointsList.add(pointsListApi.get(i));
        }
        adapter.notifyDataSetChanged();
    } // function of fillPointsList

    private void getUsersPointFromApi(final int userId) {
        progress = new CustomDialogProgress();
        progress.init(getContext());


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(getActivity());
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final PointsApi pointsApi = retrofit.create(PointsApi.class);

                final Call<UserPointsResponse> getInterestConnection = pointsApi.getUserPoints(userId);

                getInterestConnection.enqueue(new Callback<UserPointsResponse>() {
                    @Override
                    public void onResponse(Call<UserPointsResponse> call, Response<UserPointsResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            Log.i("QP", "response" + response.body().getCode());

                            if (response.body() != null) {
                                if (code.equals("200")) {

                                    pointsListApi = response.body().getData();
                                    fillPointsList();
                                } // user add review to this kitchen before

                            } // user didn't add review to this kitchen before
                            else {
                                toastDialog.initDialog(getString(R.string.retry), getActivity());
                            } // rsponse faild

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<UserPointsResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of selectAllKitchenReview

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }
} // class of PointsFragment
