package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.InvoiceOrderDetailsAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi.OrderdDetails;
import com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi.RejectOrder;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrderDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OrdersDetails extends Language {

    // bind views

    @BindView(R.id.tv_order_name)
    TextView tv_order_name;

    @BindView(R.id.tv_meals)
    TextView tv_meals;

    @BindView(R.id.tv_order_number)
    TextView tv_order_number;

    @BindView(R.id.tv_order_status)
    TextView tv_order_status;

    @BindView(R.id.tv_customer_name)
    TextView tv_customer_name;

    @BindView(R.id.tv_customer_number)
    TextView tv_customer_number;

    @BindView(R.id.tv_customer_location)
    TextView tv_customer_location;

    @BindView(R.id.tv_order_time)
    TextView tv_order_time;

    @BindView(R.id.tv_order_date)
    TextView tv_order_date;

    @BindView(R.id.tv_customer_comments)
    TextView tv_customer_comments;

    @BindView(R.id.tv_vendor_comments)
    TextView tv_vendor_comments;

    @BindView(R.id.tv_preparation_time)
    TextView tv_preparation_time;

    @BindView(R.id.tv_outlets)
    TextView tv_outlets;

    @BindView(R.id.tv_distance)
    TextView tv_distance;

    @BindView(R.id.tv_delivery_fees)
    TextView tv_delivery_fees;

    @BindView(R.id.tv_totally_price)
    TextView tv_totally_price;

    @BindView(R.id.tv_discountpoint)
    TextView tv_discountpoint;

    @BindView(R.id.tv_priceVat)
    TextView tv_priceVat;

    @BindView(R.id.tv_orderPrice)
    TextView tv_orderPrice;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;

    @BindView(R.id.linear_discount)
    LinearLayout linear_discount;

    Handler handler;
    CustomDialogProgress progress;

    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;

    // Vars
    String orderName = "", orderNumber = "", orderStatus = "", type = "user",
            customerName = "", customerNumber = "", customerLocation = "",
            orderTime = "", orderDate = "", customerComments = "", vendorComments = "",
            preparationTime = "", outlets = "", distance = "", diliveryFees = "", order_id = "",
            totallyPrice = "", mealsArray = "", discount = "", price_vat = "" , order_price = "";
    InvoiceOrderDetailsAdapter adapter;
    SharedPreferences.Editor editor;
    OrderDetailsResponse.OneorderBean oneorder;
    List<OrderDetailsResponse.OneorderBean.MealsBean> mealsApi = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_details);
        ButterKnife.bind(this);
        initLanguage();
        getOrderId();
        getOrderDetailsApi();
        setToolBarConfig();
        toastDialog = new ShowDialog();
    }


    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.order_details));
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();


    } // function of setToolBarConfig

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void getOrderId() {
        Intent order_num = getIntent();
        if (order_num != null) {
            order_id = order_num.getStringExtra("orderId");
            Log.i("QP", "Order id = " + order_id);
        }
    } // get order id to show details of order

    private void getDetailsData() {

        if (sharedPreferences.getString("language", "ar").equals("en")) {
            if (!(oneorder.getMeals().size() == 0)) {
                orderName = oneorder.getMeals().get(0).getEn_title();
            }

            outlets = oneorder.getKitchen().getEn_title();

            if (oneorder.getMeals() != null) {

                mealsApi = oneorder.getMeals();
                for (int i = 0; i < mealsApi.size(); i++) {
                    mealsArray = mealsArray + mealsApi.get(i).getAmount() + " " + mealsApi.get(i).getEn_title() + "\n";
                }
            }
        }

        else if (sharedPreferences.getString("language", "ar").equals("ar")) {
            if (!(oneorder.getMeals().size() == 0)) {
                orderName = oneorder.getMeals().get(0).getAr_title();
            }
            outlets = oneorder.getKitchen().getAr_title();

            if (oneorder.getMeals() != null) {
                mealsApi = oneorder.getMeals();

                for (int i = 0; i < mealsApi.size(); i++) {
                      //mealsArrayList.add(mealsApi.get(i));
                    mealsArray = mealsArray + mealsApi.get(i).getAmount() + " " + mealsApi.get(i).getAr_title() + "\n";
                }
                //   adapter.notifyDataSetChanged();
            }
        }
        orderNumber = oneorder.getId();
        orderStatus = oneorder.getStatus();

        if (orderStatus.equals("0")) {
            btn_cancel.setVisibility(View.VISIBLE);
            tv_order_status.setText(getString(R.string.newOrder));
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rejectOrder(order_id, type);
                }
            });
        }
        else if (orderStatus.equals("-2")) {
            tv_order_status.setText(R.string.canceled);
        } else if (orderStatus.equals("-1")) {
            tv_order_status.setText(R.string.rejected);
        } else if (orderStatus.equals("1")) {
            tv_order_status.setText(R.string.accepted);
        }

        customerName = oneorder.getUser().getName();
        customerNumber = oneorder.getUser().getMobilenumber();
        customerLocation = oneorder.getUserplace();
        orderTime = oneorder.getOrdertime();
        orderDate = oneorder.getOrderdate();
        customerComments = oneorder.getUsernote().trim();

        Log.i("QP", "customer comments : " + customerComments);

        vendorComments = oneorder.getVendornote();
        preparationTime = oneorder.getNeededtime();
        distance = oneorder.getDistancetouser();
        diliveryFees = oneorder.getDeliverytotalprice();
        totallyPrice = oneorder.getTotalprice();

        if (oneorder.getDiscountpoint().equals("0")) {
            linear_discount.setVisibility(View.GONE);
        } else if (!oneorder.getDiscountpoint().equals("0")) {
            discount = oneorder.getDiscountpoint();

        }

        price_vat = oneorder.getPriceVat();
        double priceVat = Double.parseDouble(price_vat);
        Log.i("QP", "priceVat : " + priceVat);

        double totalDilevery = Double.parseDouble(diliveryFees);
        Log.i("QP", "totalDilevery : " + totalDilevery);

        order_price = oneorder.getMealstotalprice();

        Log.i("QP", "order price price : " + order_price);

        setTextWithData();


    } // get details

    private void rejectOrder(final String order_id, final String type) {

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                RetrofitConnection connection = new RetrofitConnection(OrdersDetails.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();
                final RejectOrder orderApi = retrofit.create(RejectOrder.class);
                final Call<LoginResponse> getInterestConnection = orderApi.rejectOrderApi(order_id, type);
                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.order_cancelled), OrdersDetails.this);

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent intent = new Intent(OrdersDetails.this, MenuActivity.class);
                                        intent.putExtra("fragmentFlag","home");
                                        startActivity(intent);
                                        finish();

                                    }
                                }, 2000);


                            } //  success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), OrdersDetails.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // reject order if its status cancel return 0 from api

    private void setTextWithData() {

        tv_order_name.setText(orderName);
        tv_order_number.setText(orderNumber);
        tv_customer_name.setText(customerName);
        tv_customer_number.setText(customerNumber);
        tv_customer_location.setText(customerLocation);
        tv_order_time.setText(orderTime);
        tv_order_date.setText(orderDate);

        Log.i("QP", "customer comments in settext : " + customerComments);

        if (customerComments.equals("none")) {

            tv_customer_comments.setText(getString(R.string.no_comments));
            Log.i("QP", "customer comments in if : " + customerComments);

        } else {
            tv_customer_comments.setText(customerComments);
            Log.i("QP", "customer comments in else : " + customerComments);


        }
        tv_vendor_comments.setText(vendorComments);
        tv_preparation_time.setText(preparationTime);
        tv_outlets.setText(outlets);
        tv_distance.setText(distance);
        tv_delivery_fees.setText(diliveryFees);
        tv_totally_price.setText(totallyPrice);
        tv_meals.setText(mealsArray);
        tv_discountpoint.setText(discount);
        tv_priceVat.setText(price_vat + "");
        tv_orderPrice.setText(order_price + "");


    } // set texts with the data from api

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getOrderDetailsApi() {

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final OrderdDetails orderApi = retrofit.create(OrderdDetails.class);

                final Call<OrderDetailsResponse> getInterestConnection = orderApi.getOneOrderDetailsApi(order_id, "user");

                getInterestConnection.enqueue(new Callback<OrderDetailsResponse>() {
                    @Override
                    public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                oneorder = response.body().getOneorder();
                                getDetailsData();

                            } // found order
                            else if (code.equals("1313")) {

                            } // not found

                            progress.dismiss();


                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), OrdersDetails.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // function of getOrderDetailsApi

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }

}
