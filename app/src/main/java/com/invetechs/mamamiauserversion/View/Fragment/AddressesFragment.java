package com.invetechs.mamamiauserversion.View.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.AddressesAdapter;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AdressesApi.AddNewAddressApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.AddAddressActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class AddressesFragment extends Fragment
{


    @BindView(R.id.recyclerView_addresses)
    RecyclerView recyclerView;

    @BindView(R.id.bt_addNewAddress)
    Button bt_addNewAddress;

    // vars

    List<AddressResponse.DataBean> addressListApi = new ArrayList<>();
    List<AddressResponse.DataBean> addressList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences sharedPreferences;
    ShowDialog  toastDialog = new ShowDialog();
    AddressesAdapter adapter ;
    ToolBarConfig toolBarConfig;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addresses_fragment,container,false);
        ButterKnife.bind(this,view);
        checkLanguage();
        toolBarConfig = new ToolBarConfig(getActivity());
        initRecycelerView(); // intialize reclerview
        getAddressesFromApi(); // get laa addresses of user
        return view;
    } // onCreateView function

    private void checkLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            bt_addNewAddress.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            bt_addNewAddress.setTypeface(font);

        }
    } // checkLanguage


    @OnClick(R.id.bt_addNewAddress)
    public void addNewAddress()
    {
        Intent intent = new Intent(getContext(), AddAddressActivity.class);
        intent.putExtra("flag","new");
        startActivity(intent);
    } // button add new address

    private  void initRecycelerView()
    {
        adapter = new AddressesAdapter(getActivity(),addressList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    } // intialize list of addresses

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public int getPrefUserId()
    {
        int userId = 0;
        SharedPreferences preferences = getContext().getSharedPreferences("MyPrefsFile",MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        }
        catch (Exception e)
        {
            Log.i("QP" , "Exception" + e.getMessage());
        }

        return  userId;
    } // function of getPrefUserId

    private void getAddressesFromApi()
    {
        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AddNewAddressApi addressApi = retrofit.create(AddNewAddressApi.class);

                final Call<AddressResponse> getInterestConnection = addressApi.selectUserAddresses(getPrefUserId());

                getInterestConnection.enqueue(new Callback<AddressResponse>() {
                    @Override
                    public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                        try
                        {
                            if(response.body() != null) {
                                String code = response.body().getCode();
                                Log.i("QP", "code" + code);

                                if (code.equals("200")) {

                                    addressListApi = response.body().getData();
                                    fillLsitWithAddresses();
                                } //

                            } // if response success
                            else
                            {
                                toastDialog.initDialog(getString(R.string.retry),getActivity());
                            } // response faild
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<AddressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry),getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    }//function of getAddressesFromApi

    private  void  fillLsitWithAddresses()
    {
        for(int i=0;i<addressListApi.size();i++)
        {
            addressList.add(addressListApi.get(i));
        }
        adapter.notifyDataSetChanged();

        if(addressListApi.size() <= 0)
            toastDialog.initDialog(getString(R.string.noAddresses),getContext());

    } // function of fillListWithAddresses

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }

} // class of AddressesFragment
