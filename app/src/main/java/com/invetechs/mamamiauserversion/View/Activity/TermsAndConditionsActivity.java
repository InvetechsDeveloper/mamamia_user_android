package com.invetechs.mamamiauserversion.View.Activity;

import android.content.SharedPreferences;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;

public class TermsAndConditionsActivity extends Language {

    // vars
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    ToolBarConfig toolBarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        ButterKnife.bind(this);
        initLanguage();
        setToolBarConfig();

    } // onCreate function

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP","language arabic" + sharedPreferences);
        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void setToolBarConfig()
    {
        toolBarConfig = new ToolBarConfig(this,"none");
        toolBarConfig.setTitle(getString(R.string.terms_and_condition));
    } // function of setToolBarConfig

    public void buttonOk(View view)
    {
        onBackPressed();
    } // function of buttonOk

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
} // class of TermsAndConditionsActivity
