package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;

import me.anwarshahriar.calligrapher.Calligrapher;

public class SplashScreen extends Language {

    // vars
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editorAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initSplashScreen();
        initLanguage();
        deleteSavedLocation();
    } // function of onCreate

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }


    private  void deleteSavedLocation()
    {
        editorAddress =getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
        editorAddress.putFloat("lat", 0);
        editorAddress.putFloat("lng", 0);
        editorAddress.apply();
    } // function of savedLocation

    private void initSplashScreen() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                    Intent intent = new Intent(SplashScreen.this, MenuActivity.class);
                intent.putExtra("fragmentFlag","home");
                startActivity(intent);
                    finish();

            }
        }, 3000);
    } // set splash screen with 3 seconds

    private void initLanguage() {

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Log.i("QP","language : login " + sharedPreferences.getString("language", "en"));

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // initialize language and font

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    } // prevent touch from resize image

}
