package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Language.MyContextWrapper;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.ForgetPasswordApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.VerifyApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.VerifyForgetPasswordApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.NotificationResponse;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RegisterResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NeedVerificationActivity extends AppCompatActivity {

    // bind views
    @BindView(R.id.et_code)
    TextInputEditText et_code;

    @BindView(R.id.tv_resend_code)
    TextView tv_resend_code;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    // vars
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String mobile = "", code = "" , category = "user" , verifi = "";
    int userId ;
    Boolean verification;
    String phone = "" , token = "";
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_verification);
        ButterKnife.bind(this);
        toastDialog = new ShowDialog();
        initLanguage();
        initToolBar();
        getDataFromIntent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts


    private void initToolBar() {
        setSupportActionBar(tool_bar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);

            et_code.setTypeface(null);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
            et_code.setTypeface(null);
        }


    } // initialize language and font

    @OnClick(R.id.btn_confirm)
    public void confirmClick() {
        code = et_code.getText().toString().trim();
        phone = getIntent().getExtras().getString("mobilenumber");

        Log.i("QP", "allCode : " + code);
        verify();

    } // confirm code

    private void verify() {

        if (verification == true) {

            progress = new CustomDialogProgress();
            progress.init(this);


            handler = new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    progress.dismiss();
                    super.handleMessage(msg);
                }

            };
            progress.show();
            new Thread() {
                public void run() {

                    Retrofit retrofit = RetrofitConnection.ConnectWith();

                    final VerifyForgetPasswordApi userApi = retrofit.create(VerifyForgetPasswordApi.class);

                    Log.i("QP", "phone before request  : " + phone);

                    final Call<LoginResponse> getInterestConnection = userApi.verifyForgetPass(phone, code , category);

                    getInterestConnection.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            try {

                                Log.i("QP", "response  : " + response + "\n" + mobile + " : " + code);
                                String code = response.body().getCode();
                                Log.i("QP", "response  : " + code);
                                if (code.equals("200")) {
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("isLogin", "true");
                                    LoginResponse.UserdataBean user = response.body().getUserdata();

                                    editor.putInt("id", user.getId());
                                    editor.putString("isLogin", "true");
                                    editor.putString("name", user.getName());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("verify", "0");
                                    editor.putString("mobilenumber", user.getMobilenumber());
                                    editor.putString("image", user.getImage());
                                    editor.apply();
                                    editor.apply();

                                    Intent intent = new Intent(NeedVerificationActivity.this, MenuActivity.class);
                                    intent.putExtra("fragmentFlag","home");
                                    startActivity(intent);
                                    finish();
                                    progress.dismiss();

                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.wrong_token), NeedVerificationActivity.this);
                                    progress.dismiss();
                                }

                                progress.dismiss();

                            } // try
                            catch (Exception e) {
                                Log.i("QP", "exception : " + e.toString());
                                progress.dismiss();
                            } // catch
                        } // onResponse

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {

                            toastDialog.initDialog(getString(R.string.retry), NeedVerificationActivity.this);
                            Log.i("QP", "error : " + t.toString());
                            progress.dismiss();
                        } // on Failure
                    });
// Retrofit

                }

            }.start();
        }
        else {

            progress = new CustomDialogProgress();
            progress.init(this);


            handler = new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    progress.dismiss();
                    super.handleMessage(msg);
                }

            };
            progress.show();
            new Thread() {
                public void run() {
//Retrofit
                    Retrofit retrofit = RetrofitConnection.ConnectWith();

                    final VerifyApi userApi = retrofit.create(VerifyApi.class);

                    final Call<LoginResponse> getInterestConnection = userApi.verify(userId, code);

                    getInterestConnection.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            try {

                                Log.i("QP", "response  : " + response + "\n" + userId + " : " + code);


                                String code = response.body().getCode();
                                if (code.equals("200")) {
                                    LoginResponse.UserdataBean user = response.body().getUserdata();

                                    progress.dismiss();
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putInt("id", user.getId());
                                    editor.putString("isLogin", "true");
                                    editor.putString("name", user.getName());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("verify", "0");
                                    editor.putString("mobilenumber", user.getMobilenumber());
                                    editor.putString("image", user.getImage());
                                    editor.apply();


                                    Intent intent = new Intent(NeedVerificationActivity.this, MenuActivity.class);
                                    intent.putExtra("fragmentFlag","home");
                                    startActivity(intent);
                                    finish();

                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.wrong_token), NeedVerificationActivity.this);
                                } else if (code.equals("1314")) {
                                    toastDialog.initDialog(getString(R.string.user_verified), getApplicationContext());
                                }
                                progress.dismiss();

                            } // try
                            catch (Exception e) {
                                Log.i("QP", "exception : " + e.toString());
                                progress.dismiss();
                            } // catch
                        } // onResponse

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {

                            toastDialog.initDialog(getString(R.string.retry), NeedVerificationActivity.this);
                            Log.i("QP", "error : " + t.toString());
                            progress.dismiss();
                        } // on Failure
                    });
                }

            }.start();
        } // verify

    }

    private void getDataFromIntent() {

        verification = getIntent().getExtras().getBoolean("verify");
        phone = getIntent().getExtras().getString("mobilenumber");
        token = getIntent().getExtras().getString("verification_code");

        Intent intent = getIntent();
        if (intent != null) {
            userId =  intent.getIntExtra("id",0);
            mobile = intent.getStringExtra("mobilenumber");
            Log.i("QP", "user id in verification activity: " + userId);
            Log.i("QP", "mobile number : " + mobile);
            Log.i("QP", "verification_code : " + token);

            if (token != null) {
                toastDialog.initDialog(getString(R.string.code_send), NeedVerificationActivity.this);
            }
        }

    } // get data from login

    @OnClick(R.id.tv_resend_code)
    public void resendCode() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend_code.setText(getString(R.string.second_remaining) + " " + millisUntilFinished / 1000);
                tv_resend_code.setEnabled(false);
            }
            public void onFinish() {
                tv_resend_code.setText(getString(R.string.resend_code));
                tv_resend_code.setEnabled(true);
                resendVerCode();
            }
        }.start();

    } // click on resend code again

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void resendVerCode() {

        phone = getIntent().getExtras().getString("mobilenumber");

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ForgetPasswordApi userApi = retrofit.create(ForgetPasswordApi.class);

                Log.i("Print", "phone before request : " + phone);

                final Call<LoginResponse> getInterestConnection = userApi.forgetPass(phone , category);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {

                            String code = response.body().getCode();
                            if (code.equals("200")) {
                                LoginResponse.UserdataBean user_data = response.body().getUserdata();
                               // verifi = user_data.getVerificationcode();
                              //  Log.i("Print", "resend verification : " + verifi);
                                toastDialog.initDialog(getString(R.string.code_send) , NeedVerificationActivity.this);
                                progress.dismiss();

                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.invalid_mobile), NeedVerificationActivity.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getApplicationContext());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // function of forget password

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    } // on destroy
}
