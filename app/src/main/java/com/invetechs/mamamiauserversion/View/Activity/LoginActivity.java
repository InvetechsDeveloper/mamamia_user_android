package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Language.MyContextWrapper;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.LoginApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    // bind views
    @BindView(R.id.tv_forget_password)
    TextView tvForgetPassword;

    @BindView(R.id.et_phone_number)
    MaterialEditText et_phone_number;

    @BindView(R.id.et_password)
    TextInputEditText et_password;

    @BindView(R.id.phone_layout)
    TextInputLayout phone_layout;

    @BindView(R.id.password_layout)
    TextInputLayout password_layout;

    // vars
    String mobile, password;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String category = "user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        hideKeyboard();
        initLanguage();
        progress = new CustomDialogProgress();
        toastDialog = new ShowDialog();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
        intent.putExtra("fragmentFlag", "home");
        startActivity(intent);
        finish();
    }

    private void initLanguage() {

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/GESSTwoMedium.otf");

            phone_layout.setTypeface(font);
            password_layout.setTypeface(font);
            et_phone_number.setTypeface(null);
            et_password.setTypeface(null);

            Log.i("QP", "language arabic" + sharedPreferences);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
            phone_layout.setTypeface(font);
            password_layout.setTypeface(font);
            et_phone_number.setTypeface(null);
            et_password.setTypeface(null);


        }


    } // initialize language and font

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click

    @OnClick(R.id.tv_forget_password)
    public void forgetPassword() {
        try {
            Intent forgetPassword = new Intent(getApplicationContext(), ForgetPassword.class);
            startActivity(forgetPassword);
        } catch (Exception e) {
            Log.e("QP", "exception : " + e.toString());
        }

    } // intent to forget password activity

    @OnClick(R.id.tv_register)
    public void register() {
        Intent register = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(register);
    } // intent to registration activity

    @OnClick(R.id.btn_login)
    public void menuScreen() {
        if (!ValidateMobile() | !ValidatePassword()) {

            return;
        }

        mobile = et_phone_number.getText().toString().trim();
        password = et_password.getText().toString().trim();

        login();
    } // intent to menu activity

    public boolean ValidatePassword() {

        String passwordInput = et_password.getText().toString().trim();
        if (passwordInput.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (passwordInput.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;
        } else {
            et_password.setError(null);
            return true;
        }
    } // validate password

    private boolean ValidateMobile() {

        String Mobile = et_phone_number.getText().toString().trim();

        if (Mobile.isEmpty()) {
            et_phone_number.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (Mobile.length() == 10) {
            if (!(Mobile.charAt(0) == '0' && Mobile.charAt(1) == '5')) {
                et_phone_number.setError(getString(R.string.enter_correct_number));
                return false;
            }
        } else {
            et_phone_number.setError(getString(R.string.enter_correct_number));
        }

        return true;
    } // validation for mobile phone

    private void generateCode() {

    } // function of generate code

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void login() {

        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final LoginApi userApi = retrofit.create(LoginApi.class);

                final Call<LoginResponse> getInterestConnection = userApi.login(mobile, password, category);

                getInterestConnection.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);
                            response.body();
                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                LoginResponse.UserdataBean user = response.body().getUserdata();
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("isLogin", "true");
                                editor.putInt("id", user.getId());
                                editor.putString("name", user.getName());
                                editor.putString("image", user.getImage());
                                editor.putString("password", password);
                                editor.putString("token", user.getToken());
                                editor.apply();
                                Log.i("QP", "login id" + user.getId());

                                Log.e("QS", "login : token" + user.getToken());
                                // start intent
                                Intent menuActivity = new Intent(LoginActivity.this, MenuActivity.class);
                                menuActivity.putExtra("fragmentFlag", "home");
                                startActivity(menuActivity);
                                finish();

                            } // login success

                            else if (code.equals("1314")) {

                                LoginResponse.UserdataBean user = response.body().getUserdata();
                                Intent intent = new Intent(LoginActivity.this, NeedVerificationActivity.class);
                                intent.putExtra("verify", true);
                                intent.putExtra("mobilenumber", mobile);
                                intent.putExtra("verification_code", user.getVerificationcode());
                                startActivity(intent);
                                finish();

                            } // user need to verify code
                            else if (code.equals("1315")) {
                                toastDialog.initDialog(getString(R.string.password_invalid), LoginActivity.this);
                                progress.dismiss();

                            } // invaild password
                            else if (code.equals("1316")) {
                                toastDialog.initDialog(getString(R.string.invalid_mobile), LoginActivity.this);
                                progress.dismiss();

                            } // invaild mobile

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), LoginActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();

    } // login function

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }


} // class of Login
