package com.invetechs.mamamiauserversion.View.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.util.Patterns;
import android.view.WindowManager;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Language.MyContextWrapper;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AuthApi.RegisterApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.RegisterResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {

    // bind views

    @BindView(R.id.profileImage)
    CircleImageView profileImage;

    @BindView(R.id.show_terms)
    TextView show_terms;

    @BindView(R.id.et_name)
    TextInputEditText et_name;

    @BindView(R.id.et_phoneNumber)
    TextInputEditText et_phoneNumber;

    @BindView(R.id.et_email)
    TextInputEditText et_email;

    @BindView(R.id.et_password)
    TextInputEditText et_password;

    @BindView(R.id.et_confirm_password)
    TextInputEditText et_confirm_password;

    @BindView(R.id.cb_terms)
    AppCompatCheckBox cb_terms;

    @BindView(R.id.inPass)
    TextInputLayout inPass;

    @BindView(R.id.inCoPass)
    TextInputLayout inCoPass;

    // vars
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final int GET_FROM_GALLERY = 10;
    private static final int REQUEST_CAMERA = 1888;
    Handler handler;
    CustomDialogProgress progress;
    File file;
    String imagePath = "";
    String password = "", confirmPassword = "", email = "", name = "", mobileNumber = "", category = "";
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    ShowDialog toastDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        toastDialog = new ShowDialog();
        initLanguage();
        hideKeyboard();
    } // onCreate function


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/GESSTwoMedium.otf");

            Log.i("QP", "language arabic" + sharedPreferences);
            et_phoneNumber.setTypeface(null);
            et_password.setTypeface(null);
            inPass.setTypeface(font);
            inCoPass.setTypeface(font);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
            et_phoneNumber.setTypeface(null);
            et_password.setTypeface(null);
            inPass.setTypeface(font);
            inCoPass.setTypeface(font);

        }


    } // initialize language and font

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    @OnClick(R.id.show_terms)
    public void showTerms() {
        Intent intent = new Intent(RegisterActivity.this, TermsAndConditionsActivity.class);
        startActivity(intent);
    } // show Terms & Conditions to the user

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click

    @OnClick(R.id.profileImage)
    public void changeProfileImage() {
        int PERMISSION_REQUEST_CODE = 1;

        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            // ask user for permission to access storage // allow - deni

        }

        startActivityForResult(new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                , GET_FROM_GALLERY);

    } // click imageView

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == GET_FROM_GALLERY) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        } // if

    } // onResult Activity

    public void onSelectFromGalleryResult(Intent data) {
        if (data != null) {

            Uri imageUri = data.getData();
            profileImage.setImageURI(imageUri);
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = this.getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = getRealPathFromURI(imageUri);
            }

            // upload image
        }
    } // onSelectFromGalleryResult

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }// function of getRealPathFromURI

    public void onCaptureImageResult(Intent data) {
        if (data != null) {

           /* bitmap = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedimage = Base64.encodeToString(imageBytes, Base64.DEFAULT);*/

            Uri imageUri = data.getData();
            profileImage.setImageURI(imageUri);
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.ACTION_IMAGE_CAPTURE};

            Cursor cursor = this.getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = getRealPathFromURI(imageUri);
            }


            Log.i("QP", imagePath);
        }
    } // onCaptureImageResult

    @OnClick(R.id.btn_register)
    public void registerButton() {
        if (!ValidateMobile() | !ValidatePassword() | !ValidateUserName() | !ValidatecheckBox()) {
            return;
        } // if vaildation of editText Regidtration

        else {

            name = et_name.getText().toString();
            email = et_email.getText().toString();
            mobileNumber = et_phoneNumber.getText().toString().trim();
            password = et_password.getText().toString();
            category = "user";
            if (!email.equals("")) {
                if (!ValidateEmail()) {
                    return;
                }
            }
            register();

        } // function of button register click

    } // click on register button

    private boolean ValidatecheckBox() {

        if (cb_terms.isChecked()) {
            cb_terms.setError(null);
            return true;

        } else {
            cb_terms.setError(getString(R.string.accept_terms));
            return false;
        }

    } // validation for check box

    private boolean ValidateUserName() {

        name = et_name.getText().toString().trim();

        if (name.isEmpty()) {
            et_name.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (et_name.length() < 5) {
            et_name.setError(getString(R.string.enter_full_name));
            return false;
        } else {

            return true;
        }
    } // validation on username

    public boolean ValidatePassword() {

        password = et_password.getText().toString().trim();
        confirmPassword = et_confirm_password.getText().toString().trim();

        if (password.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (password.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;

        }

        if (!password.equals(confirmPassword)) {
            toastDialog.initDialog(getString(R.string.password_not_match), RegisterActivity.this);

            return false;

        }
        return true;


    } // validation on password

    private boolean ValidateMobile() {

        mobileNumber = et_phoneNumber.getText().toString().trim();

        if (mobileNumber.isEmpty()) {
            et_phoneNumber.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (!(mobileNumber.charAt(0) == '0' && mobileNumber.charAt(1) == '5' && mobileNumber.length() == 10)) {
            et_phoneNumber.setError(getString(R.string.enter_correct_number));
            return false;
        }

        return true;
    } // validation on mobile number

    private boolean ValidateEmail() {

        email = et_email.getText().toString().trim();

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError(getString(R.string.error_invalid_email));
            return false;
        }

        return true;
    } // validation if the user write email as it is optional

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void register() {

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();
                final RegisterApi userApi = retrofit.create(RegisterApi.class);

                Log.i("QP", "file : " + imagePath);

                if (!imagePath.equals("")) {
                    file = new File(imagePath);

                    // RequestBody userIdRequest = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userID));
                    RequestBody requestBody = RequestBody.create(MediaType.parse("image"), file);

                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                    //RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

                    RequestBody namePart = RequestBody.create(MediaType.parse("text/plain"), name);
                    RequestBody mobilePart = RequestBody.create(MediaType.parse("text/plain"), mobileNumber);
                    RequestBody passwordPart = RequestBody.create(MediaType.parse("text/plain"), password);
                    RequestBody emailPart = RequestBody.create(MediaType.parse("text/plain"), email);
                    RequestBody categoryPart = RequestBody.create(MediaType.parse("text/plain"), category);

                    Log.i("QP", "file : " + fileToUpload + "\n" +
                            "name : " + name + "\n" + "mobile : " + mobileNumber + "\n"
                            + "email : " + email + "\n" + "category : " + category + "\n" + "password : " + password + "\n");
                    Call<RegisterResponse> getConnection = userApi.registerApiWithImage(fileToUpload, namePart, mobilePart,
                            passwordPart, emailPart, categoryPart);

                    getConnection.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            try {
                                String code = response.body().getCode();
                                Log.i("QP", "code : " + code);

                                if (code.equals("200")) {
                                    RegisterResponse.UserdataBean user = response.body().getUserdata();
                                    int id = user.getId();
                                    String mobile_number = user.getMobilenumber();
                                    String verification_code = user.getVerificationcode();

                                    progress.dismiss();
                                    Intent intent = new Intent(RegisterActivity.this, NeedVerificationActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("mobilenumber", mobile_number);
                                    intent.putExtra("verification_code", verification_code);
                                    Log.i("QP", "send id from register : " + id);
                                    Log.i("QP", "mobile number from register: " + mobile_number);
                                    startActivity(intent);
                                    finish();
                                } else if (code.equals("1313")) {
                                    progress.dismiss();
                                    toastDialog.initDialog(getString(R.string.unique), RegisterActivity.this);

                                }

                                progress.dismiss();
                            } catch (Exception e) {

                                Log.i("QP", "exception : " + e.toString());
                                progress.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {

                            Log.i("QP", "error : " + t.toString());
                            progress.dismiss();
                        }
                    });
                } // if user upload image
                else {
                    Call<RegisterResponse> getConnection = userApi.registerApiWithOutImage(name, mobileNumber, password, email, category);

                    getConnection.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            try {
                                String code = response.body().getCode();

                                Log.i("QP", "code : " + code);

                                if (code.equals("200")) {
                                    RegisterResponse.UserdataBean user = response.body().getUserdata();
                                    int id = user.getId();
                                    Log.i("QP", "send id from register : " + id);
                                    String mobile_number = user.getMobilenumber();
                                    String verification_code = user.getVerificationcode();
                                    Log.i("QP", "verification code : " + verification_code);
                                    Log.i("QP", "send mobile from register : " + user.getMobilenumber());

                                    progress.dismiss();
                                    Intent intent = new Intent(RegisterActivity.this, NeedVerificationActivity.class);
                                    intent.putExtra("id", id);
                                    Log.i("QP", "send id from register : " + id);
                                    intent.putExtra("mobilenumber", mobile_number);
                                    intent.putExtra("verification_code", verification_code);
                                    startActivity(intent);
                                    finish();

                                } else if (code.equals("1313")) {
                                    progress.dismiss();
                                    toastDialog.initDialog(getString(R.string.unique), RegisterActivity.this);


                                }

                                progress.dismiss();
                            } catch (Exception e) {

                                Log.i("QP", "exception : " + e.toString());
                                progress.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            toastDialog.initDialog(getString(R.string.retry), RegisterActivity.this);
                            Log.i("QP", "error : " + t.toString());
                            progress.dismiss();
                        }
                    });
                } // register without Image
            }
        }.start();
    } // function of register

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    } // on destroy
} // class of public class RegisterActivity extends AppCompatActivity {

