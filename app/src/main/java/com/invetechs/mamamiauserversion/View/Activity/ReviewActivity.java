package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.ReviewAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.RestaurantApi.ReviewApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.ReviewResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ReviewActivity extends Language {

    // Bind views
    @BindView(R.id.rate_recycler_view)
    RecyclerView rate_recycler_view;

    @BindView(R.id.layoutAddReview)
    CardView layoutAddReview;

    @BindView(R.id.btn_review)
    Button btn_review;

    @BindView(R.id.et_review)
    EditText et_review;
    @BindView(R.id.rateBar_addRate)
    AppCompatRatingBar rateBar_addRate;
    @BindView(R.id.tx_addRate)
    TextView tx_addRate;

    // vars

    ReviewAdapter adapter;
    LinearLayoutManager layoutManager;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    LinearLayout layout;
    List<ReviewResponse.DataBean> reviewListApi = new ArrayList<>();
    List<ReviewResponse.DataBean> reviewList = new ArrayList<>();
    int intentKitchenId=0 ;
    ToolBarConfig toolBarConfig;
    // user add review and rate
    String userReview = ""; float userRate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.bind(this);
        toolBarConfig = new ToolBarConfig(this,"none");
        initLanguage();
        initRecyclerView();
        getIntentId();
        selectAllKitchenReview(toolBarConfig.getPrefUserId(),intentKitchenId);
        takeRateFromUser();
        setToolBarConfig();
        toastDialog = new ShowDialog();


    } // function of onCreate



    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP","language arabic" + sharedPreferences);


        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void setToolBarConfig()
    {
        ToolBarConfig toolBarConfig = new ToolBarConfig(this,"none");
        toolBarConfig.setTitle(getString(R.string.Review));
          toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    private void initRecyclerView() {

        adapter = new ReviewAdapter(this,reviewList);
        layoutManager = new LinearLayoutManager(this);
        rate_recycler_view.setHasFixedSize(true);
        rate_recycler_view.setLayoutManager(layoutManager);
        rate_recycler_view.setAdapter(adapter);

    } // initialize recycler view


    private  void getIntentId()
    {
        Intent intent = getIntent();
        if(intent != null)
        {
            intentKitchenId = intent.getIntExtra("id",0);
        } // if
        Log.i("QP","id : "+intentKitchenId);
    } // git kitchenId

    private void takeRateFromUser()
    {
         rateBar_addRate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
             @Override
             public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                 userRate = v;
                 tx_addRate.setText(v+"");
             }
         });
    } // function of takeRateFromUser

    @OnClick(R.id.btn_review)
    public void addReview()
    {
         userReview = et_review.getText().toString().trim();
         if(userReview.equals("") || userRate == 0)
         {
             toastDialog.initDialog(getString(R.string.completeReview),ReviewActivity.this);

         } // rate or review empty
        else
         {
             addReviewAndRate(userRate,toolBarConfig.getPrefUserId(),intentKitchenId,userReview);
         }
    } // add review button


    private void addReviewAndRate(final float rate, final int userId , final int kitchenId, final String review)
    {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(ReviewActivity.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ReviewApi reviewApi = retrofit.create(ReviewApi.class);

                final Call<ReviewResponse> getInterestConnection = reviewApi.addReview(userId, kitchenId,rate,review);

                getInterestConnection.enqueue(new Callback<ReviewResponse>() {
                    @Override
                    public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                        try
                        {
                            String code = response.body().getCode();
                            Log.i("QP","code" + code);
                            response.body();
                            Log.i("QP","response"+ response.body().getCode());

                            if(response.body() != null) {
                                if (code.equals("200")) {
                                    toastDialog.initDialog(getString(R.string.reviewAdded),ReviewActivity.this);
                                    progress.dismiss();

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            selectAllKitchenReview(toolBarConfig.getPrefUserId(),intentKitchenId);
                                        }
                                    },2000);



                                } // review added successfuly

                            } // if response success
                            else
                            {
                                toastDialog.initDialog(getString(R.string.retry),ReviewActivity.this);

                            } // rsponse faild

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ReviewResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry),ReviewActivity.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of addReviewAndRate

    private void selectAllKitchenReview(final int userId , final int kitchenId)
    {

        Log.i("QP","userId : "+userId+" // kitchenId : "+kitchenId);

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(ReviewActivity.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final ReviewApi reviewApi = retrofit.create(ReviewApi.class);

                final Call<ReviewResponse> getInterestConnection = reviewApi.getReview(kitchenId, userId);

                getInterestConnection.enqueue(new Callback<ReviewResponse>() {
                    @Override
                    public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                        try
                        {
                            response.body();
                            String code = response.body().getCode();
                            Log.i("QP","code" + code);

                            Log.i("QP","response"+ response.body().getCode());

                            if(response.body() != null) {
                                if (code.equals("200")) {

                                     reviewListApi = response.body().getData();
                                     fillReviewList();
                                } // user add review to this kitchen before
                                else if (code.equals("1313")) {

                                    hideLayoutAddReview();
                                    reviewListApi = response.body().getData();
                                    fillReviewList();

                                } // user added review before
                            } // user didn't add review to this kitchen before
                            else
                            {
                                toastDialog.initDialog(getString(R.string.retry),ReviewActivity.this);

                            } // rsponse faild

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ReviewResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry),ReviewActivity.this);

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // function of selectAllKitchenReview

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void fillReviewList()
    {
        for (int i = 0 ; i <reviewListApi.size() ; i++)
        {
            if(reviewListApi.get(i).getUser_id() == toolBarConfig.getPrefUserId())
                reviewList.add(0,reviewListApi.get(i));
                else
                reviewList.add(reviewListApi.get(i));
        }
        adapter.notifyDataSetChanged();
        if(reviewListApi.size() <= 0)
        {
            toastDialog.initDialog(getString(R.string.notFoundRate),ReviewActivity.this);

        } // if not found review
    } // function of fillReviewList

    private void hideLayoutAddReview()
    {
        layoutAddReview.setVisibility(View.GONE);
         btn_review.setVisibility(View.GONE);
    } // function hide layout add review if user add review previously

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }
} // class of ReviewActivity
