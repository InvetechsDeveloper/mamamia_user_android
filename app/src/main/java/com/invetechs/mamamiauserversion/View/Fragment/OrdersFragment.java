package com.invetechs.mamamiauserversion.View.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Control.OrdersAdapter;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.OrdersApi.OrdersApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.OrdersResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OrdersFragment extends Fragment {

    // Bind views
    @BindView(R.id.rec_view_orders)
    RecyclerView rec_view_orders;

    // vars
    ArrayList<OrdersResponse.DataBean> ordersArrayList = new ArrayList<>();
    ArrayList<OrdersResponse.DataBean> newOrdersArrayList = new ArrayList<>();
    ArrayList<OrdersResponse.DataBean> nacceptedAndRejectedOrdersArrayList = new ArrayList<>();
    OrdersAdapter order_adapter;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    SharedPreferences sharedPreferences;
    int user_id = 0;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_fragment, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();
        checkLanguage();
        getOrders();
        getUserId();
        return view;
    }

    private void checkLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);

        }
    } // checkLanguage

    private void getUserId() {

        prefs = getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        user_id = prefs.getInt("id", 0);

    } // get the user id

    private void initRecyclerView() {

        order_adapter = new OrdersAdapter(getContext(), ordersArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rec_view_orders.setHasFixedSize(true);
        rec_view_orders.setLayoutManager(linearLayoutManager);
        rec_view_orders.setAdapter(order_adapter);

    } //initialize recycler view

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    private void getOrders() {
        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(getActivity());
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final OrdersApi ordersApi = retrofit.create(OrdersApi.class);

                final Call<OrdersResponse> getInterestConnection = ordersApi.showOrders(user_id);

                getInterestConnection.enqueue(new Callback<OrdersResponse>() {
                    @Override
                    public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);
                            response.body();
                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                nacceptedAndRejectedOrdersArrayList = (ArrayList<OrdersResponse.DataBean>) response.body().getData();
                                newOrdersArrayList = (ArrayList<OrdersResponse.DataBean>) response.body().getNewOrders();

                                 fillListWithOrders();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<OrdersResponse
                            > call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();

    } //  function of getOrders from Api

    private void fillListWithOrders()
    {
        if(ordersArrayList.size() > 0)
        ordersArrayList.clear();

        for(int i=0;i<newOrdersArrayList.size();i++)
        {
           ordersArrayList.add(newOrdersArrayList.get(i));
        }
        for(int i=0;i<nacceptedAndRejectedOrdersArrayList.size();i++)
        {
            ordersArrayList.add(nacceptedAndRejectedOrdersArrayList.get(i));
        }
        order_adapter.notifyDataSetChanged();
    } // function of fillListWithOrders

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    } // on destroy

} // class of OrderFraagment
