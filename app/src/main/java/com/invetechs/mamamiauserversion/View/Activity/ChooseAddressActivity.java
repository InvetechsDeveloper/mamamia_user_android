package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.ChooseAddressAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AdressesApi.AddNewAddressApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChooseAddressActivity extends Language {

    // bind views
    @BindView(R.id.recyclerView_chooseAddresses)
    RecyclerView recyclerView;

    // vars
    ChooseAddressAdapter adapter;
    List<AddressResponse.DataBean> addressListApi = new ArrayList<>();
    List<AddressResponse.DataBean> addressList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    Button okChooseAddressButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        ButterKnife.bind(this);
        initLanguage();
        okChooseAddressButton = findViewById(R.id.okChooseAddress);
        initRecycelerView(); // intialize reclerview
        getAddressesFromApi(); // get laa addresses of user
        toastDialog = new ShowDialog();
        toolBarConfig = new ToolBarConfig(this);
    } // function of onCreate


    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }


    } // initialize language and font

    private void initRecycelerView() {
        adapter = new ChooseAddressAdapter(this, addressList, okChooseAddressButton);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    } // intialize list of addresses

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getAddressesFromApi() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AddNewAddressApi addressApi = retrofit.create(AddNewAddressApi.class);

                final Call<AddressResponse> getInterestConnection = addressApi.selectUserAddresses(toolBarConfig.getPrefUserId());

                getInterestConnection.enqueue(new Callback<AddressResponse>() {
                    @Override
                    public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                        try {
                            if (response.body() != null) {
                                String code = response.body().getCode();
                                Log.i("QP", "code" + code);

                                if (code.equals("200")) {

                                    addressListApi = response.body().getData();
                                    fillLsitWithAddresses();
                                } //

                            } // if response success
                            else {
                                toastDialog.initDialog(getString(R.string.retry), ChooseAddressActivity.this);
                            } // response faild
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<AddressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), ChooseAddressActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    }//function of getAddressesFromApi

    private void fillLsitWithAddresses() {
        for (int i = 0; i < addressListApi.size(); i++) {
            addressList.add(addressListApi.get(i));
        }
        adapter.notifyDataSetChanged();
    } // function of fillListWithAddresses

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }
} // class of ChooseAddressActivity
