package com.invetechs.mamamiauserversion.View.Fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.CartAdapter;
import com.invetechs.mamamiauserversion.Model.CartApiModel;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.CartApi.CheckOutApi;
import com.invetechs.mamamiauserversion.Retrofit.Request.CartApi.GetUserPointDiscountApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.UserPointResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.View.Activity.AddAddressActivity;
import com.invetechs.mamamiauserversion.View.Activity.ChooseAddressActivity;
import com.invetechs.mamamiauserversion.View.Activity.MenuActivity;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;


public class CartFragment extends Fragment {

    // Bind views
    @BindView(R.id.cash_linear)
    LinearLayout cashlinear;

    @BindView(R.id.btn_cash)
    Button btnCash;

    @BindView(R.id.btn_mada)
    Button btnMada;

    @BindView(R.id.cash_recycler_view)
    RecyclerView cash_recycler_view;

    @BindView(R.id.txt_totalBill)
    TextView txt_totalBill;

    @BindView(R.id.txt_grandTotal)
    TextView txt_grandTotal;

    @BindView(R.id.txt_userPoints)
    TextView txt_userPoints;

    @BindView(R.id.txt_deliveryPrice)
    TextView txt_deliveryPrice;

    @BindView(R.id.txt_address)
    TextView txt_address;

    @BindView(R.id.ch_userPoints)
    CheckBox ch_userPoints;

    @BindView(R.id.address_title)
    TextView address_title;

    @BindView(R.id.payment_title)
    TextView payment_title;

    @BindView(R.id.btn_checkOut)
    Button btn_checkOut;

    @BindView(R.id.bill_total)
    TextView bill_total;

    @BindView(R.id.dilev)
    TextView dilev;

    @BindView(R.id.grand_total)
    TextView grand_total;

    @BindView(R.id.name_card)
    TextView name_card;

    @BindView(R.id.edt_userPoints)
    EditText edt_userPoints;

    @BindView(R.id.txt_vat)
    TextView txt_vat;

    @BindView(R.id.vat_title)
    TextView vat_title;

    @BindView(R.id.txt_discount)
    TextView txt_discount;


//    @BindView(R.id.btn_address)
//    Button btn_address;

    // vars

    double totalBill;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    ToolBarConfig toolBarConfig;
    double discountPerPoint = 0;
    public static double pointDiscount = 0;
    int kitchenId = 0, deliveryMaxBill = 0, point = 0;
    float distanceToUser = 0f, totalDelivery = 0f, delivery = 0f;
    double grandTotal = 0, discount = 0, total = 0, minCharge = 0, vat = 0, totalAfterUsePoints = 0;
    String address = "";
    double lat = 0, lng = 0, totalAfterVat;
    int pointsUserEntered = 0;
    ArrayList<CartApiModel.Meals> mealList = new ArrayList<CartApiModel.Meals>();

    CartApiModel cartApiModel;
    DecimalFormat df = new DecimalFormat("0.00");
    String textTotalBill = "", textGrandTotal = "";
    SharedPreferences preferences;
    List<CartModel> cartArrayListCash = new ArrayList<>();
    LinearLayoutManager layoutManager;
    CartAdapter cartAdapter;
    SharedPreferences sharedPreferences;
    List<CartModel> cartList = new ArrayList<>();
    int checkPoint = 0, usedPoint = 0, clickCheckButton = 0;
    double lastVat, lastTotal;
    LocationManager locationManager;
    double latitude = 0; // latitude
    double longitude = 0; // longitude
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editorCart;
    double prefLat, prefLng;
    SharedPreferences prefs;
    Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment, container, false);
        ButterKnife.bind(this, view);
        pointDiscount = 0;
        Log.i("QT", "on Create testCase : " + pointDiscount);
        toolBarConfig = new ToolBarConfig(getActivity(), "none");
        initLanguage();
        setCashDefault();
        getFinalCartData();
        getKitchenUserChoose();
        Log.i("C", "testTest : " + deliveryMaxBill);
        getAddress();

        if (kitchenId != 0)
            getUserPointDiscount();
        userCheckHisPoints();

        Log.i("QCART", "cart : \n" + "kitchenId : " + kitchenId + "\n" + "deliveryMaxBill : " + deliveryMaxBill
                + "\n" + "totalDelivery : " + totalDelivery + "\n" + "distanceToUser : " + distanceToUser);


        return view;
    } //  function of onCreateView

    @OnTextChanged(R.id.edt_userPoints)
    public void enterUserPoint() {

        String s = txt_totalBill.getText().toString();
        String lastTextTotal = s.replace(getString(R.string.sr), "").trim();

        String v = txt_vat.getText().toString();
        String lastTextVat = v.replace(getString(R.string.sr), "").trim();
        lastVat = 0;
        try {
            lastVat = Double.parseDouble(String.valueOf(df.parse(lastTextVat)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("QP", "crash : " + lastTextTotal);
        lastTotal = 0;
        try {
            lastTotal = Double.parseDouble(String.valueOf(df.parse(lastTextTotal)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        grandTotal = ((lastTotal + delivery + lastVat));

        if (!edt_userPoints.getText().toString().isEmpty()) {
            pointsUserEntered = Integer.parseInt(edt_userPoints.getText().toString());

            if (pointsUserEntered <= 0) {
                toastDialog.initDialog(getString(R.string.invaildPoints), getContext());
                txt_discount.setVisibility(View.INVISIBLE);
                pointDiscount = 0;
            } // point exceed
            else if (pointsUserEntered > point) {
                toastDialog.initDialog(getString(R.string.exceedPoints), getContext());
                txt_discount.setVisibility(View.INVISIBLE);
                pointDiscount = 0;
                txt_grandTotal.setText("0" + " " + getString(R.string.sr));
            }// inVaild Point

            else {
                pointDiscount = pointsUserEntered * discountPerPoint;
                txt_discount.setText(getString(R.string.discount) + " : " + pointDiscount);
                txt_discount.setVisibility(View.VISIBLE);

                if (grandTotal > pointDiscount) {

                    totalAfterUsePoints = grandTotal - pointDiscount;
                    txt_grandTotal.setText(df.format(totalAfterUsePoints) + " " + getString(R.string.sr));
                } else
                    txt_grandTotal.setText("0" + " " + getString(R.string.sr));

            }
        } else {
            pointsUserEntered = 0;
            pointDiscount = 0;
            txt_discount.setVisibility(View.INVISIBLE);

            txt_grandTotal.setText(df.format(grandTotal) + " " + getString(R.string.sr));

        }
        Log.e("WE", "lastTotal : " + lastTotal + "\nlastVat : " + lastVat + "\ndelivery : " + delivery + "\nDiscount : " + discount
                + "\npointDiscount : " + pointDiscount);

        Log.i("QO", "change  grand : " + grandTotal + "\n" + "discount : " + pointDiscount + "\n");

    } // function of enterUserPoint

    private void setCashDefault() {
        btnCash.setTextColor(getResources().getColor(R.color.white));
        btnCash.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btnMada.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnMada.setBackgroundColor(getResources().getColor(R.color.white));
        cash_recycler_view.setVisibility(View.VISIBLE);
    } // set button cash to default

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            address_title.setTypeface(font);
            payment_title.setTypeface(font);
            btnCash.setTypeface(font);
            btnMada.setTypeface(font);
            bill_total.setTypeface(font);
            txt_totalBill.setTypeface(font);
            txt_vat.setTypeface(font);
            dilev.setTypeface(font);
            txt_deliveryPrice.setTypeface(font);
            ch_userPoints.setTypeface(font);
            txt_userPoints.setTypeface(font);
            grand_total.setTypeface(font);
            txt_grandTotal.setTypeface(font);
            btn_checkOut.setTypeface(font);
            vat_title.setTypeface(font);
            txt_vat.setTypeface(font);
            edt_userPoints.setTypeface(font);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            address_title.setTypeface(font);
            payment_title.setTypeface(font);
            btnCash.setTypeface(font);
            btnMada.setTypeface(font);
            bill_total.setTypeface(font);
            txt_totalBill.setTypeface(font);
            dilev.setTypeface(font);
            txt_deliveryPrice.setTypeface(font);
            ch_userPoints.setTypeface(font);
            txt_userPoints.setTypeface(font);
            grand_total.setTypeface(font);
            txt_grandTotal.setTypeface(font);
            btn_checkOut.setTypeface(font);
            vat_title.setTypeface(font);
            txt_vat.setTypeface(font);
            edt_userPoints.setTypeface(font);

        }

    } // init language

    private void setData() {
        Log.i("QCART", "setData : totalBill : " + totalBill);
        if (totalBill >= deliveryMaxBill) {
            if (totalBill == 0)
                txt_deliveryPrice.setText("");
            else
                txt_deliveryPrice.setText(getString(R.string.deliveryFree));
            delivery = 0;
            Log.i("QCART", "freeeeeeeeeeeeeee");
        } // delivery for this order free
        else {
            if (totalBill == 0)
                txt_deliveryPrice.setText("");
            else
                txt_deliveryPrice.setText(df.format(totalDelivery) + " " + getString(R.string.sr));
            delivery = totalDelivery;
            Log.i("QCART", "Not freeeeeeeeeeeeeee");

        } // delivery fro this order not free

        txt_totalBill.setText(df.format(totalBill) + " " + getString(R.string.sr));
        totalAfterVat = totalBill + totalBill * vat;

        txt_vat.setText(df.format(totalBill * vat) + " " + getString(R.string.sr));

        Log.i("QCART", "print vat = " + df.format(totalBill * vat) + " " + getString(R.string.sr));

        grandTotal = totalAfterVat + delivery;

        if (grandTotal > discount)
            txt_grandTotal.setText(df.format(grandTotal) + " " + getString(R.string.sr));
        else
            txt_grandTotal.setText("0" + " " + getString(R.string.sr));

    } // function of setdata


    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


//    @OnClick(R.id.btn_address)
//    public void chooseAddress() {
//
//        dialog = new Dialog(getContext());
//        dialog.setContentView(R.layout.address_dialog);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//        dialog.getWindow().setLayout((int) (getScreenWidth((Activity) getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
//        Button btn_address_dialog = dialog.findViewById(R.id.btn_address_dialog);
//        Button btn_saved_places = dialog.findViewById(R.id.btn_saved_places);
//        Button btn_add_new_address = dialog.findViewById(R.id.btn_add_new_address);
//        btn_address_dialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                getCurrentLocation();
//                try {
//                    editorCart = getContext().getSharedPreferences("EditorCart", MODE_PRIVATE).edit();
//                    editorCart.putString("address", address);
//                    editorCart.putFloat("lat", (float) latitude);
//                    editorCart.putFloat("lng", (float) longitude);
//                    editorCart.apply();
//
//                } catch (Exception e) {
//                    Log.i("QP", "Exception " + e.getMessage()); //
//
//                }
//
//            }
//        }); // button get current location and save lat and lng in shared preference
//
//        btn_saved_places.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                Intent chooseAddress = new Intent(getContext(), ChooseAddressActivity.class);
//                chooseAddress.putExtra("fragmentFlag", "cart");
//                startActivity(chooseAddress);
//            }
//        }); // button choose from saved places
//
//
//        btn_add_new_address.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent addNewAddress = new Intent(getContext(), AddAddressActivity.class);
//                addNewAddress.putExtra("flag", "cart");
//                startActivity(addNewAddress);
//            }
//        }); // button add new address
//
//        dialog.show();
//    }

//    private void getCurrentLocation() {
//
//        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
//        List<String> providers = locationManager.getProviders(true);
//        Location bestLocation = null;
//        for (String provider : providers) {
//            checkPermission();
//            Location l = locationManager.getLastKnownLocation(provider);
//            if (l == null) {
//                continue;
//            }
//            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
//                // Found best last known location: %s", l);
//                bestLocation = l;
//            }
//        }
//
//        if (bestLocation != null) {
//            latitude = bestLocation.getLatitude();
//            longitude = bestLocation.getLongitude();
//            if (prefLat != 0 && prefLng != 0) {
//                latitude = prefLat;
//                longitude = prefLng;
//            }
//
//            Log.i("QP", "latitude in get current location : " + latitude);
//            Log.i("QP", "longitude in get current location : " + longitude);
//
//            getAddressFromLatandLng(latitude, longitude);
//            Log.i("QP", "current Location : " + bestLocation);
//        } // bestlocation not equal null
//        else {
//            Log.i("QP", "current Location : Null"); //
//
//        } // location equal null
//    } // function of getCurrentLocation

//    private void getAddressFromLatandLng(double lat, double lng) {
//        Geocoder geocoder;
//        List<Address> addresses = null;
//        geocoder = new Geocoder(getContext(), Locale.getDefault());
//
//        try {
//            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned
//            address = addresses.get(0).getAddressLine(0);
//            editorCart = getContext().getSharedPreferences("EditorCart", MODE_PRIVATE).edit();
//            editorCart.putString("address", address);
//            editorCart.putFloat("lat", (float) lat);
//            editorCart.putFloat("lng", (float) lng);
//            editorCart.apply();
//            Log.i("QP", "address in getaddressfrom lat and lng : " + address);
//
//            btn_address.setText(address);
//
//        } catch (Exception e) {
//            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
//            return;
//        }
//
//      /*  String city = addresses.get(0).getLocality();
//        String state = addresses.get(0).getAdminArea();
//        String country = addresses.get(0).getCountryName();
//        String postalCode = addresses.get(0).getPostalCode();
//        String knownName = addresses.get(0).getFeatureName();
//*/
//
//
//    } // function of getAddressFromLatandLng

//    private void checkPermission() {
//        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Log.i("QP", "checkPermission");
//            return;
//        }
//    } //checkPermission


    @Override
    public void onPause() {
        super.onPause();
        try {
            dialog.dismiss();

        } catch (Exception e) {
            Log.i("QP", "Exception" + e.getMessage());
        }
    }

    private void getAddress() {
        SharedPreferences prefs = getContext().getSharedPreferences("AddressPref", MODE_PRIVATE);
        address = prefs.getString("address", "");
        Log.i("QP", "cart Address : " + address);
        txt_address.setText(address);
        lat = prefs.getFloat("lat", 0);
        lng = prefs.getFloat("lng", 0);
        Log.i("QCART", "address : " + address + " : lat : " + lat + " : lng : " + lng);
    } // function of get Address

    private void userCheckHisPoints() {
        ch_userPoints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.i("QCART", "check : " + b);

                String s = txt_totalBill.getText().toString();
                String lastTextTotal = s.replace(getString(R.string.sr), "").trim();
                String v = txt_vat.getText().toString();
                String lastTextVat = v.replace(getString(R.string.sr), "").trim();
                lastVat = 0;
                try {
                    lastVat = Double.parseDouble(String.valueOf(df.parse(lastTextVat)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("QP", "crash : " + lastTextTotal);
                lastTotal = 0;
                try {
                    lastTotal = Double.parseDouble(String.valueOf(df.parse(lastTextTotal)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                edt_userPoints.setEnabled(b);
                txt_discount.setText(getString(R.string.discount) + " : " + pointDiscount);

                Log.i("QCART", "replace : " + lastTotal);
                if (b == true) {
                    Log.i("QT", "testCase : " + pointDiscount);
                    checkPoint = 1;
                    discount = pointDiscount;
                    if (pointDiscount > 0)
                        txt_discount.setVisibility(View.VISIBLE);
                    else
                        txt_discount.setVisibility(View.INVISIBLE);
                } else if (b == false) {
                    checkPoint = 0;
                    discount = 0;
                    pointDiscount = 0;
                    txt_discount.setVisibility(View.INVISIBLE);
                    edt_userPoints.setText("");
                }

                if (txt_deliveryPrice.getText().toString().equals(getString(R.string.deliveryFree)))
                    delivery = 0;
                else delivery = totalDelivery;

                grandTotal = ((lastTotal + delivery + lastVat) - discount);

                Log.i("QQ", "lastTotal : " + lastTotal);
                Log.i("QQ", "delivery : " + delivery);
                Log.i("QQ", "lastVat : " + lastVat);
                Log.i("QQ", "discount : " + discount);
                Log.i("QQ", "grand : " + grandTotal);


//                Log.i("QP", "grand : " + grandTotal + "\n" +
//                        "discount : " + discount + "\n" + "pointDiscount : " + pointDiscount);

                if (grandTotal > discount)
                    txt_grandTotal.setText(df.format(grandTotal) + " " + getString(R.string.sr));
                else
                    txt_grandTotal.setText("0" + " " + getString(R.string.sr));
            }
        });
    } // function of userCheckHisPoints

    private void getFinalCartData() {

        if (mealList.size() > 0) mealList.clear();
        total = 0;

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("MyObject", "");
        if (json != null) {
            Type type = new TypeToken<List<CartModel>>() {
            }.getType();
            cartList = gson.fromJson(json, type);
            if (cartList == null) cartList = new ArrayList<>();
        }

        for (int i = 0; i < cartList.size(); i++) {
            CartApiModel.Meals meals = new CartApiModel.Meals(cartList.get(i).getId(),
                    cartList.get(i).getAmount(), cartList.get(i).getNotes(), cartList.get(i).getAdditions());

            mealList.add(meals);

            Log.i("QCART", "get mealllllls : " + mealList.get(0));
            Log.i("QCART", "get ID number : " + cartList.get(i).getId());
            Log.i("QCART", "get amount number : " + cartList.get(i).getAmount());
            Log.i("QCART", "get notes number : " + cartList.get(i).getNotes());
            Log.i("QCART", "get additions number : " + cartList.get(i).getAdditions());
        }
    } // function of getCartData

    @OnClick(R.id.btn_cash)
    public void cashClick() {
        btnCash.setTextColor(getResources().getColor(R.color.white));
        btnCash.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btnMada.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnMada.setBackgroundColor(getResources().getColor(R.color.white));
        cash_recycler_view.setVisibility(View.VISIBLE);
    } // on cash click

    @OnClick(R.id.btn_mada)
    public void madaClick() {
        btnMada.setTextColor(getResources().getColor(R.color.white));
        btnMada.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btnCash.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnCash.setBackgroundColor(getResources().getColor(R.color.white));
    } // on mada click

    private void getKitchenUserChoose() {
        SharedPreferences prefs;
        prefs = getContext().getSharedPreferences("cart", MODE_PRIVATE);
        if (prefs != null) {
            kitchenId = prefs.getInt("kitchenId", 0);
            deliveryMaxBill = prefs.getInt("maxbill", 0);
            totalDelivery = prefs.getFloat("deliveryPerKilo", 0f);
            distanceToUser = prefs.getFloat("distanceToUser", 0f);
        }
    } // get data of kitchen user choose to make order

    private void initCashRecyclerView() {
        layoutManager = new LinearLayoutManager(getContext());
        cartAdapter = new CartAdapter(getContext(), cartArrayListCash, txt_totalBill, txt_vat, txt_grandTotal
                , ch_userPoints, totalDelivery, deliveryMaxBill, txt_deliveryPrice, txt_userPoints, vat);
        cash_recycler_view.setHasFixedSize(false);
        cash_recycler_view.setLayoutManager(layoutManager);
        cash_recycler_view.setAdapter(cartAdapter);
    }

    private void fillCash() {
        if (cartArrayListCash.size() > 0)
            cartArrayListCash.clear();

        if (cartList.size() > 0) {
            for (int i = 0; i < cartList.size(); i++) {
                cartArrayListCash.add(cartList.get(i));
                totalBill = totalBill + (cartList.get(i).getPrice() * cartList.get(i).getAmount());
                cartArrayListCash.get(i).setTotal(totalBill);
                Log.i("QCART", "totalBill : " + cartArrayListCash.get(i).getTotal());

            }
            cartAdapter.notifyDataSetChanged();

        } // if cart bot empty

    } // function of fill cash

    public int getPrefUserId() {
        int userId = 0;
        try {
            preferences = getContext().getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
            if (preferences != null)
                userId = preferences.getInt("id", 0);
            Log.i("QP", "profile id : " + userId);

        } catch (Exception e) {
            Log.i("QP", "Ecxeption : " + e.getMessage());
        }

        return userId;

    } // function of getPrefUserId

    private void getUserPointDiscount() {
        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final GetUserPointDiscountApi getUserPointDiscountApi = retrofit.create(GetUserPointDiscountApi.class);

                final Call<UserPointResponse> getInterestConnection = getUserPointDiscountApi.getUserDiscount(getPrefUserId(), kitchenId);

                getInterestConnection.enqueue(new Callback<UserPointResponse>() {
                    @Override
                    public void onResponse(Call<UserPointResponse> call, Response<UserPointResponse> response) {
                        try {
                            if (response.body() != null) {
                                String code = response.body().getCode();
                                Log.i("QCART", "code" + code);

                                if (code.equals("200")) {


                                    point = Integer.parseInt(response.body().getPoint());
                                    txt_userPoints.setText(point + " " + getString(R.string.point));
                                    minCharge = response.body().getMinCharge();
                                    vat = response.body().getVat() / 100;
                                    Log.i("QP", "price vat : " + response.body().getVat());
                                    discountPerPoint = response.body().getDiscountPerPoint();


                                } // user have points in this kitchen
                                else if (code.equals("1313")) {
                                    point = 0;
                                    discountPerPoint = 0;
                                    txt_userPoints.setText(point + " " + getString(R.string.point));
                                    minCharge = response.body().getMinCharge();
                                    vat = response.body().getVat() / 100;

                                } // user haven't point in this kitchen

                                initCashRecyclerView();
                                fillCash();
                                setData();
                            } // if response success
                            else {
                                toastDialog.initDialog(getString(R.string.retry), getActivity());
                            } // response faild
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QCART", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<UserPointResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QCART", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of getUserPointDiscount

    private boolean checkPoints() {

        if (ch_userPoints.isChecked() && edt_userPoints.getText().toString().isEmpty()) {
            toastDialog.initDialog(getString(R.string.invaildPoints), getContext());
            return false;
        }
//        else if (btn_address.getText().toString().equals(getString(R.string.choose_address)) || btn_address.getText().toString().isEmpty()) {
//            Log.i("QP", "btn address : : " + btn_address.getText().equals(getString(R.string.choose_address)));
//
//            toastDialog.initDialog(getString(R.string.choose_address), getContext());
//            return false;
//        }

        return true;

    }

    @OnClick(R.id.btn_checkOut)
    public void buttonCheckOut() {

        if (!edt_userPoints.getText().toString().isEmpty())
            clickCheckButton = Integer.valueOf(edt_userPoints.getText().toString());


        if (!checkPoints()) {
            return;
        }

        if (clickCheckButton > point) {
            toastDialog.initDialog(getString(R.string.invaildPoints), getContext());
        } else {
            getFinalCartData();

            textTotalBill = txt_totalBill.getText().toString().replace(getString(R.string.sr), "");
            textGrandTotal = txt_grandTotal.getText().toString().replace(getString(R.string.sr), "");

            try {
                totalBill = Double.parseDouble(String.valueOf(df.parse(textTotalBill)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.i("QP", "total : " + totalBill + " : min : " + minCharge + " : " + grandTotal);

            if (totalBill < minCharge) {
                if (totalBill == 0) {
                    toastDialog.initDialog(getString(R.string.cartEmpty), getContext());

                } else {
                    toastDialog.initDialog(getString(R.string.checkMinCharge) + " " + minCharge, getContext());

                } // total bill fewer than minCharhge

            } // total bill fewer than minCharhge

            else {

                try {
                    grandTotal = Double.parseDouble(String.valueOf(df.parse(textGrandTotal)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (txt_deliveryPrice.getText().toString().equals(getString(R.string.deliveryFree)))
                    delivery = 0;
                else
                    delivery = totalDelivery;

                if (!edt_userPoints.getText().toString().equals(""))
                    usedPoint = Integer.parseInt(edt_userPoints.getText().toString());

                Log.i("QCART", "lat cart api : " + latitude);
                Log.i("QCART", "lng cart api: " + longitude);
                Log.i("QCART", "address cart api: " + address);

                cartApiModel = new CartApiModel();
                cartApiModel.setUser_id(toolBarConfig.getPrefUserId());
                cartApiModel.setKitchen_id(kitchenId);
                cartApiModel.setLat(lat);
                cartApiModel.setLng(lng);
                cartApiModel.setUserplace(address);
                cartApiModel.setDistancetouser(distanceToUser);
                cartApiModel.setDeliverytotalprice(delivery);
                cartApiModel.setTotalprice(grandTotal);
                cartApiModel.setBillTotal(totalBill);
                cartApiModel.setMeals(mealList);
                cartApiModel.setCheckuserpoint(checkPoint);
                cartApiModel.setDiscountpoint(pointDiscount);
                cartApiModel.setPoint(usedPoint);
                cartApiModel.setPriceVat(vat * totalBill);


                Log.i("QCART", "new lat : " + cartApiModel.getLat());
                Log.i("QCART", "new lng : " + cartApiModel.getLng());
                Log.i("QCART", "new address : " + cartApiModel.getUserplace());


                Log.i("QCART", "priceVat : " + vat * totalBill);


                Log.i("QCART", "--- CartModel ---\n"
                        + "kitchenId : " + cartApiModel.getKitchen_id() + "\n"
                        + "lat : " + cartApiModel.getLat() + "\n"
                        + "lng : " + cartApiModel.getLng() + "\n"
                        + "address : " + cartApiModel.getUserplace() + "\n"
                        + "distanceToUser : " + cartApiModel.getDistancetouser() + "\n"
                        + "totalDelivery : " + cartApiModel.getDeliverytotalprice() + "\n"
                        + "totalPrice : " + cartApiModel.getTotalprice() + "\n"
                        + "totalBill : " + cartApiModel.getBillTotal() + "\n"
                        + "mealList : " + cartApiModel.getMeals().size() + "\n"
                        + "discount : " + cartApiModel.getDiscountpoint() + "\n"
                        + "checkPoint : " + cartApiModel.getCheckuserpoint() + "\n"
                        + "point : " + cartApiModel.getPoint() + "\n"
                        + "priceVat : " + cartApiModel.getPriceVat() + "\n");

                if (mealList.size() > 0) {

                    if (grandTotal != 0)
                        checkOutApi();
                    else if (grandTotal == 0 && clickCheckButton < point)
                        checkOutApi();
                    else
                        toastDialog.initDialog(getString(R.string.invaildPoints), getContext());
                } else {
                    toastDialog.initDialog(getString(R.string.cartEmpty), getActivity());
                }
            } // total bill exceed minCharge or equal
        } // else
    } // function of button check out

    @SuppressLint("HandlerLeak")
    private void checkOutApi()
    {
        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final CheckOutApi checkOutApi = retrofit.create(CheckOutApi.class);
                final Call<CartApiModel> getInterestConnection = checkOutApi.checKOut(cartApiModel);

                Log.i("QCART", "cartApiModel" + cartApiModel);
                Log.i("QCART", "cartApiModel meals" + cartApiModel.getMeals().size());
                Log.i("QCART", "cartApiModel" + cartApiModel.getUser_id());


                getInterestConnection.enqueue(new Callback<CartApiModel>() {
                    @Override
                    public void onResponse(Call<CartApiModel> call, Response<CartApiModel> response) {
                        try {

                            response.body();
                            String code = response.body().getCode();
                            Log.i("QCART", "code" + code);

                            if (code.equals("200")) {

                                toolBarConfig.clearCart();
                                toastDialog.initDialog(getString(R.string.orderSubmited), getContext());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(getActivity(), MenuActivity.class);
                                        intent.putExtra("fragmentFlag", "home");
                                        getContext().startActivity(intent);
                                    }
                                }, 2000);

                            } // checkOut success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QCART", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<CartApiModel> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QCART", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    }  // function of checkOutApi

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();

    }

} // calss of cartFragment
