package com.invetechs.mamamiauserversion.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.invetechs.mamamiauserversion.Config.CheckUserLogin;
import com.bumptech.glide.Glide;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.CustomTypefaceSpan;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Language.MyContextWrapper;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.LoginResponse;
import com.invetechs.mamamiauserversion.View.Fragment.AddressesFragment;
import com.invetechs.mamamiauserversion.View.Fragment.CartFragment;
import com.invetechs.mamamiauserversion.View.Fragment.ContactFragment;
import com.invetechs.mamamiauserversion.View.Fragment.HomeFragment;
import com.invetechs.mamamiauserversion.View.Fragment.InvoiceFragment;
import com.invetechs.mamamiauserversion.View.Fragment.LanguageFragment;
import com.invetechs.mamamiauserversion.View.Fragment.MyBanquetFragment;
import com.invetechs.mamamiauserversion.View.Fragment.OrdersFragment;
import com.invetechs.mamamiauserversion.View.Fragment.PointsFragment;
import com.invetechs.mamamiauserversion.View.Fragment.ProfileFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class MenuActivity extends Language implements NavigationView.OnNavigationItemSelectedListener {

    // Bind Views
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.nav_view)
    NavigationView nav_view;


    // vars
    ToolBarConfig toolBarConfig;
    SharedPreferences preferences;
    SharedPreferences prefs;
    LoginResponse.UserdataBean user;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public CircleImageView img_profile;
    String user_image;
    public TextView tvUserName;
    String user_name = "";
    Double lat, lng;
    CheckUserLogin checkUserLogin;
    ShowDialog toastDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        checkLanguage();
        initNavigationMenu(savedInstanceState); // navigation menu
        setViewData();
        checkUserLogin = new CheckUserLogin();
        setToolBarConfig();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String isLogin = prefs.getString("isLogin", "false");
        Log.i("QP", "profile id" + isLogin);

        if (isLogin.equals("true")) {
            Menu menuNav = nav_view.getMenu();
            MenuItem logoutItem = menuNav.findItem(R.id.nav_login);
            logoutItem.setTitle(getString(R.string.logout));
            applyFontToMenuItem(logoutItem);
        }

        //  setViewData();

    } // onCreate

//    private void getCartData() {
//
//        String cart = getIntent().getStringExtra("flag");
//
//        Log.i("QP", "flag : " + cart);
//
//        if (cart != null) {
//            if (cart.equals("cart")) {
//                Log.i("QP", "replace with cart true : " + cart);
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).commit();
//                tv_title.setText(getString(R.string.cart));
//                tv_title.setTextColor(getResources().getColor(R.color.white));
//                Log.i("QP", "replacing : " + getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).commit());
//
//            } else {
//                Log.i("QP", "replace with cart failed : " + cart);
//
//            }
//
//        }
//    }

    private void setToolBarConfig() {

        toolBarConfig = new ToolBarConfig(this, "none");

        String fragmentFlag = getIntent().getStringExtra("fragmentFlag");
        if (fragmentFlag != null)
//            if (fragmentFlag.equals("cart")) {
//                toolBarConfig.setTitle(getString(R.string.cart));
//            }
            toolBarConfig.setTitle(getString(R.string.home));
        toolBarConfig.initCartBadge();
        toolBarConfig.addNotification();
        Log.i("QW", "menu activity notification");

        toolBarConfig.inVisibleBackButton();
    } // function of setToolBarConfig

    private void logOut() {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        if (editor != null) {
            editor.remove("id");
            editor.remove("name");
            editor.remove("email");
            editor.remove("mobilenumber");
            editor.remove("image");
            editor.remove("password");
            editor.putString("isLogin", "false");
            editor.apply();
        }

        SharedPreferences preferences = getSharedPreferences("cart", Context.MODE_PRIVATE);

        if (preferences != null) {
            preferences.edit().remove("badge").commit();
        }

        toolBarConfig.clearCart();

    } // function of logOut

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void checkLanguage() {

        preferences = getSharedPreferences("user", MODE_PRIVATE);

        if (preferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (preferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);

        }
    } // function of checkLanguage

    private void setViewData() {
        prefs = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        user_image = prefs.getString("profile_image", "");
        user_name = prefs.getString("name", "");

        if (tvUserName != null || img_profile != null) {

            String name = "";

            for (int i = 0; i < 10; i++) {

                if (user_name != null)
                    if (user_name.length() > 10) {
                        if (i == 9)
                            name = name + "...";
                        else
                            name = name + user_name.charAt(i);
                    } else if (user_name.length() <= 10) {
                        name = user_name;

                    }
            }

            preferences = getSharedPreferences("user", MODE_PRIVATE);
            tvUserName.setText(name);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.pizza_back);
            requestOptions.error(R.drawable.pizza_back);

            if (!user_image.equals(null))
                Glide.with(this)
                        .setDefaultRequestOptions(requestOptions)
                        .load("https://mammamiaa.com/cpanel/upload/user/" + user_image)
                        .into(img_profile);
        }
        Log.i("QW", "get image menu" + img_profile);
        Log.i("QW", "image menu" + user_image);
        Log.i("QW", "name menu" + tvUserName);
        Log.i("QW", "get image menu" + img_profile);
        Log.i("QW", "image menu" + user_image);
        Log.i("QW", "name menu" + tvUserName);

    } // set view data from api

    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar")));
    } // attachBaseContext


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();

        Log.i("QQ", "Destroy11");
    }

    @Override
    public void onBackPressed() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        nav_view.setCheckedItem(R.id.home);
        tv_title.setText(getString(R.string.home));
        tv_title.setTextColor(getResources().getColor(R.color.white));
        setViewData();
    }// onBackPressed

    private void initNavigationMenu(Bundle savedInstanceState) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, tool_bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(this);

        String flagFragment = getIntent().getStringExtra("fragmentFlag");

        Log.i("QP", "fragmentFlag : " + flagFragment);

        if (flagFragment != null) {
            if (flagFragment.equals("cart")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).commit();
                tv_title.setText(getString(R.string.cart));
                tv_title.setTextColor(getResources().getColor(R.color.white));
                Log.i("QP", "replacing : " + getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).commit());

                setViewData();
            }

            if (flagFragment.equals("home")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                nav_view.setCheckedItem(R.id.home);
                tv_title.setText(getString(R.string.home));
                tv_title.setTextColor(getResources().getColor(R.color.white));
                setViewData();
            }
        } else {
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                nav_view.setCheckedItem(R.id.home);
                tv_title.setText(getString(R.string.home));
                tv_title.setTextColor(getResources().getColor(R.color.white));
                setViewData();

            }
        } // else fragmentFlag empty  open home fragment

        NavigationView navView = findViewById(R.id.nav_view);
        View hView = nav_view.inflateHeaderView(R.layout.nav_header);
        tvUserName = hView.findViewById(R.id.tvUserName);
        img_profile = hView.findViewById(R.id.menu_img_profile);
        setViewData();

        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);

                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

    } // initialize menu

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                tv_title.setText(getString(R.string.home));
                tv_title.setTextColor(getResources().getColor(R.color.white));
                setViewData();
                break;

            case R.id.nav_account:
                try {
                    if (checkUserLogin.check(this)) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
                        tv_title.setText(getString(R.string.account));
                        tv_title.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        checkUserLogin.openLoginActivity(this);
                    }
                } catch (Exception e) {
                    Log.i("QP", "error : " + e.toString());
                }
                setViewData();
                break;
            case R.id.nav_invoice:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new InvoiceFragment()).commit();
                    tv_title.setText(getString(R.string.invoice));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                setViewData();
                break;
            case R.id.nav_orders:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrdersFragment()).commit();
                    tv_title.setText(getString(R.string.orders));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                setViewData();
                break;

            case R.id.nav_cart:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).commit();
                    tv_title.setText(getString(R.string.cart));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                setViewData();
                break;

            case R.id.nav_my_banquet:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MyBanquetFragment()).commit();
                    tv_title.setText(getString(R.string.my_banquet));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                    setViewData();
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                break;
            case R.id.nav_points:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PointsFragment()).commit();
                    tv_title.setText(getString(R.string.points));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                    setViewData();
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                break;

            case R.id.nav_addresses:
                if (checkUserLogin.check(this)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddressesFragment()).commit();
                    tv_title.setText(getString(R.string.addresses));
                    tv_title.setTextColor(getResources().getColor(R.color.white));
                    setViewData();
                } else {
                    checkUserLogin.openLoginActivity(this);
                }
                break;

            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactFragment()).commit();
                tv_title.setText(getString(R.string.contact_us));
                setViewData();
                break;

            case R.id.nav_rate_us:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.youtube"));
                startActivity(intent);
                setViewData();
                break;

            case R.id.nav_share:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                String shareBodyText = "Your shearing message goes here";
                share.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
                share.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(share, "Choose sharing method"));
                break;

            case R.id.nav_language:
                tv_title.setTextColor(getResources().getColor(R.color.white));
                tv_title.setText(getString(R.string.language));
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LanguageFragment()).commit();
                setViewData();
                break;

            case R.id.nav_login:

                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String isLogin = prefs.getString("isLogin", "false");
                Log.i("QP", "profile id" + isLogin);

                if (isLogin.equals("true")) {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    logOut();
                } else {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    //   toastDialog.initDialog(getString(R.string.logOutSuccessfully),this);
                }

                break;
        }

        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    } // function of onNavigationItemSelected

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/GESSTwoMedium.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    } // function font


} // class of menu acticity
