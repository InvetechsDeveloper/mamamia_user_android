package com.invetechs.mamamiauserversion.View.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.AdressesApi.AddNewAddressApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.AddressResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;
import com.invetechs.mamamiauserversion.Config.ShowDialog;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddAddressActivity extends Language implements OnMapReadyCallback {

    // bind views

    @BindView(R.id.tx_address)
    TextView tx_address;

    @BindView(R.id.ed_notes)
    EditText ed_notes;

    // vars
    LocationManager locationManager;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    Context mContext;
    GoogleMap map;
    String address = "", notes = "";
    boolean gpsEnabled = false;
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences.Editor editorAddress;
    double intentLat = 0, intentLng = 0;
    String intnentAddress = "", intentNote = "", intentFlag = "";
    int intentId = 0;
    GoogleMap googleMap;
    ShowDialog toastDialog;
    ToolBarConfig toolBarConfig;
    MarkerOptions markerOptions;
    String party_name = "";
    String banquetCompnent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        initLanguage();
        autoComplete();
        getDesignbanquetData();
        toastDialog = new ShowDialog();
        mContext = this;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                10, locationListenerGPS);

        getAddressIntent();

        markerOptions = new MarkerOptions();

        toolBarConfig = new ToolBarConfig(this);


    } // function of onCreate

    private void getDesignbanquetData() {
        Intent intent = getIntent();
        if (intent != null) {
            party_name = intent.getStringExtra("banquet_name");
            banquetCompnent = intent.getStringExtra("component");
            Log.i("QP", "party_name " + party_name);
            Log.i("QP", "banquetCompnent " + banquetCompnent);
        }
    }

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }

    } // initialize language and font

    private void getAddressIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            Log.i("QP", "cart address: " + intentFlag);

            intentFlag = intent.getStringExtra("flag");
            if (intentFlag.equals("add") || intentFlag.equals("update")) {
                intentLat = intent.getDoubleExtra("lat", 0);
                intentLng = intent.getDoubleExtra("lng", 0);
                intnentAddress = intent.getStringExtra("address");
                intentNote = intent.getStringExtra("note");
                intentId = intent.getIntExtra("id", 0);
            } else if (intentFlag.equals("designBanquet") || intentFlag.equals("new")) {
                intentLat = 0;
                intentLng = 0;
                intnentAddress = "";
                intentNote = "";
                intentId = 0;
            }

        }
    }

    private void callMap() {
        Log.i("QP", "map loaded");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    } // call map vire

    private void getAddressFromLatandLng(double lat, double lng) {
        Log.i("QP", "Gecode Here ");
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned
            address = addresses.get(0).getAddressLine(0);
            tx_address.setText(address);
            Log.i("QP", "address : try " + latitude + " : " + longitude + " : " + address + " : " + addresses);
        } catch (Exception e) {
            Log.i("QP", "address : exception " + latitude + " : " + longitude + " : " + address + " : " + e.toString());
            tx_address.setText(intnentAddress);
        }

        if (!intentNote.equals("0"))
            ed_notes.setText(intentNote);
        else
            ed_notes.setHint(getString(R.string.enterNote));


    } // function of getAddressFromLatandLng

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        Log.i("QP", "map ready : " + latitude + " : " + longitude);
        // Add a marker in current location
        // and move the map's camera to the same location.

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                // Clears the previously touched position
                googleMap.clear();

                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                googleMap.addMarker(markerOptions);

                longitude = latLng.longitude;
                latitude = latLng.latitude;

                getAddressFromLatandLng(latLng.latitude, latLng.longitude); // get address name
            }
        });
        googleMap.clear();
        if (latitude != 0 && longitude != 0) {
            LatLng currentLocation = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(currentLocation)
                    .title("Marker in myLocation"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

            zoomInCameraLocation(latitude, longitude, googleMap); // make zoom in location

            getAddressFromLatandLng(latitude, longitude); // get address name

        } // if location doesn't null

    } // view map

    private void zoomInCameraLocation(double latitude, double longitude, GoogleMap mMap) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    } // make zoom in location

    private void getLocation() {
        try {

            Log.i("QP", "locationManger : " + locationManager);

            if (ActivityCompat.checkSelfPermission(AddAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(AddAddressActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                Log.i("QP", "permission false");
                return;
            } else {
                // Write you code here if permission already given.
                Log.i("QP", "permission true");
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Log.i("QP", "Location : " + location.getLatitude());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                callMap();
            } // location not equal null
            else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    Log.i("QP", "Location : " + location.getLatitude());
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    callMap();
                } // location not equal null
                else {
                    Log.i("QP", "Location null on network and gps");
                }
            }

        } catch (SecurityException e) {
            Log.i("QP", "locationManger :exception " + e.toString());
        }
    } // function getlocation

    private void getLocationWithLatAndLng(double lat, double lng) {
        latitude = lat;
        longitude = lng;
        callMap();
    } // function of getLocationWithLatAndLng

    private void autoComplete() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("QP", "Place: " + place.getLatLng());
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                callMap();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("QP", "An error occurred: " + status);
            }
        });
    } // function of autoComplete


    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {

            if (intentLat != 0 && intentLng != 0) {
                latitude = intentLat;
                longitude = intentLng;
            } else {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
            Log.i("QP", "listner : ");
            callMap();
            String msg = "New Latitude: " + latitude + "New Longitude: " + longitude;

            //Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    protected void onResume() {
        super.onResume();
        if (gpsEnabled == false)
            isLocationEnabled("intent");
    }

    private void isLocationEnabled(final String flag) {

        gpsEnabled = true;
        Log.i("QP", "isLocationEnabled");

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.enableLocation));
            alertDialog.setMessage(getString(R.string.settingLocationMenu));
            alertDialog.setPositiveButton(getString(R.string.locationSetting), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);

                }
            });
            alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
          /*  AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.confirmLocation));
            alertDialog.setMessage(getString(R.string.yourLocationEnabledContinue));
            alertDialog.setNegativeButton(getString(R.string.conttinue), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {*/
            if (flag.equals("intent"))
                getLocationWithLatAndLng(intentLat, intentLng);
            else {
                getLocation();
            }
                   /* dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();*/
        }
    } // function check permission


    public void currentLocationButton(View view) {
        callMap();
        isLocationEnabled("current");
    } // function of current location button

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void saveNewAddressButton(View view) {

        notes = ed_notes.getText().toString();

        if (latitude == 0 || longitude == 0) {
            toastDialog.initDialog(getString(R.string.retry), this);
        } else {
            if (notes.equals(""))
                toastDialog.initDialog(getString(R.string.pleaseEnterNameOfNewLocation), this);

            else {
                if (intentFlag.equals("add") || intentFlag.equals("designBanquet") || intentFlag.equals("new") || intentFlag.equals("cart"))
                    addNewAddress();
                else if (intentFlag.equals("update") || intentFlag.equals("cart"))
                    updateAddress();
            }
        }
    } // function of save new address button android

    public int getPrefUserId() {
        int userId = 0;
        SharedPreferences preferences = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        } catch (Exception e) {
            Log.i("QP", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    @SuppressLint("HandlerLeak")
    private void addNewAddress() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AddNewAddressApi addressApi = retrofit.create(AddNewAddressApi.class);

                Log.i("QP", "Location : addAddress : " + latitude + " : " + longitude);
                final Call<AddressResponse> getInterestConnection = addressApi.addNewAddress(toolBarConfig.getPrefUserId(), address
                        , latitude, longitude
                        , notes);

                getInterestConnection.enqueue(new Callback<AddressResponse>() {
                    @Override
                    public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.addressAdded), AddAddressActivity.this);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Log.i("QP", "intent  flag: " + intentFlag);

                                        editorAddress = getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
                                        editorAddress.putString("address", address);
                                        editorAddress.putFloat("lat", (float) latitude);
                                        editorAddress.putFloat("lng", (float) longitude);
                                        editorAddress.apply();

                                        Log.i("QP", "newAddress : " + latitude + " : " + longitude);
                                        if (intentFlag.equals("designBanquet")) {
                                            Intent intent = new Intent(AddAddressActivity.this, DesignBanquet.class);
                                            intent.putExtra("banquet_name", party_name);
                                            intent.putExtra("component", banquetCompnent);
                                            startActivity(intent);
                                            finish();
                                        }
//                                        else if (intentFlag.equals("cart")) {
//                                            Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
//                                            intent.putExtra("fragmentFlag", "cart");
//
//                                            SharedPreferences.Editor cartAddress = getSharedPreferences("EditorCart", MODE_PRIVATE).edit();
//                                            cartAddress.putFloat("lat", (float) latitude);
//                                            cartAddress.putFloat("lng", (float) longitude);
//                                            cartAddress.putString("address", address);
//                                            cartAddress.apply();
//
//                                            Log.i("QP", "cart address: " + cartAddress);
//                                            Log.i("QP", "lat from add new address: " + latitude);
//                                            Log.i("QP", "lng from add new address: " + longitude);
//                                            Log.i("QP", "address from add new address: " + address);
//
//
//                                            Log.i("QP", "intent with cart flag");
//                                            startActivity(intent);
//                                            finish();
//                                        }
                                        else {
                                            Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
                                            intent.putExtra("fragmentFlag", "home");
                                            Log.i("QP", "intent with home flag");
                                            startActivity(intent);
                                            finish();
                                        }

                                        // Toast.makeText(getContext(), getString(R.string.enterVaildDate), Toast.LENGTH_SHORT).show();
                                    }
                                }, 2000);
                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.address_exist), AddAddressActivity.this);
                                progress.dismiss();
                                return;
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<AddressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), AddAddressActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of addNewAddress

    private void updateAddress() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                RetrofitConnection connection = new RetrofitConnection(AddAddressActivity.this);
                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final AddNewAddressApi addressApi = retrofit.create(AddNewAddressApi.class);

                final Call<AddressResponse> getInterestConnection = addressApi.updateUserAddress(intentId, getPrefUserId(), address
                        , latitude, longitude
                        , notes);

                getInterestConnection.enqueue(new Callback<AddressResponse>() {
                    @Override
                    public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.addressUpdated), AddAddressActivity.this);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        editorAddress = getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
                                        editorAddress.putFloat("lat", (float) latitude);
                                        editorAddress.putFloat("lng", (float) longitude);
                                        editorAddress.apply();

                                        Log.i("QP", "newAddress : " + latitude + " : " + longitude);

//                                        if (intentFlag.equals("cart")) {
//                                            SharedPreferences.Editor cartAddress = getSharedPreferences("EditorCart", MODE_PRIVATE).edit();
//
//                                            cartAddress.putFloat("lat", (float) latitude);
//                                            cartAddress.putFloat("lng", (float) longitude);
//                                            cartAddress.putString("address", address);
//                                            cartAddress.apply();
//
//                                            Log.i("QP", "cart address: " + cartAddress);
//                                            Log.i("QP", "lat from add new address: " + latitude);
//                                            Log.i("QP", "lng from add new address: " + longitude);
//                                            Log.i("QP", "address from add new address: " + address);
//
//                                            Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
//                                            intent.putExtra("fragmentFlag", "cart");
//                                            Log.i("QP", "intent with cart flag");
//                                            startActivity(intent);
//                                            finish();
//                                        }

                                        Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
                                        intent.putExtra("fragmentFlag", "home");
                                        Log.i("QP", "intent with home flag");
                                        startActivity(intent);
                                        finish();


                                        // Toast.makeText(getContext(), getString(R.string.enterVaildDate), Toast.LENGTH_SHORT).show();
                                    }
                                }, 2000);

                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<AddressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), AddAddressActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of updateAddress

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListenerGPS);
        }
        Thread.interrupted();
    } // on destroy

} // class of AddAddressActivity
