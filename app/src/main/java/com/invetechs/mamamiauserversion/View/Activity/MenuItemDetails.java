package com.invetechs.mamamiauserversion.View.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invetechs.mamamiauserversion.Config.CustomDialogProgress;
import com.invetechs.mamamiauserversion.Config.ShowDialog;
import com.invetechs.mamamiauserversion.Config.ToolBarConfig;
import com.invetechs.mamamiauserversion.Control.ExtraMealAdapter;
import com.invetechs.mamamiauserversion.Language.Language;
import com.invetechs.mamamiauserversion.Model.CartModel;
import com.invetechs.mamamiauserversion.Model.ExtraModel;
import com.invetechs.mamamiauserversion.R;
import com.invetechs.mamamiauserversion.Retrofit.Request.MenuAndSearchApi.MealDetailsApi;
import com.invetechs.mamamiauserversion.Retrofit.ResultModel.MealDetailsResponse;
import com.invetechs.mamamiauserversion.Retrofit.UrlConnection.RetrofitConnection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MenuItemDetails extends Language {


    // bind views
    @BindView(R.id.linearComponent)
    LinearLayout linearComponent;

    @BindView(R.id.linearCategory)
    LinearLayout linearCategory;

    @BindView(R.id.linearPrepartionTime)
    LinearLayout linearPrepartionTime;

    @BindView(R.id.linearPartyType)
    LinearLayout linearPartyType;

    @BindView(R.id.linearPersonalNumber)
    LinearLayout linearPersonalNumber;

    @BindView(R.id.linearAvialbleTime)
    LinearLayout linearAvialbleTime;

    @BindView(R.id.d_name)
    TextView d_name;
    @BindView(R.id.d_component)
    TextView d_component;
    @BindView(R.id.d_price)
    TextView d_price;
    @BindView(R.id.d_outlets)
    TextView d_outlets;
    @BindView(R.id.d_description)
    TextView d_description;
    @BindView(R.id.d_avialbleTime)
    TextView d_avialbleTime;

    @BindView(R.id.d_prepartionTime)
    TextView d_prepartionTime;
    @BindView(R.id.d_category)
    TextView d_category;

    @BindView(R.id.d_partyType)
    TextView d_partyType;
    @BindView(R.id.d_personalNumber)
    TextView d_personalNumber;

    //vars
    Dialog cartDialog;
    RecyclerView extra_recycler_view;
    Button cancelCardtDialog, addCart;
    TextView textTitle, textPrice;
    ImageView imageItem;
    EditText ed_notes;
    ElegantNumberButton buttonNumber;
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences prefs;
    int counterOfCallingImageSlider = 0;
    int cartKitchenId = 0;
    SharedPreferences.Editor editor;
    Dialog acceptDialog;
    Button yes, no;
    LinearLayout layout, layout2;
    MealDetailsResponse.DataBean mealData = new MealDetailsResponse.DataBean();
    List<MealDetailsResponse.DataBean.ImagesBean> imageList = new ArrayList<>();
    int itemId;
    ToolBarConfig toolBarConfig;
    int kitchenId = 0, deliveryMaxBill = 0;
    double distanceToUser = 0, totalDelivery = 0;
    CartModel cartModel;
    ShowDialog toastDialog = new ShowDialog();
    ExtraModel extraModelArrayList;
    public int itemCount = 1;
    int sum;
    String send = "";
    ExtraMealAdapter extraAdapter;
    private List<MealDetailsResponse.AdditionsBean> additionsBeans = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_details);
        ButterKnife.bind(this);
        initAcceptDialog();
        getIntentId();
        getItemDetails();
        setToolBarConfig();
        extraModelArrayList = ExtraModel.getInstance();
        initLanguage();

    } // function of onCreate

    private void saveCartLocation() {
        SharedPreferences prefs = getSharedPreferences("AddressPref", MODE_PRIVATE);
        String address = prefs.getString("address", "");
        float lat = prefs.getFloat("lat", 0);
        float lng = prefs.getFloat("lng", 0);

        SharedPreferences.Editor prefEditor = getSharedPreferences("AddressPrefCart", MODE_PRIVATE).edit();
        prefEditor.putString("address", address);
        prefEditor.putFloat("lat", lat);
        prefEditor.putFloat("lng", lng);
        prefEditor.apply();
    } // function of saveCartLocation

    private void initLanguage() {
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
            Log.i("QP", "language arabic" + sharedPreferences);


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);

        }


    } // initialize language and font

    private void setToolBarConfig() {
        toolBarConfig = new ToolBarConfig(this, "none");
        toolBarConfig.setTitle(getString(R.string.details));
        toolBarConfig.addNotification();
        toolBarConfig.initCartBadge();
    } // function of setToolBarConfig

    @Override
    protected void onResume() {
        super.onResume();
        toolBarConfig.initCartBadge();
    } // function of OnResume

    private void getIntentId() {
        Intent intent = getIntent();
        if (intent != null) {
            itemId = intent.getIntExtra("id", 0);
            Log.i("QP", "id : details : " + itemId);
            kitchenId = intent.getIntExtra("kitchenId", 0);
            deliveryMaxBill = intent.getIntExtra("maxbill", 0);
            totalDelivery = intent.getDoubleExtra("deliveryPerKilo", 0);
            distanceToUser = intent.getDoubleExtra("distanceToUser", 0);

            Log.i("QT", "MD delivery : " + totalDelivery + " : distance : " + distanceToUser);
        }
    } // get id from list

    private void initAcceptDialog() {
        acceptDialog = new Dialog(this);
        acceptDialog.setContentView(R.layout.confirm_remove_cart);

        yes = acceptDialog.findViewById(R.id.yes);
        no = acceptDialog.findViewById(R.id.no);
        layout = acceptDialog.findViewById(R.id.dialogLayout);
        layout2 = acceptDialog.findViewById(R.id.dialogLayout2);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptDialog.dismiss();
                toolBarConfig.clearCart();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptDialog.dismiss();
            }
        });
    }  // function of initAcceptDialog

    private void initCartDialog() {
        cartDialog = new Dialog(this);
        cartDialog.setContentView(R.layout.dialog_card);

        cancelCardtDialog = cartDialog.findViewById(R.id.cancelCartDialog);
        addCart = cartDialog.findViewById(R.id.addCartDialog);
        buttonNumber = cartDialog.findViewById(R.id.number_button);

        ed_notes = cartDialog.findViewById(R.id.ed_notes);
        textTitle = cartDialog.findViewById(R.id.nameMenu_dialog);
        textPrice = cartDialog.findViewById(R.id.priceMenu_dialog);
        imageItem = cartDialog.findViewById(R.id.imageMenu_dialog);
        extra_recycler_view = cartDialog.findViewById(R.id.extra_recycler_view);


        Log.i("QP", "data : " + mealData);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            textTitle.setText(mealData.getAr_title());
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/GESSTwoMedium.otf");
            textPrice.setTypeface(font);
            ed_notes.setTypeface(font);
            textTitle.setTypeface(font);
            addCart.setTypeface(font);
            cancelCardtDialog.setTypeface(font);

        }  // arabic
        else if (sharedPreferences.getString("language", "ar").equals("en")) {

            textTitle.setText(mealData.getEn_title());
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
            textPrice.setTypeface(font);
            ed_notes.setTypeface(font);
            textTitle.setTypeface(font);
            addCart.setTypeface(font);
            cancelCardtDialog.setTypeface(font);

        } // english


        textPrice.setText(mealData.getPrice() + " " + getResources().getString(R.string.sr));

        String imageUrl = "https://mammamiaa.com/cpanel/upload/meal/" + mealData.getImages().get(0).getImage();

        if (!mealData.getImages().get(0).getImage().equals(null))
            Glide.with(this)
                    .load(imageUrl)
                    .into(imageItem);

        buttonNumber.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {

                // codeeee
                extraModelArrayList = ExtraModel.getInstance();
                int a = extraModelArrayList.getId();
                String b = extraModelArrayList.getPrice();
                extraModelArrayList.setItemCounts(newValue);

                itemCount = newValue;

                try {
                    sum = extraModelArrayList.getSum();
                    Log.i("QP", "get sum : " + sum);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "sum exception : " + e.getMessage());
                }

                Log.i("QP", "a : " + a);
                Log.i("QP", "b : " + b);
                //  send = "" + Integer.parseInt(menuList.get(position).getPrice()) + " " + context.getString(R.string.sr);

                // Log.i("QP", "send : " + send);
                //holder.textPrice.setText("" + Integer.parseInt(menuList.get(position).getPrice()) * newValue + " " + context.getString(R.string.sr));

                if (sum != -1) {
                    textPrice.setText(sum * newValue + " " + getString(R.string.sr));
                } else {
                    textPrice.setText("" + Integer.parseInt(mealData.getPrice()) * newValue + " " + getString(R.string.sr));
                }
            }
        });

        cancelCardtDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                extraModelArrayList.setItemCounts(1);
                cartDialog.dismiss();
                try {
                    if (editor != null) {
                        editor.remove("Additions");
                        editor.commit();
                        editor.apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "exception : " + e.getMessage());
                }

            }
        }); // cancel buttoon

        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get shared preference ids
                prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                Gson gson = new Gson();
                String json = prefs.getString("Additions", null);
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();

                Log.i("QP", " additions arrray list in shared preference =  : " + gson.fromJson(json, type));

                saveCartLocation();
                editor = getSharedPreferences("cart", MODE_PRIVATE).edit();
                editor.putInt("kitchenId", kitchenId);
                editor.putInt("maxbill", deliveryMaxBill);
                editor.putFloat("deliveryPerKilo", (float) totalDelivery);
                editor.putFloat("distanceToUser", (float) distanceToUser);
                editor.apply();

                //cart dialog
                String enTitle = mealData.getEn_title();
                String arTitle = mealData.getAr_title();
                //  double price = Double.parseDouble(mealData.getPrice());

                double price;

                sum = extraModelArrayList.getSum();

                if (sum != -1) {
                    price = sum;
                } else {
                    price = Integer.parseInt(mealData.getPrice());
                }

                String notes = ed_notes.getText().toString();
                int amount = Integer.parseInt(buttonNumber.getNumber());
                int itemID = itemId;

                if (notes.equals(null) || notes.isEmpty()) notes = "none";

                cartModel = new CartModel(itemID, enTitle, arTitle, notes, amount, price, kitchenId, (ArrayList<Integer>) gson.fromJson(json, type));

                toolBarConfig.addCart(cartModel);
                toastDialog.initDialog(getString(R.string.itmeAdded), MenuItemDetails.this);
                cartDialog.dismiss();

                try {
                    if (editor != null) {
                        editor.remove("Additions");
                        editor.commit();
                        editor.apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("QP", "exception : " + e.getMessage());
                }
            }
        });

    } //  function of initCartDialog

    public void cartButton(View view) {

        extraModelArrayList.setItemCounts(1);
        send = "" + Integer.parseInt(mealData.getPrice());
        buttonNumber.setNumber("1");
        extraModelArrayList.setPrice(send);
        extraModelArrayList.setSum(Integer.parseInt(send));
        Log.i("QP", "send when click : " + send);

        //  Log.i("QP", "array addition : " + additionsBeans);

        // additionsBeans = menuList.get(position).getAdditions();

        Log.i("QP", "integer : " + additionsBeans);

        extraAdapter = new ExtraMealAdapter(this, additionsBeans, send, textPrice);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        extra_recycler_view.setLayoutManager(layoutManager);
        extra_recycler_view.setHasFixedSize(true);
        extra_recycler_view.setAdapter(extraAdapter);

        prefs = getSharedPreferences("cart", MODE_PRIVATE);
        if (prefs != null)
            cartKitchenId = prefs.getInt("kitchenId", 0);

        if (toolBarConfig.getPrefUserId() != 0) {

            if (cartKitchenId == kitchenId || cartKitchenId == 0) {
                cartDialog.show();
            } else {
                layout.setBackgroundResource(R.color.white);
                layout2.setBackgroundResource(R.color.white);
                acceptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                acceptDialog.getWindow()
                        .setLayout((int) (getScreenWidth(this) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                acceptDialog.show();
            }

        } // user login
        else {
            Intent intent = new Intent(MenuItemDetails.this, LoginActivity.class);
            startActivity(intent);
        } // user not login


    } // cart Button

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getItemDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitConnection.ConnectWith();

                final MealDetailsApi mealDetailsApiApi = retrofit.create(MealDetailsApi.class);

                Call<MealDetailsResponse> getConnection = mealDetailsApiApi.getMealDetails(itemId);

                getConnection.enqueue(new Callback<MealDetailsResponse>() {
                    @Override
                    public void onResponse(Call<MealDetailsResponse> call, Response<MealDetailsResponse> response) {
                        try {

                            String code = response.body().getCode();

                            response.body();

                            Log.i("QP", "code : " + code);

                            if (code.equals("200")) {
                                mealData = response.body().getData();

                                additionsBeans = response.body().getAdditions();

                                imageList = response.body().getData().getImages();
                                if (counterOfCallingImageSlider == 0)
                                    imageSlider(imageList);
                                setData();
                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<MealDetailsResponse> call, Throwable t) {

                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }

        }.start();
    } // function of getItem Details from api

    private void setData() {

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            d_name.setText(mealData.getAr_title());

            String outlets = "";
            for (int i = 0; i < mealData.getOutlets().size(); i++) {
                if (i == mealData.getOutlets().size() - 1)
                    outlets = outlets + mealData.getOutlets().get(i).getAr_title();
                else
                    outlets = outlets + mealData.getOutlets().get(i).getAr_title() + " - ";
            }
            d_outlets.setText(outlets);

            d_description.setText(mealData.getAr_description());

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {

            d_name.setText(mealData.getEn_title());

            String outlets = "";
            for (int i = 0; i < mealData.getOutlets().size(); i++) {
                if (i == mealData.getOutlets().size() - 1)
                    outlets = outlets + mealData.getOutlets().get(i).getEn_title();
                else
                    outlets = outlets + mealData.getOutlets().get(i).getEn_title() + " - ";
            }
            d_outlets.setText(outlets);

            d_description.setText(mealData.getEn_description());
        }

        d_component.setText(mealData.getComponent());
        d_price.setText(mealData.getPrice() + " " + getResources().getString(R.string.sr));


        d_prepartionTime.setText(mealData.getNeededtime() + " " + getString(R.string.minute));

        String avialableTime = "";
        for (int i = 0; i < mealData.getHours().size(); i++) {
            avialableTime = avialableTime + getResources().getString(R.string.from) + " " + mealData.getHours().get(i).getFrom() + "  " +
                    getResources().getString(R.string.to) + " " + mealData.getHours().get(i).getTo() + "\n";
        }

        if (avialableTime.equals(""))
            d_avialbleTime.setText(getString(R.string.available));
        else
            d_avialbleTime.setText(avialableTime);

        String categories = "", typeCategory = "";
        for (int i = 0; i < mealData.getCategory().size(); i++) {
            if (mealData.getCategory().get(i).getId() == 1) {
                typeCategory = getResources().getString(R.string.breakFast);
            }
            if (mealData.getCategory().get(i).getId() == 2) {
                typeCategory = getResources().getString(R.string.launch);
            }
            if (mealData.getCategory().get(i).getId() == 3) {
                typeCategory = getResources().getString(R.string.dinner);
            }
            if (mealData.getCategory().get(i).getId() == 1 || mealData.getCategory().get(i).getId() == 2 || mealData.getCategory().get(i).getId() == 3) {
                if (i == mealData.getCategory().size() - 1) {
                    categories = categories + typeCategory;
                } else {
                    categories = categories + typeCategory + " - ";
                }

            }
        }


        d_category.setText(categories);

        d_personalNumber.setText(mealData.getCustomer_num() + " " + getResources().getString(R.string.person));

        String categories1 = "", typeCategory1 = "";
        for (int i = 0; i < mealData.getCategory().size(); i++) {
            if (mealData.getCategory().get(i).getId() == 6) {
                typeCategory1 = getResources().getString(R.string.wedding);

            }
            if (mealData.getCategory().get(i).getId() == 7) {
                typeCategory1 = getResources().getString(R.string.engagment);

            }
            if (mealData.getCategory().get(i).getId() == 8) {
                typeCategory1 = getResources().getString(R.string.aqiqa);

            }
            if (mealData.getCategory().get(i).getId() == 6 || mealData.getCategory().get(i).getId() == 7 || mealData.getCategory().get(i).getId() == 8) {
                if (i == mealData.getCategory().size() - 1) {
                    categories1 = categories1 + typeCategory1;
                } else {
                    categories1 = categories1 + typeCategory1 + " - ";
                }

            }

        }


        d_partyType.setText(categories1);


        if (mealData.getKitchentag_id() == 1) {
            linearCategory.setVisibility(View.GONE);
            linearAvialbleTime.setVisibility(View.GONE);
        }// banquet details
        if (mealData.getKitchentag_id() == 2) {
            linearComponent.setVisibility(View.GONE);
            linearCategory.setVisibility(View.GONE);

            linearPartyType.setVisibility(View.GONE);
            linearPersonalNumber.setVisibility(View.GONE);
        }// dessert details
        if (mealData.getKitchentag_id() == 3) {
            linearComponent.setVisibility(View.GONE);

            linearPartyType.setVisibility(View.GONE);
            linearPersonalNumber.setVisibility(View.GONE);
        }// meal details
        if (mealData.getKitchentag_id() == 4) {
            linearComponent.setVisibility(View.GONE);
            linearCategory.setVisibility(View.GONE);
            linearPrepartionTime.setVisibility(View.GONE);

            linearPartyType.setVisibility(View.GONE);
            linearPersonalNumber.setVisibility(View.GONE);
        }// beverge details
        if (mealData.getKitchentag_id() == 5) {
            linearPartyType.setVisibility(View.GONE);
            linearPersonalNumber.setVisibility(View.GONE);
        }// offer details

        initCartDialog();

    } // function of setData

    public void imageSlider(List<MealDetailsResponse.DataBean.ImagesBean> urls) {

        SliderLayout sliderShow = findViewById(R.id.slider);

        for (int i = 0; i < urls.size(); i++) {


            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image("https://mammamiaa.com/cpanel/upload/meal/" + urls.get(i).getImage());
            sliderShow.addSlider(textSliderView);
            Log.e("QP", "Image : " + urls.get(i).getImage());

        }
        counterOfCallingImageSlider = 1;

    } // imageSlider function

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.destroy();
    }

} // class of MenuItemDetails
